#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os


def setupDjango():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'extranet.settings')

    import django
    django.setup()


if __name__ == '__main__':
    setupDjango()

    from cron import log_file_checker, products_out_checker

    products_out_checker.check()
    log_file_checker.check()
