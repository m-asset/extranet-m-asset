from website.apps.api.serializers.products import kit_serializer
from website.apps.web.encryption import AesEncryption
from website.apps.web.models import UserJobs, Badges, Users, GeneralConfig


def job_serializer(job, aes=AesEncryption()):
    from website.apps.api.serializers.products import kit_type_serializer
    return {
        "id": job.id,
        "name": job.name,
        "assign": bool(job.assign),
        "kit_type": kit_type_serializer(job.kit_type, aes) if job.kit_type is not None else None
    }


def shift_serializer(shift):
    return {
        "id": shift.id,
        "name": shift.name,
        "start": str(shift.start),
        "end": str(shift.end),
    }


def badge_serializer(badge, aes=AesEncryption()):
    user = aes.decrypt(badge.user) if badge.user else None
    if user is not None:
        ruser = Users.objects.filter(id=user)
        if ruser.count() > 0:
            user = user_serializer(ruser.first(), basic=True, aes=aes)

    return {
        "id": badge.id,
        "name": badge.name,
        "user": user,
        "rfid": badge.rfid,
        "updater": {
            "user": user_serializer(badge.update_user, basic=True, aes=aes),
            "time": str(badge.update_timestamp)
        }
    }


def message_serializer(message, aes=AesEncryption()):
    return {
        "id": message.id,
        "message": message.message,
        "start": str(message.start),
        "end": str(message.end) if message.end else None,
        "read": str(message.read) if message.read else None,
        "placer": {
            "user": user_serializer(message.creation_user, basic=True, aes=aes),
            "time": str(message.creation_timestamp)
        }
    }


def user_serializer(user, basic=True, aes=AesEncryption()):
    if user.ihm_password is not None:
        return {
            "id": 0,
            "name": "ADMIN",
            "surname": "VENDOR",
            "email": "Contact your reseller",
            "disabled": False,
            "last_login": None
        }
    elif user.is_deleted():
        return {
            "id": 0,
            "name": "DELETED",
            "surname": "USER",
            "email": "Account removed",
            "disabled": True,
            "last_login": None
        }

    last = str(user.get_last_login())

    base = {
        "id": user.id,
        "name": aes.decrypt(user.name),
        "surname": aes.decrypt(user.surname),
        "email": aes.decrypt(user.email) if user.email else None,
        "disabled": user.disabled,
        "last_login": last
    }
    if basic:
        return base

    jobs = []
    for job in UserJobs.objects.filter(user=user):
        sjob = job_serializer(job.job, aes=aes)
        sjob["placer"] = {
            "user": user_serializer(job.creation_user, basic=True, aes=aes),
            "time": str(job.creation_timestamp)
        }
        jobs.append(sjob)
    base["jobs"] = jobs

    base['shift'] = shift_serializer(user.shift) if user.shift is not None else None

    badges = []
    for badge in Badges.objects.filter(user=aes.encrypt(str(user.id), "rfid")).all():
        badges.append(badge_serializer(badge=badge, aes=aes))
    base['badges'] = badges

    kits = []
    kitsOut = user.get_kits_out()
    if kitsOut is not None:
        for kit in kitsOut:
            kits.append(kit_serializer(kit, aes))

    base['kits'] = kits

    return base


def config_serializer():
    vals = {}
    for config in GeneralConfig.objects.all():
        vals[config.key] = config.value
    return vals
