from website.apps.web.encryption import AesEncryption
from website.apps.web.models import Products, KitTypeProductFamily, KitLogs, Cases


def battery_type_serializer(battery_type, aes=AesEncryption()):
    from website.apps.api.serializers.user import user_serializer
    return {
        "id": battery_type.id,
        "name": battery_type.name,
        "placer": {
            "user": user_serializer(battery_type.update_user, basic=True, aes=aes),
            "time": str(battery_type.update_timestamp)
        }
    }


def product_family_serializer(product_family, aes=AesEncryption()):
    from website.apps.api.serializers.user import user_serializer
    return {
        "id": product_family.id,
        "name": product_family.name,
        "placer": {
            "user": user_serializer(product_family.update_user, basic=True, aes=aes),
            "time": str(product_family.update_timestamp)
        }
    }


def product_type_serializer(product_type, aes=AesEncryption()):
    from website.apps.api.serializers.user import user_serializer
    return {
        "id": product_type.id,
        "family": product_family_serializer(product_type.family, aes=aes),
        "name": product_type.name,
        "brand": product_type.brand,
        "model": product_type.model,
        "reference": product_type.reference,
        "picture": product_type.get_picture(),
        "updater": {
            "user": user_serializer(product_type.update_user, basic=True, aes=aes),
            "time": str(product_type.update_timestamp)
        }
    }


def product_serializer(product, basic=True, aes=AesEncryption()):
    from website.apps.api.serializers.user import user_serializer

    available = False
    location = ""
    assignable = False
    inhand = False

    if product.kit is not None:
        case = Cases.objects.filter(kit=product.kit)
        if case.count() > 0:
            case = case.first()
            available = case.active
            location = case.rack.name + str(case.position)
        assignable = product.kit.assignable == 1

        last_kit = KitLogs.objects.filter(kit=product.kit).order_by("-id")
        if last_kit.count() > 0:
            last_kit = last_kit.first()
            inhand = last_kit.out == 1

    return {
        "id": product.id,
        "serial": product.serial_number,
        "id_tag": product.id_tag,
        "local_id": product.local_id,
        "used": product.used,
        "updater": {
            "user": user_serializer(product.update_user, basic=True, aes=aes),
            "time": str(product.update_timestamp)
        },
        "kit": (product.kit.id if product.kit is not None else None) if basic else kit_serializer(product.kit, aes=aes),
        "available": available,
        "location": location,
        "assignable": assignable,
        "inhand": inhand,
        "type": product.product_type.name if basic else product_type_serializer(product.product_type, aes=aes),
        "family": product.product_type.family.name if basic else product_family_serializer(product.product_type.family,
                                                                                           aes=aes)
    }


def kit_type_serializer(kit_type, aes=AesEncryption()):
    product_family = []
    for klpf in KitTypeProductFamily.objects.filter(kit_type=kit_type):
        product_family.append(product_family_serializer(klpf.product_family, aes=aes))

    return {
        "id": kit_type.id,
        "product_families": product_family
    }


def kit_serializer(kit, aes=AesEncryption()):
    from website.apps.api.serializers.user import user_serializer
    if kit is None:
        return None
    products = []
    for product in Products.objects.filter(kit=kit).all():
        products.append(product_serializer(product, aes=aes))
    return {
        "id": kit.id,
        "type": kit_type_serializer(kit.type, aes=aes),
        "products": products,
        "assignable": kit.assignable == 1,
        "priority": kit.priority,
        "updater": {
            "user": user_serializer(kit.update_user, basic=True, aes=aes),
            "time": str(kit.update_timestamp)
        }
    }


def rack_serializer(rack, aes=AesEncryption()):
    from website.apps.api.serializers.user import user_serializer
    cases = []
    for case in Cases.objects.filter(rack=rack).order_by("position").all():
        cases.append(case_serializer(case, aes=aes))
    return {
        "id": rack.id,
        "number": rack.number,
        "name": rack.name,
        "state": rack.state == 1,
        "doors_count": rack.doors_count,
        "master": rack.master == 1,
        "serial": rack.serial,
        "partnumber": rack.partnumber,
        "uvc_activate": rack.uvc_activate == 1,
        "ip_address": rack.ip_address,
        "cases": cases,
        "updater": {
            "user": user_serializer(rack.update_user, basic=True, aes=aes),
            "time": str(rack.update_timestamp)
        }
    }


def case_serializer(case, aes=AesEncryption()):
    from website.apps.api.serializers.user import user_serializer
    empty = True
    inhand = False
    assigned_to = None

    if case.kit is not None:
        last_kit = KitLogs.objects.filter(kit=case.kit).order_by("-id")
        if last_kit.count() > 0:
            last_kit = last_kit.first()
            empty = last_kit.out == 1
            inhand = last_kit.out == 1

        assigned_to = case.kit.user_currently_assigned()
        if assigned_to is not None:
            assigned_to = user_serializer(assigned_to, basic=True, aes=aes)

    return {
        "id": case.id,
        "kit": kit_serializer(case.kit, aes) if case.kit else None,
        "empty": empty,
        "active": case.active,
        "rack": case.rack.id,
        "position": case.position,
        "inhand": inhand,
        "assigned_to": assigned_to,
        "updater": {
            "user": user_serializer(case.update_user, basic=True, aes=aes),
            "time": str(case.update_timestamp)
        }
    }


def incident_serializer(product_incident, aes=AesEncryption()):
    from website.apps.api.serializers.user import user_serializer
    comments = []
    for comment in product_incident.get_comment_history():
        comments.append({
            "comment": comment.comment,
            "placer": {
                "user": user_serializer(comment.creation_user, basic=True, aes=aes),
                "time": str(comment.creation_timestamp)
            }
        })
    return {
        "id": product_incident.id,
        "product": product_serializer(product_incident.product, aes=aes),
        "type": product_incident.type,
        "solved": product_incident.solved,
        "comments": comments,
        "placer": {
            "user": user_serializer(product_incident.creation_user, basic=True, aes=aes),
            "time": str(product_incident.creation_timestamp)
        },
        "updater": {
            "user": user_serializer(product_incident.update_user, basic=True, aes=aes),
            "time": str(product_incident.update_timestamp)
        }
    }


def kit_log_serializer(kit_log, aes=AesEncryption()):
    from website.apps.api.serializers.user import user_serializer
    return {
        "id": kit_log.id,
        "user": user_serializer(kit_log.user, basic=True, aes=aes),
        "kit": kit_serializer(kit_log.kit, aes=aes),
        "out": True if kit_log.out == 1 else False,
        "time": str(kit_log.creation_timestamp)
    }
