import json

from django.http import HttpResponse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from website.apps.api.serializers.products import rack_serializer, case_serializer, kit_type_serializer
from website.apps.api.serializers.user import user_serializer, config_serializer, job_serializer, badge_serializer, \
    message_serializer
from website.apps.api.views.api import get_error, args_in_array, auth_rack, get_client_ip
from website.apps.web.encryption import AesEncryption
from website.apps.web.forms_kiosk import RackCreationForm, UserTakeProductsForm, ConfirmProductGivenForm, \
    ConfirmProductReturnedForm, RackEditionForm, ConfigEditionForm
from website.apps.web.forms_users import EditBadgeAttribForm, AddBadgeForm, EditBadgeNameForm, EditBadgeDeleteForm
from website.apps.web.models import Users, Badges, Kits, KitAssignations, GeneralConfig, KitLogs, KitTypeProductFamily, \
    ProductJobOut, ProductIncidents, UserLogs, UserAdminLogs, UserJobs, UserMessages
# ERROR CODES 100 - 199
from website.apps.web.util import get_pagination_api, log_rack


class CollectRackData(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(100, "Invalid rack")

        return HttpResponse(json.dumps(rack_serializer(rack)), content_type="application/json")


class CreateRack(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CreateRack, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        form = RackCreationForm(request.POST)
        if not form.is_valid():
            return get_error(101, "Invalid data !")
        form.save(get_client_ip(request))

        log_rack(2, form.rack, "ADMIN has created the rack {}".format(form.rack.get_printed()))

        return HttpResponse(json.dumps(rack_serializer(form.rack)), content_type="application/json")


class EditRack(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(EditRack, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(100, "Invalid rack")
        form = RackEditionForm(request.POST, rack)
        if not form.is_valid():
            return get_error(101, "Invalid data !")
        form.save()

        log_rack(2, rack, "{} has edited the rack {}".format(form.admin.get_full_name(), rack.get_printed()))

        return HttpResponse(json.dumps(rack_serializer(rack)), content_type="application/json")


class UserDataRfid(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)

        if rack is None:
            return get_error(100, "Invalid rack")

        get = request.GET

        if not args_in_array(['rfid'], get):
            return get_error(101, "Missing arguments")

        rfid = get['rfid']

        guser = None
        if rfid != "2A581D97":
            badge = Badges.objects.filter(rfid=get['rfid'])
            if badge.count() == 0:
                return get_error(103, "Invalid rfid")
            badge = badge.first()

            if badge.user is None:
                return get_error(104, "Badge empty")

            aes = AesEncryption()

            guser = Users.objects.filter(id=aes.decrypt(badge.user))
            if guser.count() == 0 or guser.first().ihm_password is not None:
                return get_error(102, "User not found")
            guser = guser.first()
        else:
            guser = {}
            for usr in Users.objects.filter(email__isnull=False, disabled=False, ihm_password__isnull=True):
                if usr.shift is not None and not usr.shift.currently_valid():
                    continue
                guser[usr] = UserJobs.objects.filter(user=usr).count()
            guser = {k: v for k, v in reversed(sorted(guser.items(), key=lambda item: item[1]))}
            guser = list(guser.keys())[0]

        if "login" in get and get["login"] == "yes":
            log = UserLogs()
            log.user = guser
            log.rack = rack
            log.creation_timestamp = timezone.now()
            log.valid = not guser.disabled
            log.save()

        return HttpResponse(json.dumps(user_serializer(guser, basic=False)), content_type="application/json")


class UserAdmin(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(100, "Invalid rack")

        get = request.GET
        if not args_in_array(['ihm'], get):
            return get_error(101, "Missing arguments")

        guser = Users.objects.filter(ihm_password=get['ihm'])
        if guser.count() == 0:
            return get_error(102, "User not found")
        guser = guser.first()

        log = UserAdminLogs()
        log.user = guser
        log.rack = rack
        log.web = False
        log.phone = False
        log.creation_timestamp = timezone.now()
        log.valid = not guser.disabled
        log.save()

        user = user_serializer(guser, basic=False)
        user["id"] = guser.id

        return HttpResponse(json.dumps(user), content_type="application/json")


class UserList(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)

        if rack is None:
            return get_error(100, "Invalid rack")

        aes = AesEncryption()
        users = Users.objects.filter(ihm_password=None)
        if "filter" in request.GET:
            filter = str(request.GET["filter"])
            if len(filter) > 0:
                spl = filter.split("&")
                for s in spl:
                    vals = [s]
                    if "=" in s:
                        vals = s.split("=")
                    elif "~" in s:
                        vals = s.split("~")

                    if len(vals) == 1:
                        if s.lower() == "norfid":
                            nusers = []
                            for user in users:
                                if len(user.get_badges()) == 0:
                                    nusers.append(user)
                            users = nusers

        (users, page, max_page) = get_pagination_api(request.GET, users)

        usersSer = []
        for luser in (users if type(users) == list else users.all()):
            usersSer.append(user_serializer(luser, aes=aes))

        return HttpResponse(json.dumps({"users": usersSer, "page": page, "max_page": max_page}),
                            content_type="application/json")


class UserData(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)

        if rack is None:
            return get_error(100, "Invalid rack")
        get = request.GET

        if not args_in_array(['user'], get):
            return get_error(101, "Missing arguments")

        guser = Users.objects.filter(id=get['user'], ihm_password__isnull=True)
        if guser.count() == 0:
            return get_error(102, "User not found")
        guser = guser.first()

        return HttpResponse(json.dumps(user_serializer(guser, basic=False)), content_type="application/json")


class GetConfig(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(100, "Invalid rack")

        return HttpResponse(json.dumps(config_serializer()), content_type="application/json")


class SetConfig(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(SetConfig, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(100, "Invalid rack")
        form = ConfigEditionForm(request.POST)
        if not form.is_valid():
            return get_error(101, "Invalid data !")
        form.save()

        log_rack(2, rack,
                 "{} has edited the config {}".format(form.admin.get_full_name(), json.dumps(form.valid_data)))

        return HttpResponse(json.dumps({"valid": True}), content_type="application/json")


class UserTakeProducts(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.user = None
        self.rack = None
        self.grades_activated = False

        config = GeneralConfig.objects.filter(key="GRADES_ACTIVATED")
        if config.count() > 0:
            config = config.first()
            self.grades_activated = config.value == "1" or config.value.lower() == "true"

    def get(self, request, **kwargs):
        self.rack = auth_rack(request)

        if self.rack is None:
            return get_error(100, "Invalid rack")

        get = request.GET
        form = UserTakeProductsForm(get)

        if not form.is_valid():
            return get_error(103, "Invalid data !")

        self.user = form.user

        products_out = self.user.get_kits_out()

        kit_type = form.job.kit_type if form.job else form.kitType
        if products_out is None:
            # Full kit part
            kits = []
            for kit in Kits.objects.filter(type=kit_type):
                if kit.is_available_for_user(self.user):
                    kits.append(kit)

            if kits:
                kits.sort(key=self.order_kits, reverse=True)

                for kit in kits:
                    case = kit.get_location()
                    if case is not None:
                        return HttpResponse(json.dumps({"cases": [case_serializer(case)]}),
                                            content_type="application/json")

        # Spare kit part
        families = []
        for ktpf in KitTypeProductFamily.objects.filter(kit_type=kit_type):
            families.append(ktpf.product_family)

        if products_out is not None:
            famOut = []
            for kit in products_out:
                kt = kit.type
                for ktpf in KitTypeProductFamily.objects.filter(kit_type=kt):
                    famOut.append(ktpf.product_family.id)
            nfamilies = []
            for fam in families:
                if fam.id not in famOut:
                    nfamilies.append(fam)
            families = nfamilies

        kits = {}
        for family in families:
            kit_type = None
            for ktpf in KitTypeProductFamily.objects.filter(product_family=family):
                if KitTypeProductFamily.objects.filter(kit_type=ktpf.kit_type).count() == 1:
                    kit_type = ktpf.kit_type
                    break
            if kit_type is None:
                return get_error(104, "No spare kit type found !")
            kitsFam = []
            for kit in Kits.objects.filter(type=kit_type):
                if kit.is_available_for_user(self.user):
                    kitsFam.append(kit)
            if len(kitsFam) == 0:
                return get_error(105, "No spare kit found !")
            kitsFam.sort(key=self.order_kits, reverse=True)
            kits[family] = kitsFam

        cases = []
        aes = AesEncryption()
        for fam in kits:
            for kit in kits[fam]:
                case = kit.get_location()
                if case is not None:
                    cases.append(case_serializer(case, aes=aes))
                    break

        return HttpResponse(json.dumps({"cases": cases}), content_type="application/json")

    def order_kits(self, kit):
        # Scoring
        # Assigned     1200
        # Current rack 600
        # Grade        300
        # Priority     2
        # Usage        1 / usage

        score = 0.0

        # Assignations
        if kit.assignable:
            score += KitAssignations.objects.filter(kit=kit, user=self.user).count() * 1200

        # Current rack
        case = kit.get_location()
        if case and case.rack.id == self.rack.id:
            score += 600

        # Grades
        if self.grades_activated:
            overallGrade = 0.0
            overallGradeCount = 0
            for log in KitLogs.objects.filter(kit=kit, grade__gt=0).order_by("-creation_timestamp")[0:300]:
                overallGrade += log.grade
                overallGradeCount += 1
            score += 300 + (float(overallGrade / overallGradeCount) if overallGradeCount > 0 else 0)

        # Priority
        score += 2 + kit.priority

        # Usage
        overallUsage = 0
        for product in kit.get_products():
            overallUsage += product.used
        score += 1 if overallUsage == 0 else float(1 / (overallUsage / len(kit.get_products())))

        return score


class ConfirmProductGiven(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ConfirmProductGiven, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)

        if rack is None:
            return get_error(100, "Invalid rack")

        form = ConfirmProductGivenForm(request.POST)
        if not form.is_valid():
            return get_error(101, "Invalid data !")
        form.save()

        return HttpResponse(json.dumps({"valid": True}), content_type="application/json")


class ConfirmProductReturned(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ConfirmProductReturned, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)

        if rack is None:
            return get_error(100, "Invalid rack")

        form = ConfirmProductReturnedForm(request.POST)
        if not form.is_valid():
            return get_error(101, "Invalid data !")
        form.save()

        return HttpResponse(json.dumps({"valid": True}), content_type="application/json")


class AwaitingProduct(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)

        if rack is None:
            return get_error(100, "Invalid rack")
        get = request.GET

        if not args_in_array(['user'], get):
            return get_error(101, "Missing arguments")

        guser = Users.objects.filter(id=get['user'], ihm_password__isnull=True)
        if guser.count() == 0:
            return get_error(102, "User not found")
        guser = guser.first()

        pjo = ProductJobOut.objects.filter(user=guser,
                                           creation_timestamp__gt=timezone.now() - timezone.timedelta(days=1)).order_by(
            '-creation_timestamp')
        if pjo.count() == 0:
            return get_error(106, "None found")
        pjo = pjo.first()

        jobCount = KitTypeProductFamily.objects.filter(kit_type=pjo.job.kit_type).count()

        if pjo.product_out >= jobCount:
            return get_error(107, "None found")

        return HttpResponse(json.dumps(job_serializer(pjo.job)), content_type="application/json")


class AwaitingReplacement(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(100, "Invalid rack")
        get = request.GET

        if not args_in_array(['user'], get):
            return get_error(101, "Missing arguments")

        guser = Users.objects.filter(id=get['user'], ihm_password__isnull=True)
        if guser.count() == 0:
            return get_error(102, "User not found")
        guser = guser.first()

        lastIncident = ProductIncidents.objects.filter(creation_user=guser).order_by("-creation_timestamp")
        if lastIncident.count() == 0:
            return get_error(111, "No incident !")
        lastIncident = lastIncident.first()
        if KitLogs.objects.filter(creation_timestamp__gt=lastIncident.creation_timestamp).count() > 0:
            return get_error(112, "Not waiting !")
        kitsOut = guser.get_kits_out()
        if kitsOut is None:
            return get_error(112, "Not waiting !")
        if lastIncident.product.kit is None:
            return get_error(113, "Kit disappeared")

        return HttpResponse(json.dumps(kit_type_serializer(lastIncident.product.kit.type)),
                            content_type="application/json")


class BadgeList(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(100, "Invalid rack")

        aes = AesEncryption()
        badges = Badges.objects.all()

        if "order" in request.GET:
            ordering = request.GET["order"]
            if ordering in Badges.allowed_order():
                if ordering in ["username"]:
                    badges = list(badges)
                    aes = AesEncryption()
                    badges = sorted(badges, key=lambda
                        t: t.get_user().surname + t.get_user().name if t.get_user() is not None else "")
                    if "ascending" in request.GET and request.GET["ascending"] == "0":
                        badges = list(reversed(badges))
                else:
                    if "ascending" in request.GET and request.GET["ascending"] == "0":
                        ordering = "-" + ordering
                    badges = badges.order_by(ordering)

        (badges, page, max_page) = get_pagination_api(request.GET, badges)

        badgesSer = []
        for badge in badges:
            badgesSer.append(badge_serializer(badge, aes=aes))

        return HttpResponse(json.dumps({"badges": badgesSer, "page": page, "max_page": max_page}),
                            content_type="application/json")


class BadgeGet(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(100, "Invalid rack")
        get = request.GET

        if not args_in_array(['rfid'], get):
            return get_error(101, "Missing arguments")

        badge = Badges.objects.filter(rfid=get['rfid'])
        if badge.count() == 0:
            return get_error(106, "Missing badge")
        badge = badge.first()

        return HttpResponse(json.dumps(badge_serializer(badge)), content_type="application/json")


class BadgeEdit(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BadgeEdit, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(100, "Invalid rack")
        post = request.POST
        if not args_in_array(['user'], post):
            return get_error(101, "Missing arguments")

        guser = Users.objects.filter(id=post['user'], ihm_password__isnull=True)
        if guser.count() == 0:
            return get_error(102, "User not found")
        guser = guser.first()

        operation = post['operation']
        if operation == "attribution":
            form = EditBadgeAttribForm(request.POST)
            if not form.is_valid():
                return get_error(108, "Data invalid")
            form.save(guser)
            log_rack(1, rack,
                     "{} has assigned a badge {}".format(
                         guser.get_full_name(),
                         form.badge.get_printed()))
            return HttpResponse(json.dumps(badge_serializer(form.badge)))
        elif operation == "rename":
            form = EditBadgeNameForm(request.POST)
            if not form.is_valid():
                return get_error(108, "Data invalid")
            form.save(guser)
            log_rack(1, rack,
                     "{} has renamed a badge {}".format(
                         guser.get_full_name(),
                         form.badge.get_printed()))
            return HttpResponse(json.dumps(badge_serializer(form.badge)))
        elif operation == "delete":
            form = EditBadgeDeleteForm(request.POST)
            if not form.is_valid():
                return get_error(109, "Data invalid")
            log_rack(1, rack,
                     "{} has deleted a badge {}".format(
                         guser.get_full_name(),
                         form.badge.get_printed()))
            form.save()
            return get_error(110, "Operation complete")
        else:
            return get_error(107, "Data invalid")


class BadgeNew(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BadgeNew, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(100, "Invalid rack")
        post = request.POST
        if not args_in_array(['user'], post):
            return get_error(101, "Missing arguments")

        guser = Users.objects.filter(id=post['user'])
        if guser.count() == 0:
            return get_error(102, "User not found")
        guser = guser.first()

        form = AddBadgeForm(request.POST)

        if not form.is_valid():
            return get_error(105, "Data invalid")

        if Badges.objects.filter(rfid=form.cleaned_data['rfid']).count() > 0:
            return get_error(106, "Badge already exists")

        form.save(guser)

        log_rack(1, rack,
                 "{} has added a new badge {}".format(guser.get_full_name(), form.badge.get_printed()))

        return HttpResponse(json.dumps(badge_serializer(form.badge)))


class MessageList(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(100, "Invalid rack")

        if not args_in_array(['user'], request.GET):
            return get_error(101, "Missing arguments")

        guser = Users.objects.filter(id=request.GET['user'], ihm_password__isnull=True)
        if guser.count() == 0:
            return get_error(102, "User not found")
        guser = guser.first()

        today = timezone.now()
        messages = list(UserMessages.objects.filter(user=guser, read=None, end=None).all()) + list(
            UserMessages.objects.filter(user=guser, start__lte=today, end__isnull=False, end__gte=today).all())

        messageSer = []
        aes = AesEncryption()
        for message in messages:
            messageSer.append(message_serializer(message, aes=aes))

        return HttpResponse(json.dumps({"messages": messageSer}),
                            content_type="application/json")


class MessageRead(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(MessageRead, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(100, "Invalid rack")
        post = request.POST
        if not args_in_array(['message'], post):
            return get_error(101, "Missing arguments")

        message = UserMessages.objects.filter(id=post['message'])
        if message.count() == 0:
            return get_error(102, "Message not found")
        message = message.first()
        message.read = timezone.now()
        message.save()

        return HttpResponse(json.dumps(message_serializer(message)), content_type="application/json")
