import json

from django.db.models import Q
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from extranet import settings
from website.apps.api.serializers.products import product_serializer, case_serializer, kit_serializer, \
    incident_serializer, rack_serializer, product_type_serializer
from website.apps.api.serializers.user import user_serializer
from website.apps.api.views.api import get_error, args_in_array, auth_rack, get_user
from website.apps.web.encryption import AesEncryption
from website.apps.web.forms_kiosk import DeclareIncidentForm
from website.apps.web.forms_products import KitRemoveForm, CaseSwitchForm, KitLogForm, KitAddForm, KitAssignForm, \
    IncidentSolvedForm, ProductSetRfidForm
from website.apps.web.mail_util import send_incident
from website.apps.web.models import Products, Cases, KitAssignations, Kits, ProductIncidents, KitLogs, Racks
# ERROR CODES 200 - 199
# ERROR CODES 200 - 299
from website.apps.web.util import get_pagination_api, log_rack


class ProductData(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)

        if rack is None:
            return get_error(200, "Invalid rack")

        get = request.GET

        if not args_in_array(['product'], get):
            return get_error(201, "Missing arguments")

        val = Products.objects.filter(id=get['product'])
        if val.count() == 0:
            return get_error(202, "Product not found")
        val = val.first()

        return HttpResponse(json.dumps(product_serializer(val, False)), content_type="application/json")


class ProductDataRfid(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")
        get = request.GET

        if not args_in_array(['rfid'], get):
            return get_error(201, "Missing arguments")

        val = Products.objects.filter(id_tag=get['rfid'])
        if val.count() == 0:
            return get_error(202, "Product not found")
        val = val.first()

        return HttpResponse(json.dumps(product_serializer(val, False)), content_type="application/json")


class ProductSetRfid(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductSetRfid, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")
        user = get_user(request)
        if user is None:
            return get_error(201, "Missing arguments")

        form = ProductSetRfidForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_rack(1, rack,
                 user.get_full_name() + " has edited a product rfid : " + form.product.get_printed())

        return HttpResponse(json.dumps(product_serializer(form.product, basic=False, aes=AesEncryption())),
                            content_type="application/json")


class ProductList(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)

        if rack is None:
            return get_error(200, "Invalid rack")

        aes = AesEncryption()
        products = Products.objects
        if "filter" in request.GET:
            filter = str(request.GET["filter"])
            if len(filter) > 0:
                spl = filter.split("&")
                for s in spl:
                    vals = [s]
                    if "=" in s:
                        vals = s.split("=")
                    elif "~" in s:
                        vals = s.split("~")

                    if len(vals) == 1:
                        if s.lower() == "nokit":
                            products = products.filter(kit__isnull=True)
                        elif s.lower() == "norfid":
                            products = products.filter(id_tag__isnull=True)
                    elif len(vals) == 2:
                        if "family" in vals[0] and len(vals[1]) > 2:
                            ls = vals[1][1:-1].split(",")
                            for fam in ls:
                                if fam != "":
                                    products = products.filter(~Q(product_type__family__id=fam))

        if "order" in request.GET:
            ordering = request.GET["order"]
            if ordering in Products.allowed_order():
                if "ascending" in request.GET and request.GET["ascending"] == "0":
                    ordering = "-" + ordering
                products = products.order_by(ordering)

        (products, page, max_page) = get_pagination_api(request.GET, products.all())
        productsSer = []
        types = []
        for lproduct in products.all():
            if lproduct.product_type not in types:
                types.append(lproduct.product_type)
            productsSer.append(product_serializer(lproduct, aes=aes))

        typesSer = []
        for type in types:
            typesSer.append(product_type_serializer(type, aes=aes))

        return HttpResponse(
            json.dumps({"products": productsSer, "types": typesSer, "page": page, "max_page": max_page}),
            content_type="application/json")


class KitLog(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(KitLog, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")
        user = get_user(request)
        if user is None:
            return get_error(201, "Missing arguments")

        form = KitLogForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        return HttpResponse(json.dumps(kit_serializer(form.kit)), content_type="application/json")


class KitRemove(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(KitRemove, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")
        user = get_user(request)
        if user is None:
            return get_error(201, "Missing arguments")

        form = KitRemoveForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        kitstr = form.kit.get_printed()

        if not form.save(user):
            return get_error(206, "Cannot set data")

        log_rack(2, rack, user.get_full_name() + " has removed a kit : " + kitstr)

        return HttpResponse(json.dumps({"deleted": True}), content_type="application/json")


class CaseSwitchStatus(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CaseSwitchStatus, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")
        user = get_user(request)
        if user is None:
            return get_error(201, "Missing arguments")

        form = CaseSwitchForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_rack(1, rack,
                 "{} has edited the rack {} set case status to {} on {}".format(user.get_full_name(),
                                                                                form.case.rack.get_printed(),
                                                                                str(form.case.active),
                                                                                form.case.position))

        return HttpResponse(json.dumps(case_serializer(form.case)), content_type="application/json")


class ProductRackCaseAddKit(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductRackCaseAddKit, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")
        user = get_user(request)
        if user is None:
            return get_error(201, "Missing arguments")

        form = KitAddForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_rack(1, rack,
                 "{} has edited the rack {} set case kit to {} on {}".format(user.get_full_name(),
                                                                             form.case.rack.get_printed(),
                                                                             form.kit.get_printed(),
                                                                             form.case.position))

        return HttpResponse(json.dumps(case_serializer(form.case)), content_type="application/json")


class CaseDataFromKit(View):
    def get(self, request, **kwargs):
        get = request.GET
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")

        if not args_in_array(['kit'], get):
            return get_error(201, "Missing arguments")

        val = Cases.objects.filter(kit__id=get['kit'])
        if val.count() == 0:
            return get_error(208, "Case not found")
        val = val.first()

        return HttpResponse(json.dumps(case_serializer(val)), content_type="application/json")


class DeclareIncident(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(DeclareIncident, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")

        form = DeclareIncidentForm(request.POST)
        if not form.is_valid():
            return get_error(201, "Invalid data !")
        form.save()

        if settings.EMAIL_ACTIVATED:
            send_incident(form.product, form.incident, request=request)

        return HttpResponse(json.dumps({"valid": True}), content_type="application/json")


class ProductKitData(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")
        get = request.GET

        if not args_in_array(['product'], get) and not args_in_array(['kit'], get):
            return get_error(201, "Missing arguments")

        if "kit" in get:
            val = Kits.objects.filter(id=get['kit'])
            if val.count() == 0:
                return get_error(207, "Kit not found")
            val = val.first()
        else:
            val = Products.objects.filter(id=get['product'])
            if val.count() == 0:
                return get_error(202, "Product not found")
            val = val.first()

        return HttpResponse(json.dumps(kit_serializer(val if "kit" in get else val.kit)),
                            content_type="application/json")


class KitAssignList(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")
        get = request.GET

        if not args_in_array(['kit'], get):
            return get_error(201, "Missing arguments")

        val = Kits.objects.filter(id=get['kit'])
        if val.count() == 0:
            return get_error(206, "Kit not found")
        val = val.first()

        assigned = KitAssignations.objects.filter(kit=val)
        usersSer = []
        aes = AesEncryption()
        for assign in assigned:
            usersSer.append(user_serializer(assign.user, aes=aes))

        return HttpResponse(json.dumps({"users": usersSer}), content_type="application/json")


class KitAssignSet(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(KitAssignSet, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")
        user = get_user(request)
        if user is None:
            return get_error(201, "Missing arguments")

        form = KitAssignForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_rack(1, rack,
                 "{} has edited the kit {} set assignation to {}".format(user.get_full_name(),
                                                                         form.kit.get_printed(),
                                                                         "False" if not form.assignable else "True [{}]".format(
                                                                             form.kit.get_printed_assigned())))

        return HttpResponse(json.dumps(kit_serializer(form.kit)), content_type="application/json")


class ProductIncidentList(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")
        get = request.GET

        if not args_in_array(['product'], get):
            return get_error(201, "Missing arguments")

        val = Products.objects.filter(id=get['product'])
        if val.count() == 0:
            return get_error(202, "Product not found")
        val = val.first()

        incidentSer = []
        aes = AesEncryption()
        for incident in ProductIncidents.objects.filter(product=val).order_by("solved"):
            incidentSer.append(incident_serializer(incident, aes=aes))

        return HttpResponse(json.dumps({"incidents": incidentSer}), content_type="application/json")


class ProductIncidentSolve(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductIncidentSolve, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")
        user = get_user(request)
        if user is None:
            return get_error(201, "Missing arguments")

        form = IncidentSolvedForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        return HttpResponse(json.dumps(incident_serializer(form.incident)), content_type="application/json")


class KitLocateUser(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")
        get = request.GET

        if not args_in_array(['kit'], get):
            return get_error(201, "Missing arguments")

        val = Kits.objects.filter(id=get['kit'])
        if val.count() == 0:
            return get_error(206, "Kit not found")
        val = val.first()

        user = None

        last_kit = KitLogs.objects.filter(kit=val).order_by("-id")
        if last_kit.count() > 0:
            last_kit = last_kit.first()
            if last_kit.out == 1:
                user = last_kit.user

        return HttpResponse(json.dumps(user_serializer(user, False) if user else {}), content_type="application/json")


class ProductRacks(View):
    def get(self, request, **kwargs):
        rack = auth_rack(request)
        if rack is None:
            return get_error(200, "Invalid rack")

        aes = AesEncryption()
        racks = Racks.objects.order_by("number").all()
        racksSer = []
        for rack in racks.all():
            racksSer.append(rack_serializer(rack, aes=aes))

        return HttpResponse(json.dumps({"racks": racksSer}), content_type="application/json")
