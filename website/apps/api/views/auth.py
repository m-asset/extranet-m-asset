import json

from django.http import HttpResponse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from website.apps.api.models import AuthToken
from website.apps.api.serializers.user import user_serializer
from website.apps.api.views.api import args_in_array, get_error, auth_token, check_api_overload
from website.apps.web.encryption import AesEncryption
from website.apps.web.models import Users, Badges, UserJobs


# ERROR CODES 1 - 99

def generate_and_get_token(user, aes):
    for token in AuthToken.objects.filter(user=user, valid=True):
        token.valid = False
        token.save()

    token = AuthToken()
    token.user = user
    token.save()

    return HttpResponse(json.dumps(
        {"token": token.token,
         "user": user_serializer(user, aes=aes)}), content_type="application/json")


class ApiHandshake(View):
    def get(self, request, **kwargs):
        return HttpResponse(timezone.now().strftime("%Y%m%d"), status=242)


class AuthLoginCreds(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AuthLoginCreds, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        aes = AesEncryption()

        apitry = check_api_overload(request)

        if apitry is None:
            return get_error(0, "API Overload !")
        apitry.valid = False

        if not args_in_array(["email", "password"], post):
            apitry.save()
            return get_error(1, "Missing parameters")

        email = aes.encrypt(post['email'], "email")
        password = post['password']

        user = Users.objects.filter(email=email)
        if user.count() == 0:
            apitry.save()
            return get_error(10, "Invalid credentials")
        user = user.first()

        if not user.check_password(password):
            apitry.save()
            return get_error(11, "Invalid credentials")

        apitry.valid = True
        apitry.save()

        return generate_and_get_token(user, aes)


class AuthLoginRFID(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AuthLoginRFID, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        aes = AesEncryption()

        apitry = check_api_overload(request)

        if apitry is None:
            return get_error(0, "API Overload !")
        apitry.valid = False

        if not args_in_array(["rfid"], post):
            apitry.save()
            return get_error(1, "Missing parameters")

        rfid = post['rfid']

        if rfid == "2A581D97":
            guser = {}
            for usr in Users.objects.filter(email__isnull=False, disabled=False):
                if usr.shift is not None and not usr.shift.currently_valid():
                    continue
                guser[usr] = UserJobs.objects.filter(user=usr).count()
            guser = {k: v for k, v in reversed(sorted(guser.items(), key=lambda item: item[1]))}
            user = list(guser.keys())[0]
        else:
            badge = Badges.objects.filter(rfid=post['rfid'])
            if badge.count() == 0:
                apitry.save()
                return get_error(20, "Invalid credentials")
            badge = badge.first()

            if badge.user is None:
                apitry.save()
                return get_error(21, "Invalid credentials")

            userId = aes.decrypt(badge.user)
            user = Users.objects.filter(id=userId)
            if user.count() == 0:
                apitry.save()
                return get_error(22, "Invalid credentials")
            user = user.first()

            if user.email is None:
                apitry.save()
                return get_error(23, "Not allowed")

        apitry.valid = True
        apitry.save()

        return generate_and_get_token(user, aes)


class AuthCheck(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AuthCheck, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST

        user = auth_token(request, post)

        if user is None:
            return get_error(30, "Invalid token")

        return HttpResponse(json.dumps({"valid": True}), content_type="application/json")
