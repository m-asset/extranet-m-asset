import json
import select
import socket

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views import View
# ERROR CODES 200 - 199
from django.views.decorators.csrf import csrf_exempt

from extranet import settings
from website.apps.api.serializers.products import product_serializer, product_type_serializer, \
    product_family_serializer, kit_serializer, rack_serializer, case_serializer, incident_serializer
from website.apps.api.serializers.user import user_serializer
from website.apps.api.views.api import auth_token, get_error, args_in_array
from website.apps.web.encryption import AesEncryption
from website.apps.web.forms_products import KitAddForm, AddProductFamilyForm, AddProductTypeForm, AddProductForm, \
    CaseOpenForm, KitLogForm, KitRemoveForm, CaseSwitchForm, KitAssignForm, IncidentSolvedForm, ProductSetRfidForm, \
    EditProductForm, EditProductFamilyForm, IncidentCommentForm
from website.apps.web.mail_util import send_incident_update
from website.apps.web.models import Products, ProductModel, ProductFamily, Racks, Kits, KitAssignations, Cases, KitLogs, \
    ProductIncidents
# ERROR CODES 200 - 299
from website.apps.web.util import get_pagination_api, log_android


class ProductList(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        aes = AesEncryption()
        products = Products.objects.all()

        if "order" in request.GET:
            ordering = request.GET["order"]
            if ordering in Products.allowed_order():
                if "ascending" in request.GET and request.GET["ascending"] == "0":
                    ordering = "-" + ordering
                products = products.order_by(ordering)

        (products, page, max_page) = get_pagination_api(request.GET, products)
        productsSer = []
        for lproduct in products.all():
            productsSer.append(product_serializer(lproduct, aes=aes))

        return HttpResponse(json.dumps({"products": productsSer, "page": page, "max_page": max_page}),
                            content_type="application/json")


class ProductData(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        if not args_in_array(['product'], get):
            return get_error(201, "Missing arguments")

        val = Products.objects.filter(id=get['product'])
        if val.count() == 0:
            return get_error(202, "Product not found")
        val = val.first()

        return HttpResponse(json.dumps(product_serializer(val, False)), content_type="application/json")


class ProductDataRfid(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        if not args_in_array(['rfid'], get):
            return get_error(201, "Missing arguments")

        val = Products.objects.filter(id_tag=get['rfid'])
        if val.count() == 0:
            return get_error(202, "Product not found")
        val = val.first()

        return HttpResponse(json.dumps(product_serializer(val, False)), content_type="application/json")


class ProductKitData(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        if not args_in_array(['product'], get) and not args_in_array(['kit'], get):
            return get_error(201, "Missing arguments")

        if "kit" in get:
            val = Kits.objects.filter(id=get['kit'])
            if val.count() == 0:
                return get_error(207, "Kit not found")
            val = val.first()
        else:
            val = Products.objects.filter(id=get['product'])
            if val.count() == 0:
                return get_error(202, "Product not found")
            val = val.first()

        return HttpResponse(json.dumps(kit_serializer(val if "kit" in get else val.kit)),
                            content_type="application/json")


class ProductNew(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductNew, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = AddProductForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_android(1, request,
                    user.get_full_name() + " has added a product : " + form.product.get_printed())

        return HttpResponse(json.dumps(product_serializer(form.product, basic=False)), content_type="application/json")


class ProductEdit(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductEdit, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = EditProductForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_android(2, request,
                    "{} has edited the product {}".format(user.get_full_name(),
                                                          form.product.get_printed()))

        return HttpResponse(json.dumps(product_serializer(form.product, basic=False)), content_type="application/json")


class ProductTypeList(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        aes = AesEncryption()
        productTypes = ProductModel.objects.all()

        if "order" in request.GET:
            ordering = request.GET["order"]
            if ordering in ProductModel.allowed_order():
                if "ascending" in request.GET and request.GET["ascending"] == "0":
                    ordering = "-" + ordering
                productTypes = productTypes.order_by(ordering)

        (productTypes, page, max_page) = get_pagination_api(request.GET, productTypes)
        productTypesSer = []
        for lprod_type in productTypes.all():
            productTypesSer.append(product_type_serializer(lprod_type, aes=aes))

        return HttpResponse(json.dumps({"product_types": productTypesSer, "page": page, "max_page": max_page}),
                            content_type="application/json")


class ProductTypeNew(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductTypeNew, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = AddProductTypeForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_android(1, request,
                    user.get_full_name() + " has added a product model : " + form.type.get_printed())

        return HttpResponse(json.dumps(product_type_serializer(form.type)), content_type="application/json")


class ProductTypeData(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        if not args_in_array(['model'], get):
            return get_error(201, "Missing arguments")

        val = ProductModel.objects.filter(id=get['model'])
        if val.count() == 0:
            return get_error(202, "Product model not found")
        val = val.first()

        return HttpResponse(json.dumps(product_type_serializer(val)), content_type="application/json")


class ProductFamilyList(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        aes = AesEncryption()
        productFamilies = ProductFamily.objects.all()
        productFamiliesSer = []
        for lprod_family in productFamilies.all():
            productFamiliesSer.append(product_family_serializer(lprod_family, aes=aes))

        return HttpResponse(json.dumps({"product_families": productFamiliesSer}), content_type="application/json")


class ProductFamilyData(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        if not args_in_array(['family'], get):
            return get_error(201, "Missing arguments")

        val = ProductFamily.objects.filter(id=get['family'])
        if val.count() == 0:
            return get_error(202, "Product not found")
        val = val.first()

        return HttpResponse(json.dumps(product_family_serializer(val)), content_type="application/json")


class ProductFamilyNew(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductFamilyNew, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = AddProductFamilyForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_android(1, request,
                    user.get_full_name() + " has added a product family : " + form.family.name)

        return HttpResponse(json.dumps(product_family_serializer(form.family)), content_type="application/json")


class ProductFamilyEdit(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductFamilyEdit, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = EditProductFamilyForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_android(2, request,
                    user.get_full_name() + " has edited a product family : " + form.family.name)

        return HttpResponse(json.dumps(product_family_serializer(form.family)), content_type="application/json")


class ProductRacks(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        aes = AesEncryption()
        racks = Racks.objects.order_by("number").all()
        racksSer = []
        for rack in racks.all():
            racksSer.append(rack_serializer(rack, aes=aes))

        return HttpResponse(json.dumps({"racks": racksSer}), content_type="application/json")


class ProductRackCaseAddKit(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductRackCaseAddKit, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = KitAddForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_android(1, request,
                    "{} has edited the rack {} set case kit to {} on {}".format(user.get_full_name(),
                                                                                form.case.rack.get_printed(),
                                                                                form.kit.get_printed(),
                                                                                form.case.position))

        return HttpResponse(json.dumps(case_serializer(form.case)), content_type="application/json")


class KitData(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        if not args_in_array(['kit'], get):
            return get_error(201, "Missing arguments")

        val = Kits.objects.filter(id=get['kit'])
        if val.count() == 0:
            return get_error(206, "Kit not found")
        val = val.first()

        return HttpResponse(json.dumps(kit_serializer(val)), content_type="application/json")


class CaseDataFromKit(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        if not args_in_array(['kit'], get):
            return get_error(201, "Missing arguments")

        val = Cases.objects.filter(kit__id=get['kit'])
        if val.count() == 0:
            return get_error(208, "Case not found")
        val = val.first()

        return HttpResponse(json.dumps(case_serializer(val)), content_type="application/json")


class CaseOpen(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CaseOpen, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = CaseOpenForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        UDP_IP = form.case.rack.ip_address
        UDP_PORT = 40200
        door_id = str(form.case.position)
        if len(door_id) == 1:
            door_id = "0" + door_id
        MESSAGE = ("open_door {}".format(door_id)).encode()

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))

        ready = select.select([sock], [], [], 5)
        data, addr = (None, None)
        if ready[0]:
            data, addr = sock.recvfrom(1024)
        i = 0
        while data is not None and i < len(data) and data[i] != 0:
            i += 1
        if i > 0:
            data = data[0:i]

        data = data.decode() if data is not None else ""

        resp = "undefined"
        if data == "OK":
            resp = "ok"
        elif data == "Erreur":
            resp = "error"

        return HttpResponse(json.dumps({"response": resp}), content_type="application/json")


class KitAssignList(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        if not args_in_array(['kit'], get):
            return get_error(201, "Missing arguments")

        val = Kits.objects.filter(id=get['kit'])
        if val.count() == 0:
            return get_error(206, "Kit not found")
        val = val.first()

        assigned = KitAssignations.objects.filter(kit=val)
        usersSer = []
        aes = AesEncryption()
        for assign in assigned:
            usersSer.append(user_serializer(assign.user, aes=aes))

        return HttpResponse(json.dumps({"users": usersSer}), content_type="application/json")


class KitLog(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(KitLog, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = KitLogForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        return HttpResponse(json.dumps(kit_serializer(form.kit)), content_type="application/json")


class KitRemove(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(KitRemove, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = KitRemoveForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        kitstr = form.kit.get_printed()

        if not form.save(user):
            return get_error(206, "Cannot set data")

        log_android(2, request, user.get_full_name() + " has removed a kit : " + kitstr)

        return HttpResponse(json.dumps({"deleted": True}), content_type="application/json")


class CaseSwitchStatus(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CaseSwitchStatus, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = CaseSwitchForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_android(1, request,
                    "{} has edited the rack {} set case status to {} on {}".format(user.get_full_name(),
                                                                                   form.case.rack.get_printed(),
                                                                                   str(form.case.active),
                                                                                   form.case.position))

        return HttpResponse(json.dumps(case_serializer(form.case)), content_type="application/json")


class KitAssignSet(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(KitAssignSet, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = KitAssignForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_android(1, request,
                    "{} has edited the kit {} set assignation to {}".format(user.get_full_name(),
                                                                            form.kit.get_printed(),
                                                                            "False" if not form.assignable else "True [{}]".format(
                                                                                form.kit.get_printed_assigned())))

        return HttpResponse(json.dumps(kit_serializer(form.kit)), content_type="application/json")


class KitLocateUser(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        if not args_in_array(['kit'], get):
            return get_error(201, "Missing arguments")

        val = Kits.objects.filter(id=get['kit'])
        if val.count() == 0:
            return get_error(206, "Kit not found")
        val = val.first()

        user = None

        last_kit = KitLogs.objects.filter(kit=val).order_by("-id")
        if last_kit.count() > 0:
            last_kit = last_kit.first()
            if last_kit.out == 1:
                user = last_kit.user

        return HttpResponse(json.dumps(user_serializer(user, False) if user else {}), content_type="application/json")


class ProductIncidentList(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(200, "Invalid token")

        incidents = ProductIncidents.objects

        if args_in_array(['product'], get):
            val = Products.objects.filter(id=get['product'])
            if val.count() == 0:
                return get_error(202, "Product not found")
            incidents = incidents.filter(product=val.first()).order_by("solved").all()

            (page, max_page) = (1, 1)
        else:
            if "order" in request.GET:
                ordering = request.GET["order"]
                if ordering in ProductIncidents.allowed_order():
                    if "ascending" in request.GET and request.GET["ascending"] == "0":
                        ordering = "-" + ordering
                    incidents = incidents.order_by(ordering)
            else:
                incidents = incidents.order_by("solved")

            (incidents, page, max_page) = get_pagination_api(request.GET, incidents.all())

        incidentSer = []
        aes = AesEncryption()
        for incident in incidents:
            incidentSer.append(incident_serializer(incident, aes=aes))

        return HttpResponse(json.dumps({"incidents": incidentSer, "page": page, "max_page": max_page}),
                            content_type="application/json")


class ProductIncidentSolve(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductIncidentSolve, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = IncidentSolvedForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        return HttpResponse(json.dumps(incident_serializer(form.incident)), content_type="application/json")


class ProductIncidentComment(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductIncidentComment, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = IncidentCommentForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        if settings.EMAIL_ACTIVATED:
            send_incident_update(request, form.incident.product, form.incident, form.comment)

        return HttpResponse(json.dumps(incident_serializer(form.incident)), content_type="application/json")


class ProductSetRfid(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductSetRfid, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(200, "Invalid token")

        form = ProductSetRfidForm(request.POST)

        if not form.is_valid():
            return get_error(205, "Data invalid")

        form.save(user)

        log_android(1, request,
                    user.get_full_name() + " has edited a product rfid : " + form.product.get_printed())

        return HttpResponse(json.dumps(product_serializer(form.product, basic=False, aes=AesEncryption())),
                            content_type="application/json")
