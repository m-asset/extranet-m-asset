# Create your views here.
import json
from datetime import datetime, timedelta

import pytz
from django.http import HttpResponse

from website.apps.api.models import AuthToken, ApiTry
from website.apps.web.models import Racks, Users


def args_in_array(args, array):
    for arg in args:
        if arg not in array:
            return False
    return True


def get_error(error_code, error_message):
    return HttpResponse(json.dumps({"error": error_code, "message": error_message}), content_type="application/json",
                        status=400)


def get_user(request):
    if "user" not in request.GET and "user" not in request.POST:
        return None
    try:
        user = int(request.GET['user']) if "user" in request.GET else int(request.POST['user'])
        user = Users.objects.filter(id=user, ihm_password__isnull=True, disabled=False)
        if user.count() == 0:
            return None
        return user.first()
    except Exception:
        return None


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def check_api_overload(request):
    ip = get_client_ip(request)  # TODO WHITELIST

    if ApiTry.objects.filter(ip=ip, valid=False,
                             time__gte=(datetime.now() - timedelta(minutes=15)).replace(
                                 tzinfo=pytz.utc)).count() > 30:
        return None

    apitry = ApiTry()
    apitry.ip = ip
    apitry.valid = True

    return apitry


def auth_token(request, args):
    apitry = check_api_overload(request)

    if apitry is None:
        return None

    if args_in_array(["token"], args):
        token = args['token']

        authTk = AuthToken.objects.filter(token=token, valid=True)
        if authTk.count() > 0:
            authTk = authTk.first()

            if (datetime.now() - timedelta(hours=3)).replace(tzinfo=pytz.utc) <= authTk.time.replace(tzinfo=pytz.utc):
                apitry.save()
                return authTk.user
            else:
                authTk.valid = False
                authTk.save()
    apitry.valid = False
    apitry.save()

    return None


def auth_rack(request):
    apitry = check_api_overload(request)

    if apitry is None:
        return None

    ip = get_client_ip(request)

    rack = Racks.objects.filter(ip_address=ip)
    if rack.count() > 0:
        apitry.valid = True
        apitry.save()
        return rack.first()

    apitry.valid = False
    apitry.save()

    return None
