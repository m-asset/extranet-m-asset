import json

from django.http import HttpResponse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from website.apps.api.serializers.products import kit_log_serializer
from website.apps.api.serializers.user import user_serializer, shift_serializer, badge_serializer, job_serializer, \
    config_serializer, message_serializer
from website.apps.api.views.api import auth_token, get_error, args_in_array
from website.apps.web.encryption import AesEncryption
from website.apps.web.forms import LogsForm
from website.apps.web.forms_users import AddBadgeForm, AddShiftForm, AddJobForm, AddUserForm, EditBadgeAttribForm, \
    EditUserJobsForm, EditUserShiftForm, EditJobKitForm, EditUserDisabledForm, EditBadgeNameForm, EditBadgeDeleteForm, \
    EditJobForm
from website.apps.web.models import Users, Badges, Jobs, Shifts, UserMessages
# ERROR CODES 100 - 199
from website.apps.web.util import get_pagination_api, log_android


class UserList(View):
    def get(self, request, **kwargs):
        get = request.GET

        user = auth_token(request, get)

        if user is None:
            return get_error(100, "Invalid token")

        aes = AesEncryption()
        users = Users.objects.filter(ihm_password=None).all()

        if "order" in request.GET:
            ordering = request.GET["order"]
            if ordering in Users.allowed_order():
                if ordering in ["name"]:
                    users = list(users)
                    aes = AesEncryption()
                    users = sorted(users, key=lambda
                        t: aes.decrypt(t.surname) + aes.decrypt(t.name))
                    if "ascending" in request.GET and request.GET["ascending"] == "0":
                        users = list(reversed(users))
                else:
                    if "ascending" in request.GET and request.GET["ascending"] == "0":
                        ordering = "-" + ordering
                    users = users.order_by(ordering)

        (users, page, max_page) = get_pagination_api(request.GET, users)

        usersSer = []
        for luser in users:
            usersSer.append(user_serializer(luser, aes=aes))

        return HttpResponse(json.dumps({"users": usersSer, "page": page, "max_page": max_page}),
                            content_type="application/json")


class UserListManagers(View):
    def get(self, request, **kwargs):
        get = request.GET

        user = auth_token(request, get)

        if user is None:
            return get_error(100, "Invalid token")

        aes = AesEncryption()
        users = Users.objects.filter(ihm_password=None, email__isnull=False)
        usersSer = []
        for luser in users.all():
            usersSer.append(user_serializer(luser, aes=aes))

        return HttpResponse(json.dumps({"users": usersSer}), content_type="application/json")


class UserData(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)

        if user is None:
            return get_error(100, "Invalid token")

        if not args_in_array(['user'], get):
            return get_error(101, "Missing arguments")

        guser = Users.objects.filter(id=get['user'], ihm_password__isnull=True)
        if guser.count() == 0:
            return get_error(102, "User not found")
        guser = guser.first()

        return HttpResponse(json.dumps(user_serializer(guser, basic=False)), content_type="application/json")


class UserDataRfid(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)

        if user is None:
            return get_error(100, "Invalid token")

        if not args_in_array(['rfid'], get):
            return get_error(101, "Missing arguments")

        badge = Badges.objects.filter(rfid=get['rfid'])
        if badge.count() == 0:
            return get_error(103, "Invalid rfid")
        badge = badge.first()

        if badge.user is None:
            return get_error(104, "Badge empty")

        aes = AesEncryption()

        guser = Users.objects.filter(id=aes.decrypt(badge.user))
        if guser.count() == 0 or guser.first().ihm_password is not None:
            return get_error(102, "User not found")
        guser = guser.first()

        return HttpResponse(json.dumps(user_serializer(guser, basic=False)), content_type="application/json")


class UserNew(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UserNew, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(100, "Invalid token")

        form = AddUserForm(request.POST)

        if not form.is_valid():
            return get_error(105, "Data invalid")

        form.save(user)

        log_android(1, request,
                    "{} has added the user {}".format(user.get_full_name(),
                                                      form.user.get_full_name()))

        return HttpResponse(json.dumps(user_serializer(form.user)))


class UserEditJobs(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UserEditJobs, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(100, "Invalid token")

        form = EditUserJobsForm(request.POST)

        if not form.is_valid():
            return get_error(105, "Data invalid")

        form.save(user)

        log_android(1, request,
                    "{} has edited the user job for {}, job: {}".format(user.get_full_name(),
                                                                        form.user.get_full_name(),
                                                                        form.user.get_printed_jobs()))

        return HttpResponse(json.dumps(user_serializer(form.user)))


class UserEditShift(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UserEditShift, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(100, "Invalid token")

        form = EditUserShiftForm(request.POST)

        if not form.is_valid():
            return get_error(105, "Data invalid")

        form.save(user)

        log_android(1, request,
                    "{} has edited the user shift for {}, shift: {}".format(user.get_full_name(),
                                                                            form.user.get_full_name(),
                                                                            form.user.shift.get_printed() if form.user.shift else "None"))

        return HttpResponse(json.dumps(user_serializer(form.user)))


class UserEditDisabled(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UserEditDisabled, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(100, "Invalid token")

        form = EditUserDisabledForm(request.POST)

        if not form.is_valid():
            return get_error(105, "Data invalid")

        form.save(user)

        log_android(2, request,
                    "{} has edited the user status for {}, disabled: {}".format(user.get_full_name(),
                                                                                form.user.get_full_name(),
                                                                                str(form.user.disabled)))

        return HttpResponse(json.dumps(user_serializer(form.user)))


class BadgeList(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(100, "Invalid token")

        aes = AesEncryption()
        badges = Badges.objects.all()

        if "order" in request.GET:
            ordering = request.GET["order"]
            if ordering in Badges.allowed_order():
                if ordering in ["username"]:
                    badges = list(badges)
                    aes = AesEncryption()
                    badges = sorted(badges, key=lambda
                        t: t.get_user().surname + t.get_user().name if t.get_user() is not None else "")
                    if "ascending" in request.GET and request.GET["ascending"] == "0":
                        badges = list(reversed(badges))
                else:
                    if "ascending" in request.GET and request.GET["ascending"] == "0":
                        ordering = "-" + ordering
                    badges = badges.order_by(ordering)

        (badges, page, max_page) = get_pagination_api(request.GET, badges)

        badgesSer = []
        for badge in badges:
            badgesSer.append(badge_serializer(badge, aes=aes))

        return HttpResponse(json.dumps({"badges": badgesSer, "page": page, "max_page": max_page}),
                            content_type="application/json")


class ShiftList(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(100, "Invalid token")

        shifts = Shifts.objects.all()
        shiftsSer = []
        for shift in shifts.all():
            shiftsSer.append(shift_serializer(shift))

        return HttpResponse(json.dumps({"shifts": shiftsSer}), content_type="application/json")


class JobList(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(100, "Invalid token")

        jobs = Jobs.objects.all()
        (jobs, page, max_page) = get_pagination_api(request.GET, jobs)
        jobsSer = []
        for job in jobs.all():
            jobsSer.append(job_serializer(job))

        return HttpResponse(json.dumps({"jobs": jobsSer, "page": page, "max_page": max_page}),
                            content_type="application/json")


class BadgeNew(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BadgeNew, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(100, "Invalid token")

        form = AddBadgeForm(request.POST)

        if not form.is_valid():
            return get_error(105, "Data invalid")

        if Badges.objects.filter(rfid=form.cleaned_data['rfid']).count() > 0:
            return get_error(106, "Badge already exists")

        form.save(user)

        log_android(1, request,
                    "{} has added a new badge {}".format(user.get_full_name(), form.badge.get_printed()))

        return HttpResponse(json.dumps(badge_serializer(form.badge)))


class ShiftNew(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ShiftNew, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(100, "Invalid token")

        form = AddShiftForm(request.POST)

        if not form.is_valid():
            return get_error(105, "Data invalid")

        form.save(user)

        log_android(1, request,
                    "{} has added the shift {}".format(user.get_full_name(),
                                                       form.shift.get_printed()))

        return HttpResponse(json.dumps(shift_serializer(form.shift)))


class JobNew(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(JobNew, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(100, "Invalid token")

        form = AddJobForm(request.POST)

        if not form.is_valid():
            return get_error(105, "Data invalid")

        form.save(user)

        log_android(1, request,
                    "{} has added the job {}".format(user.get_full_name(),
                                                     form.job.get_printed()))

        return HttpResponse(json.dumps(job_serializer(form.job)))


class JobEdit(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(JobEdit, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(100, "Invalid token")

        form = EditJobForm(request.POST)

        if not form.is_valid():
            return get_error(105, "Data invalid")

        form.save(user)

        log_android(1, request,
                    "{} has edited the job {}".format(user.get_full_name(),
                                                      form.job.get_printed()))

        return HttpResponse(json.dumps(job_serializer(form.job)))


class JobEditKit(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(JobEditKit, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(100, "Invalid token")

        form = EditJobKitForm(request.POST)

        if not form.is_valid():
            return get_error(105, "Data invalid")

        form.save(user)

        log_android(1, request,
                    "{} has edited the job families of {}, families: [{}]".format(user.get_full_name(),
                                                                                  form.job.get_printed(),
                                                                                  form.job.get_families_printed()))

        return HttpResponse(json.dumps(job_serializer(form.job)))


class BadgeGet(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(100, "Invalid token")

        if not args_in_array(['rfid'], get):
            return get_error(101, "Missing arguments")

        badge = Badges.objects.filter(rfid=get['rfid'])
        if badge.count() == 0:
            return get_error(106, "Missing badge")
        badge = badge.first()

        return HttpResponse(json.dumps(badge_serializer(badge)), content_type="application/json")


class BadgeEdit(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BadgeEdit, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST
        user = auth_token(request, post)
        if user is None:
            return get_error(100, "Invalid token")

        operation = post['operation']
        if operation == "attribution":
            form = EditBadgeAttribForm(request.POST)
            if not form.is_valid():
                return get_error(108, "Data invalid")
            form.save(user)
            log_android(1, request,
                        "{} has assigned a badge {}".format(
                            user.get_full_name(),
                            form.badge.get_printed()))
            return HttpResponse(json.dumps(badge_serializer(form.badge)))
        elif operation == "rename":
            form = EditBadgeNameForm(request.POST)
            if not form.is_valid():
                return get_error(108, "Data invalid")
            form.save(user)
            log_android(1, request,
                        "{} has renamed a badge {}".format(
                            user.get_full_name(),
                            form.badge.get_printed()))
            return HttpResponse(json.dumps(badge_serializer(form.badge)))
        elif operation == "delete":
            form = EditBadgeDeleteForm(request.POST)
            if not form.is_valid():
                return get_error(109, "Data invalid")
            log_android(1, request,
                        "{} has deleted a badge {}".format(
                            user.get_full_name(),
                            form.badge.get_printed()))
            form.save()
            return get_error(110, "Operation complete")
        else:
            return get_error(107, "Data invalid")


class GetConfig(View):
    def get(self, request, **kwargs):
        get = request.GET
        user = auth_token(request, get)
        if user is None:
            return get_error(100, "Invalid token")

        return HttpResponse(json.dumps(config_serializer()), content_type="application/json")


class Logs(View):
    def get(self, request, **kwargs):
        get = request.GET

        user = auth_token(request, get)

        if user is None:
            return get_error(100, "Invalid token")

        form = LogsForm(request.GET, android=True)
        form.is_valid()

        (logs, page_count) = form.save()

        kLogs = []
        aes = AesEncryption()
        for log in logs:
            kLogs.append(kit_log_serializer(log, aes=aes))

        return HttpResponse(json.dumps({"logs": kLogs, "page_count": page_count}), content_type="application/json")


class MessageList(View):
    def get(self, request, **kwargs):
        get = request.GET

        user = auth_token(request, get)

        if user is None:
            return get_error(100, "Invalid token")

        today = timezone.now()
        messages = list(UserMessages.objects.filter(user=user, read=None, end=None).all()) + list(
            UserMessages.objects.filter(user=user, start__lte=today, end__isnull=False, end__gte=today).all())

        messageSer = []
        aes = AesEncryption()
        for message in messages:
            messageSer.append(message_serializer(message, aes=aes))

        return HttpResponse(json.dumps({"messages": messageSer}),
                            content_type="application/json")


class MessageRead(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(MessageRead, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        post = request.POST

        user = auth_token(request, post)

        if user is None:
            return get_error(100, "Invalid token")

        if not args_in_array(['message'], post):
            return get_error(101, "Missing arguments")

        message = UserMessages.objects.filter(id=post['message'])
        if message.count() == 0:
            return get_error(102, "Message not found")
        message = message.first()
        message.read = timezone.now()
        message.save()

        return HttpResponse(json.dumps(message_serializer(message)), content_type="application/json")
