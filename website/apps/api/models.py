import secrets

from django.db import models
from django.db.models import Model

from website.apps.web.models import Users


def auth_token_gen():
    ide = secrets.token_hex(128)
    while AuthToken.objects.filter(token=ide).exists():
        ide = secrets.token_hex(128)
    return ide


class AuthToken(Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(Users, on_delete=models.CASCADE, related_name='+')
    ip = models.CharField(max_length=128)
    token = models.CharField(max_length=256, default=auth_token_gen)
    valid = models.BooleanField(default=True)
    time = models.DateTimeField(auto_now_add=True)


class ApiTry(Model):
    id = models.BigAutoField(primary_key=True)
    ip = models.CharField(max_length=128)
    valid = models.BooleanField()
    time = models.DateTimeField(auto_now_add=True)


class UserPasswordUpdate(Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(Users, on_delete=models.CASCADE, related_name='+')
    temp = models.BooleanField(default=False)
    time = models.DateTimeField(auto_now_add=True)
