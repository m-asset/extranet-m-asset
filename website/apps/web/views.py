# Create your views here.
import os

from django.contrib.auth import login, logout
from django.db import connection
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.utils import timezone
from django.views import View
from django.views.generic import FormView

from extranet import settings
from website.apps.api.models import UserPasswordUpdate
from website.apps.api.views.api import args_in_array
from website.apps.web.config import MaxPasswordDays
from website.apps.web.forms import LoginForm, ChangePasswordForm, LogsForm, StatsForm, EditCompanyLogoForm, \
    AddDocumentForm
from website.apps.web.models import Shifts, UserMessages, KitLogs, UserAdminLogs, UserLogs, ProductIncidents, \
    LogsCasesOpen, LogsProductTransaction, LogsKitTransaction, ProductJobOut, LogsForensic, ProductIncidentsComment
from website.apps.web.stats import get_user_statistics, get_product_field, get_product_usage, get_product_family_usage
from website.apps.web.util import get_pages, log_web, collect_logs_as_content

REDIRECT_IF_LOGGED = '/'
REDIRECT_IF_NOT_LOGGED = '/login'


def is_pre_log(request):
    b = request.user.is_authenticated
    if not b:
        return False
    if request.user.email is None:
        logout(request)
        return False
    return True


def is_post_log(request):
    if request.user.ihm_password is not None:
        return True
    lastlogpass = UserPasswordUpdate.objects.filter(user=request.user).order_by("-time")
    if lastlogpass.count() > 0:
        lastlogpass = lastlogpass.first()
        if lastlogpass.temp:
            return False
        if lastlogpass.time < timezone.now() - timezone.timedelta(days=MaxPasswordDays()):
            return False
    return True


def is_logged(request):
    if not is_pre_log(request):
        return False
    return is_post_log(request)


def not_logged_url(request):
    return REDIRECT_IF_NOT_LOGGED + "?next=" + request.path


class Index(View):
    def get(self, request):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        today = timezone.now()
        messages = list(UserMessages.objects.filter(user=request.user, read=None, end=None).all()) + list(
            UserMessages.objects.filter(user=request.user, start__lte=today, end__isnull=False, end__gte=today).all())
        for message in messages:
            message.message = message.message.replace("\n", "<br>")

        return render(request, 'index.html',
                      {"graph_user": get_user_statistics(), "graph_product_field": get_product_field(),
                       "graph_product_usage": get_product_usage(), "graph_family_usage": get_product_family_usage(),
                       "messages": messages})

    def post(self, request):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        if args_in_array(["mark_read", "message"], request.POST):
            message = UserMessages.objects.filter(id=request.POST["message"])
            if message.count() > 0:
                message = message.first()
                message.read = timezone.now()
                message.save()

        return redirect("/")


class Login(FormView):
    def get(self, request, **kwargs):
        next = ""
        if "next" in request.GET:
            next = "?next=" + request.GET['next']

        if is_pre_log(request):
            if not is_post_log(request):
                form = ChangePasswordForm()
                return render(request, 'user/login_reset_pass.html', {'form': form, "next": next})
            else:
                return redirect(REDIRECT_IF_LOGGED)

        form = LoginForm()
        return render(request, 'user/login.html', {'form': form, "next": next})

    def post(self, request, **kwargs):
        next = ""
        if "next" in request.GET:
            next = "?next=" + request.GET['next']

        if is_pre_log(request):
            if not is_post_log(request):
                form = ChangePasswordForm(request.POST)

                if form.is_valid():
                    user = request.user
                    user.set_password(form.cleaned_data['password'])
                    user.save()
                    update = UserPasswordUpdate()
                    update.user = user
                    update.temp = False
                    update.save()

                    log_web(1, request, user.get_full_name() + " has changed his password")

                    return redirect(REDIRECT_IF_LOGGED)

                return render(request, 'user/login_reset_pass.html', {'form': form, "next": next})
            else:
                return redirect(REDIRECT_IF_LOGGED)

        form = LoginForm(request.POST)
        if form.is_valid():
            login(request, form.get_user())
            log_web(1, request, form.get_user().get_full_name() + " logged successfully")
            if request.GET:
                nxt = request.GET['next']
                if nxt:
                    return HttpResponseRedirect(nxt)
            return HttpResponseRedirect(REDIRECT_IF_LOGGED)
        return render(request, 'user/login.html', {'form': form, "next": next})


class Stats(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        form = StatsForm(request.GET)
        form.is_valid()

        stats = form.save()

        return render(request, 'stats.html',
                      {"start": form.start, "end": form.end, "stats": stats})


class Logs(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        get = request.GET

        form = LogsForm(request.GET)
        form.is_valid()

        (logs, page_count) = form.save()

        return render(request, 'logs.html',
                      {"start": form.start, "end": form.end, "args": get, "shifts": Shifts.objects.all(), "logs": logs,
                       "sel_shift": form.shift.id if form.shift else 0, "pages": get_pages(form.page, page_count),
                       "max_page": page_count, "page": form.page})


class RequestLogForensic(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        path = os.path.join(settings.BASE_DIR, "static/log")
        if not os.path.lexists(path):
            os.makedirs(path)
        logfile = os.path.join(path, "forensic_log.tsv")

        with open(logfile, "w") as file:
            file.write(collect_logs_as_content())

        return redirect("/static/log/forensic_log.tsv")


class LogsClean(View):
    data = [{
        "short_name": "kit_logs",
        "name": "Kit logs",
        "description": "Entries of all products in and out of the racks",
        "model": [KitLogs, ProductJobOut],
    }, {
        "short_name": "admin_logs",
        "name": "Admin logs",
        "description": "Every admin connection on a Kiosk",
        "model": [UserAdminLogs],
    }, {
        "short_name": "user_logs",
        "name": "User logs",
        "description": "Every user connection on a Kiosk",
        "model": [UserLogs],
    }, {
        "short_name": "user_messages",
        "name": "User messages",
        "description": "Every user message",
        "model": [UserMessages],
    }, {
        "short_name": "product_incidents",
        "name": "Equipment incidents",
        "description": "Equipment incidents and comments",
        "model": [ProductIncidentsComment, ProductIncidents],
    }, {
        "short_name": "general_logs",
        "name": "Cases, Kits and Equipement logs",
        "description": "Logs used for statistical information",
        "model": [LogsProductTransaction, LogsKitTransaction, LogsCasesOpen],
    }, {
        "short_name": "forensic_logs",
        "name": "Forensic logs",
        "description": "Logs used to track every action on the system",
        "model": [LogsForensic]
    }]

    def calculate_size(self, model):
        db_size_req = "SELECT TABLE_NAME AS `name`, ROUND((DATA_LENGTH + INDEX_LENGTH) / 1024) AS `size` " \
                      "FROM information_schema.TABLES WHERE TABLE_SCHEMA = \"myasset\"  AND TABLE_NAME = \"{}\"" \
                      "ORDER BY (DATA_LENGTH + INDEX_LENGTH) DESC;".format(model.objects.model._meta.db_table)

        with connection.cursor() as cursor:
            cursor.execute(db_size_req)
            row = cursor.fetchone()

        return model.objects.count(), int(row[1])

    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)
        if request.user.ihm_password is None:
            return redirect(REDIRECT_IF_LOGGED)

        now = timezone.now()
        bef_3_months = now - timezone.timedelta(days=90)
        bef_6_months = now - timezone.timedelta(days=180)

        data = self.data
        for i in range(len(data)):
            content = data[i]
            size = 0
            count = 0
            three_month = 0
            six_month = 0
            for model in content["model"]:
                (lcount, lsize) = self.calculate_size(model)
                count += lcount
                size += lsize
                three_month += model.objects.filter(creation_timestamp__lte=bef_3_months).count()
                six_month += model.objects.filter(creation_timestamp__lte=bef_6_months).count()
            content["size"] = "{} Ko".format(size) if size < 1024 else "{} Mo".format(round(size / 1024, 1))
            content["count"] = count
            content["threemonth"] = three_month
            content["sixmonth"] = six_month
            data[i] = content

        return render(request, 'logs_clean.html', {"data": data})

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)
        if request.user.ihm_password is None:
            return redirect(REDIRECT_IF_LOGGED)

        now = timezone.now()
        bef_3_months = now - timezone.timedelta(days=90)
        bef_6_months = now - timezone.timedelta(days=180)

        for i in range(len(self.data)):
            content = self.data[i]

            if content["short_name"] in request.POST:
                val = request.POST[content["short_name"]]

                if val == "3months":
                    for model in content["model"]:
                        model.objects.filter(creation_timestamp__lte=bef_3_months).delete()
                elif val == "6months":
                    for model in content["model"]:
                        model.objects.filter(creation_timestamp__lte=bef_6_months).delete()

        return redirect("/logs/clean/")


class EditCompanyLogo(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)
        if request.user.ihm_password is None:
            return redirect(REDIRECT_IF_LOGGED)

        redir = request.GET["redirect"] if "redirect" in request.GET else REDIRECT_IF_LOGGED
        return redirect(redir)

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)
        if request.user.ihm_password is None:
            return redirect(REDIRECT_IF_LOGGED)

        redir = request.GET["redirect"] if "redirect" in request.GET else REDIRECT_IF_LOGGED

        form = EditCompanyLogoForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()

        return redirect(redir)


class Docs(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        path = os.path.join(settings.BASE_DIR, "static/docs")
        if not os.path.lexists(path):
            os.makedirs(path)

        files = []
        for file in os.listdir(path):
            if file.endswith(".pdf"):
                files.append(file)

        return render(request, 'docs.html', {"files": files})

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)
        if request.user.ihm_password is None:
            return redirect(REDIRECT_IF_LOGGED)

        path = os.path.join(settings.BASE_DIR, "static/docs")

        if "remove_document" in request.POST:
            form = None
            file = os.path.join(path, request.POST["remove_document"])
            if os.path.lexists(file):
                os.remove(file)
        else:
            form = AddDocumentForm(request.POST, request.FILES)

        if form and form.is_valid():
            form.save()

        return redirect("/docs/")
