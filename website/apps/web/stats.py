from math import ceil

from django.utils import timezone
from django.utils.translation import gettext as _

from extranet import settings
from website.apps.web.models import UserLogs, Products, Kits, KitLogs, ProductFamily, Cases


class GraphData:
    def __init__(self, labels, data):
        self.labels = labels
        self.data = data


def get_time_values():
    end = timezone.now()
    start = end - timezone.timedelta(days=14)
    start = start.replace(hour=0, minute=0, second=0)
    mid = start + timezone.timedelta(days=1)
    return start, mid, end


def get_user_statistics():
    start, mid, end = get_time_values()

    labels = []
    data = []

    while start < end:
        labels.append(start.strftime("%d/%m"))
        users = []
        for log in UserLogs.objects.filter(creation_timestamp__gt=start, creation_timestamp__lte=mid):
            if log.user.id not in users:
                users.append(log.user.id)
        data.append(len(users))

        start += timezone.timedelta(days=1)
        mid += timezone.timedelta(days=1)

    return GraphData(labels, data)


def get_product_field():
    labels = [_("Available"), _("Disabled"), _("Unavailable")]

    query = Products.objects
    products = query.count()
    kits = {}
    for prod in query.filter(kit__isnull=False):
        kit = prod.kit.id
        c = 0
        if kit in kits:
            c = kits[kit]
        kits[kit] = c + 1
    inkits = 0
    disabled = 0
    for kit in kits:
        case = Cases.objects.filter(kit__id=kit, active=True)
        if case.count() > 0:
            inkits += kits[kit]
        else:
            disabled += kits[kit]
    available = inkits * 100 // products if products > 0 else 0
    disabledP = disabled * 100 // products if products > 0 else 0

    data = [available, disabledP, 100 - available - disabledP]

    return GraphData(labels, data)


def get_product_usage():
    start, mid, end = get_time_values()

    labels = []
    data = []

    while start < end:
        labels.append(start.strftime("%d/%m"))

        query = Products.objects.filter(creation_timestamp__lte=mid)
        products = query.count()
        products_used = 0
        for kit in Kits.objects.filter(creation_timestamp__lte=mid):
            out = KitLogs.objects.filter(creation_timestamp__gt=start, creation_timestamp__lte=mid, out=True,
                                         kit=kit).count() > 0
            if out:
                products_used += Products.objects.filter(kit=kit).count()

        data.append(products_used * 100 // products if products > 0 else 0)

        start += timezone.timedelta(days=1)
        mid += timezone.timedelta(days=1)

    return GraphData(labels, data)


def get_product_family_usage():
    labels = []
    data = []
    for family in ProductFamily.objects.all():
        val = 0
        count = 0
        for product in Products.objects.filter(product_type__family=family):
            val += product.used
            count += 1
        labels.append(family.name)
        data.append(val // count if count > 0 else 0)

    return GraphData(labels, data)


def get_logs(start=None, end=None, unfinished=False, shift=None, page=1, android=False):
    objs = KitLogs.objects
    if start:
        objs = objs.filter(creation_timestamp__gte=start)
    if end:
        objs = objs.filter(creation_timestamp__lte=end)

    if shift:
        objs = objs.filter(user__shift=shift)

    objs = objs.order_by("-creation_timestamp")

    if unfinished:
        tobjs = KitLogs.objects.filter(out=True)
        objs = []
        for obj in tobjs:
            if KitLogs.objects.filter(kit=obj.kit, out=False,
                                      creation_timestamp__gt=obj.creation_timestamp).count() == 0:
                objs.append(obj)
        count = len(objs)
    else:
        count = objs.count()

    max_show = settings.ANDROID_MAX_LINES if android else settings.MAX_LOG_COUNT

    return objs[(page - 1) * max_show:page * max_show], ceil(count / max_show)
