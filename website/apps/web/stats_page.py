from django.utils import timezone
from django.utils.translation import gettext

from website.apps.web import models
from website.apps.web.models import UserLogs, KitLogs, LogsCasesOpen, LogsKitTransaction, Racks, Cases, ProductIncidents


class GraphData:
    def __init__(self, labels, data):
        self.labels = labels
        self.data = data


def get_stats(start, end):
    return {"daily_activity": get_daily_activity(start, end), "family_usage": get_family_usage(start, end),
            "rack_usage": get_rack_usage(start, end), "rack_aff_usage": get_rack_usage(start, end, True),
            "cases_usage": get_case_usage(start, end), "incidents_data": get_incidents(start, end)}


def get_daily_activity(start, end):
    data = {}

    nav = start
    while nav < end:
        nex = nav + timezone.timedelta(minutes=30)
        ldata = []
        label = nav.strftime("%H:%M")
        if label in data:
            ldata = data[label]

        users = []
        try:
            for log in UserLogs.objects.filter(creation_timestamp__gte=nav,
                                               creation_timestamp__lt=nex):
                if log.user.id not in users:
                    users.append(log.user.id)
        except Exception:
            pass
        ldata.append(len(users))
        data[label] = ldata

        nav = nex

    labels = []
    dataout = []
    for label in data:
        val = 0
        dt = data[label]
        for v in dt:
            val += v
        labels.append(label)
        dataout.append(round(val / len(dt), 2))
    return GraphData(labels, dataout)


def get_family_usage(start, end):
    labels = []

    data = {}
    maxvals = {}
    families = models.ProductFamily.objects.all()
    for fam in families:
        data[fam.name] = []
        maxvals[fam.name] = models.Products.objects.filter(product_type__family=fam).count()

    nav = start
    while nav < end:
        nex = nav + timezone.timedelta(days=1)
        labels.append(nav.strftime("%d/%m"))

        ldata = {}

        try:
            for log in KitLogs.objects.filter(creation_timestamp__gt=nav, creation_timestamp__lte=nex, out=True):
                for product in log.kit.get_products():
                    family = product.product_type.family.name
                    tbl = []
                    if family in ldata:
                        tbl = ldata[family]
                    if product.id in tbl:
                        continue
                    tbl.append(product.id)
                    ldata[family] = tbl
        except Exception:
            pass

        for family in families:
            famname = family.name
            val = len(ldata[famname]) if famname in ldata else 0
            total = maxvals[famname]
            data[famname].append(round(val * 100 / total, 2) if total > 0 else 0)

        nav = nex

    return GraphData(labels, data)


def get_rack_usage(start, end, affected=False):
    labels = ["{}h00".format(i if i > 9 else "0{}".format(i)) for i in range(24)]

    assigned_txt = " " + gettext("Assigned")

    racks = {}
    for case_log in LogsCasesOpen.objects.filter(creation_timestamp__gt=start, creation_timestamp__lte=end):
        if case_log.rack not in racks:
            racks[case_log.rack] = [0] * 24
            if affected:
                racks[case_log.rack + assigned_txt] = [0] * 24

    nracks = {}
    for k in sorted(racks.keys()):
        nracks[k] = racks[k]
    racks = nracks

    nav = start
    while nav < end:
        nex = nav + timezone.timedelta(days=1)

        try:
            for case_log in LogsCasesOpen.objects.filter(creation_timestamp__gt=nav, creation_timestamp__lte=nex):
                time = timezone.localtime(case_log.creation_timestamp)
                index = int(time.strftime("%H"))

                if not affected:
                    racks[case_log.rack][index] += 1
                else:
                    kit_log = LogsKitTransaction.objects.filter(log_case=case_log)
                    aff = False
                    if kit_log.count() > 0:
                        aff = kit_log.first().assigned == 1

                    rack = case_log.rack
                    if aff:
                        rack += assigned_txt
                    racks[case_log.rack][index] += 1
        except Exception:
            pass

        nav = nex

    return GraphData(labels, racks)


def get_case_usage(start, end):
    racks = {}

    for rack in Racks.objects.order_by("number"):
        racks[rack.name] = {}
        for case in Cases.objects.filter(rack=rack).order_by("position"):
            racks[rack.name][case] = (0, 0)

    nav = start
    while nav < end:
        nex = nav + timezone.timedelta(days=1)

        for rack in racks:
            cases = racks[rack]

            for case in cases:
                (total, count) = cases[case]
                try:
                    ntotal = total + LogsCasesOpen.objects.filter(rack=rack, case=case.position,
                                                                  creation_timestamp__gt=nav,
                                                                  creation_timestamp__lte=nex).count()
                    cases[case] = (ntotal, count + 1 if ntotal != total else count)
                except Exception:
                    pass

        nav = nex

    data = {}

    txt_total_open = gettext("Total openings")
    txt_moy = gettext("Average openings per day")

    for rack in racks:
        labels = []
        dt = {txt_total_open: [], txt_moy: []}
        cases = racks[rack]
        for case in cases:
            status = gettext("Unused") if case.kit is None else (
                gettext("Inactive") if not case.active else gettext("Active"))
            labels.append([gettext("Case ") + str(case.position), status])
            (total, count) = cases[case]
            dt[txt_total_open].append(total)
            dt[txt_moy].append(round(total / count, 2) if count > 0 else 0)

        data[rack] = GraphData(labels, dt)

    return data


def get_incidents(start, end):
    labels = []

    txt_incidents = gettext("Incidents unsolved")
    txt_incidents_solved = gettext("Incidents solved")
    data = {txt_incidents: [], txt_incidents_solved: []}

    nav = start
    while nav < end:
        nex = nav + timezone.timedelta(days=1)
        labels.append(nav.strftime("%d/%m"))

        ldata = {txt_incidents: 0, txt_incidents_solved: 0}

        try:
            for incident in ProductIncidents.objects.filter(creation_timestamp__gt=start, creation_timestamp__lte=nex):
                ldata[txt_incidents_solved if incident.solved == 2 and incident.update_timestamp.replace(
                    tzinfo=start.tzinfo) < nex else txt_incidents] += 1
        except Exception:
            pass

        data[txt_incidents].append(ldata[txt_incidents])
        data[txt_incidents_solved].append(ldata[txt_incidents_solved])

        nav = nex

    return GraphData(labels, data)
