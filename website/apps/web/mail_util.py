from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.translation import gettext

from extranet import settings
from website.apps.web.encryption import AesEncryption
from website.apps.web.models import Users
from website.apps.web.util import collect_logs_as_content


def send_reset_password(request, user, temp_pass):
    if not settings.EMAIL_ACTIVATED:
        return
    mail_subject = "[MyAsset] " + gettext('Reset password')
    message = render_to_string('mail/reset_password.html', {
        'user': user,
        'domain': request.get_host(),
        'temp_pass': temp_pass,
        'url': "https://" + request.get_host() + '/login/',
    })
    to_email = AesEncryption().decrypt(user.email)
    email = EmailMessage(
        mail_subject, message, to=[to_email]
    )
    email.content_subtype = "html"
    return email.send()


def send_incident(product, incident, request=None):
    if not settings.EMAIL_ACTIVATED:
        return
    mail_subject = "[MyAsset] " + gettext('Equipment incident')
    message = render_to_string('mail/product_incident.html', {
        'product': product,
        'domain': request.get_host() if request else None,
        'incident': incident,
        'url': "https://" + request.get_host() + "/products/" + str(product.id) + "/" if request else None,
    })
    aes = AesEncryption()
    emails = []
    for user in Users.objects.filter(email__isnull=False):
        emails.append(aes.decrypt(user.email))
    email = EmailMessage(
        mail_subject, message, bcc=emails
    )
    email.content_subtype = "html"
    email.send()


def send_incident_update(request, product, incident, update):
    if not settings.EMAIL_ACTIVATED:
        return
    mail_subject = "[MyAsset] [Update]" + gettext('Equipment incident')
    message = render_to_string('mail/product_incident_update.html', {
        'product': product,
        'domain': request.get_host(),
        'incident': incident,
        'update': update,
        'url': "https://" + request.get_host() + "/products/" + str(product.id) + "/",
    })
    aes = AesEncryption()
    emails = []
    for user in Users.objects.filter(email__isnull=False):
        emails.append(aes.decrypt(user.email))
    email = EmailMessage(
        mail_subject, message, bcc=emails
    )
    email.content_subtype = "html"
    email.send()


def send_report():
    if not settings.EMAIL_ACTIVATED:
        return
    mail_subject = "[MyAsset] Log report"
    message = render_to_string('mail/log_report.html')
    email = EmailMessage(
        mail_subject, message, to=[settings.EMAIL_ADMIN]
    )
    email.content_subtype = "html"
    email.attach("log_report.txt", collect_logs_as_content(), "text/plain")
    email.send()
