from functools import wraps
from urllib.parse import urlparse

from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.views import redirect_to_login
from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url


def login_required(view, login_url=None, redirect_field_name=REDIRECT_FIELD_NAME):
    @wraps(view)
    def _wrapped_view(request, *args, **kwargs):
        path = request.build_absolute_uri()
        resolved_login_url = resolve_url(login_url or settings.LOGIN_URL)
        login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
        current_scheme, current_netloc = urlparse(path)[:2]
        if ((not login_scheme or login_scheme == current_scheme) and
                (not login_netloc or login_netloc == current_netloc)):
            path = request.get_full_path()
        if not request.user.is_authenticated:
            return redirect_to_login(path, resolved_login_url, redirect_field_name)

        if not request.user.profile_valid and not path[:-1] == settings.LOGIN_REDIRECT_URL:
            return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)

        return view(request, *args, **kwargs)

    return _wrapped_view
