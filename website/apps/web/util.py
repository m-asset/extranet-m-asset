from math import ceil

from django.utils import timezone

from extranet import settings
from website.apps.api.views.api import get_client_ip
from website.apps.web.models import LogsForensic


def get_pages(page, page_count):
    pages = []

    if page_count > 8:
        pages.append(1)
        pages.append(2)
        if page > 5:
            pages.append("...")
        for i in range(max(3, page - 2), min(page + 3, page_count - 1)):
            pages.append(i)
        if page < page_count - 4:
            pages.append("...")
        for i in range(page_count - 1, page_count + 1):
            pages.append(i)
    else:
        for i in range(1, page_count + 1):
            pages.append(i)
    return pages


def get_pagination(request, query):
    count = query.count() if type(query) != list else len(query)

    page = 1
    if "page" in request.GET:
        try:
            page = int(request.GET['page'])
        except Exception:
            pass
        if page < 1:
            page = 1

    max_lines = settings.MAX_LINES
    start = (page - 1) * max_lines
    if start >= count:
        start = 0
        page = 1
    end = page * max_lines

    max_page = ceil(count / max_lines)
    pages = get_pages(page, max_page)

    return query[start:end], page, max_page, pages


def get_pagination_api(req_args, query):
    count = len(query) if type(query) == list else query.count()

    page = 1
    if "page" in req_args:
        try:
            page = int(req_args['page'])
        except Exception:
            pass
        if page < 1:
            page = 1

    max_lines = settings.ANDROID_MAX_LINES
    start = (page - 1) * max_lines
    if start >= count:
        start = 0
        page = 1
    end = page * max_lines

    max_page = ceil(count / max_lines)

    return query[start:end], page, max_page


def log_android(level, request, log):
    log_action(level, "Android (" + get_client_ip(request) + ")", log)


def log_web(level, request, log):
    log_action(level, "Web (" + get_client_ip(request) + ")", log)


def log_rack(level, rack, log):
    log_action(level, "Kiosk " + rack.name, log)


def log_action(level, source, log):
    logF = LogsForensic()
    logF.level = level
    logF.source = source
    logF.log = log
    logF.creation_timestamp = timezone.now()
    logF.save()


def level_to_text(level):
    if level == 2:
        return "WARN"
    elif level == 3:
        return "ERRO"
    return "INFO"


def format_source(source):
    max_len = 25
    return source + (" " * (max_len - len(source)))


def collect_logs_as_content():
    content = ""
    start = timezone.now() - timezone.timedelta(days=60)
    formatted = {}
    for log in LogsForensic.objects.filter(creation_timestamp__gt=start).order_by("-creation_timestamp")[:1000000]:
        source = log.source
        if source in formatted:
            source = formatted[source]
        else:
            formatt = format_source(source)
            formatted[source] = formatt
            source = formatt
        content += "{}\t{}\t{}\t{}\n".format(log.creation_timestamp.astimezone(timezone.get_current_timezone()),
                                             level_to_text(log.level), source, log.log)
    return content
