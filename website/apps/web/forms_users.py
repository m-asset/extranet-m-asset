import uuid

from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.forms import Form, CharField, BooleanField, TimeField, EmailField, IntegerField, FileField, JSONField, \
    DateField
from django.utils import timezone

from website.apps.api.views.api import args_in_array
from website.apps.web.encryption import AesEncryption
from website.apps.web.models import Jobs, Shifts, Badges, Users, UserJobs, ProductFamily, KitTypes, \
    KitTypeProductFamily, KitAssignations, Kits, ProductJobOut, UserMessages


class AddUserForm(Form):
    surname = CharField(required=True, min_length=2, max_length=512)
    name = CharField(required=True, min_length=2, max_length=512)
    email = EmailField(required=False)
    id_internal_company = CharField(required=True, min_length=2, max_length=512)
    manager = IntegerField(required=True, min_value=0)
    shift = IntegerField(required=True, min_value=0)

    def __init__(self, data):
        super().__init__(data)
        self.user = None
        self.manager = None
        self.shift = None

    def clean(self):
        super(Form, self).clean()

        if self.cleaned_data['manager'] > 0:
            manager = Users.objects.filter(id=self.cleaned_data['manager'], email__isnull=False,
                                           ihm_password__isnull=True)

            if manager.count() == 0:
                raise ValidationError("")
            self.manager = manager.first()

        if self.cleaned_data['shift'] > 0:
            shift = Shifts.objects.filter(id=self.cleaned_data['shift'])

            if shift.count() == 0:
                raise ValidationError("")
            self.shift = shift.first()

    def save(self, user):
        aes = AesEncryption()
        auser = Users()
        auser.id_internal_company = self.cleaned_data['id_internal_company']
        email_edit = "email" in self.cleaned_data and len(self.cleaned_data['email']) > 0
        auser.email = aes.encrypt(self.cleaned_data['email'], "email") if email_edit else None
        auser.surname = aes.encrypt(self.cleaned_data['surname'])
        auser.name = aes.encrypt(self.cleaned_data['name'])
        auser.disabled = False
        auser.manager = self.manager
        auser.lang = "fr"
        auser.shift = self.shift
        auser.creation_user = user
        auser.update_user = user
        auser.creation_timestamp = timezone.now()
        auser.update_timestamp = timezone.now()
        auser.save()
        self.user = auser


class RemoveUserForm(Form):
    user = IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.user = None

    def clean(self):
        super(Form, self).clean()

        user = Users.objects.filter(id=self.cleaned_data['user'])
        if user.count() == 0:
            raise ValidationError("")
        self.user = user.first()

    def save(self, user):
        encid = AesEncryption().encrypt(self.user.id, "rfid")
        self.user.id_internal_user = uuid.uuid4().hex
        while Users.objects.filter(id_internal_company=self.user.id_internal_user).count() > 0:
            self.user.id_internal_user = uuid.uuid4().hex
        self.user.name = "REMOVED"
        self.user.surname = ""
        self.user.email = None
        self.user.update_user = user
        self.user.update_timestamp = timezone.now()
        self.user.password = None
        self.user.ihm_password = "REMOVED"
        self.user.phone = None
        self.user.disabled = True
        self.user.manager = None
        self.user.shift = None
        self.user.save()

        for badges in Badges.objects.filter(user=encid):
            badges.user = None
            badges.update_user = user
            badges.update_timestamp = timezone.now()

        for assign in KitAssignations.objects.filter(user=self.user):
            assign.delete()

        for userjob in UserJobs.objects.filter(user=self.user):
            userjob.delete()


class ImportUsersForm(Form):
    config = FileField(required=True)

    def __init__(self, data=None, files=None):
        super().__init__(data, files)
        self.users = []
        self.import_errors = []

    def clean(self):
        super(Form, self).clean()

        if self.cleaned_data['config'] is None:
            raise ValidationError("")

    def save(self, admin):
        if self.cleaned_data['config'].multiple_chunks(chunk_size=10000000):
            return
        data = self.cleaned_data['config'].readlines()

        aes = AesEncryption()
        later = []
        added = []
        for index in range(len(data)):
            line = data[index]
            lineIndex = index + 1
            user_data = line.decode().rstrip().lstrip().split(";")
            if 4 > len(user_data) > 5:
                self.import_errors.append("Line {}, data is invalid !".format(lineIndex))
                continue

            id_internal_company = user_data[0]
            email = user_data[1]
            surname = user_data[2]
            name = user_data[3]
            manager = user_data[4] if len(user_data) == 5 else None

            if Users.objects.filter(id_internal_company=id_internal_company).count() > 0:
                self.import_errors.append("Line {}, internal id already exists !".format(lineIndex))
                continue

            if len(email) > 0:
                try:
                    validate_email(email)
                except Exception:
                    self.import_errors.append("Line {}, email is not valid !".format(lineIndex))
                    continue

            if len(email) > 0 and Users.objects.filter(email=aes.encrypt(email, "email")).count() > 0:
                self.import_errors.append("Line {}, email already exists !".format(lineIndex))
                continue

            if manager:
                try:
                    validate_email(manager)
                except Exception:
                    self.import_errors.append("Line {}, manager email is not valid !".format(lineIndex))
                    continue

            user_data = {"id_internal_company": id_internal_company,
                         "email": email if len(email) > 0 else None,
                         "surname": surname,
                         "name": name
                         }
            if manager:
                user_data["manager"] = manager

            if "manager" in user_data and Users.objects.filter(
                    email=aes.encrypt(user_data['manager'], "email")).count() == 0:
                user_data["line"] = lineIndex
                later.append(user_data)
                continue

            added.append(self.save_user(admin, user_data, aes=aes))

        for user_data in later:
            if Users.objects.filter(email=aes.encrypt(user_data['manager'], "email")).count() == 0:
                self.import_errors.append("Line {}, manager not found !".format(user_data["line"]))
                continue

            added.append(self.save_user(admin, user_data, aes=aes))

        return added

    def save_user(self, admin, user_data, aes=AesEncryption()):
        user = Users()
        user.id_internal_company = user_data['id_internal_company']
        user.email = aes.encrypt(user_data['email'], "email") if user_data["email"] is not None else None
        user.surname = aes.encrypt(user_data['surname'])
        user.name = aes.encrypt(user_data['name'])
        user.disabled = False
        user.manager = Users.objects.filter(
            email=aes.encrypt(user_data['manager'], "email")) if "manager" in user_data else None
        user.lang = "fr"
        user.shift = None
        user.creation_user = admin
        user.update_user = admin
        user.creation_timestamp = timezone.now()
        user.update_timestamp = timezone.now()
        user.save()

        return user


class AddJobForm(Form):
    name = CharField(required=True, min_length=2, max_length=512)
    assign = BooleanField(required=False)

    def __init__(self, data):
        super().__init__(data)
        self.job = None

    def save(self, user):
        jobs = Jobs()
        jobs.name = self.cleaned_data['name']
        jobs.assign = "assign" in self.cleaned_data and self.cleaned_data['assign'] != "0" and self.cleaned_data[
            'assign']
        jobs.creation_user = user
        jobs.update_user = user
        jobs.creation_timestamp = timezone.now()
        jobs.update_timestamp = timezone.now()
        jobs.save()
        self.job = jobs


class EditJobForm(Form):
    job = IntegerField(required=True)
    name = CharField(required=True, min_length=2, max_length=512)
    assign = BooleanField(required=False)

    def __init__(self, data):
        super().__init__(data)
        self.job = None

    def clean(self):
        super(Form, self).clean()

        if not args_in_array(["job", "name"], self.cleaned_data):
            raise ValidationError("")

        job = Jobs.objects.filter(id=self.cleaned_data["job"])
        if job.count() == 0:
            raise ValidationError("")
        self.job = job.first()

    def save(self, user):
        jobs = self.job
        jobs.name = self.cleaned_data['name']
        jobs.assign = "assign" in self.cleaned_data and self.cleaned_data['assign'] != "0" and self.cleaned_data[
            'assign']
        jobs.update_user = user
        jobs.update_timestamp = timezone.now()
        jobs.save()


class AddShiftForm(Form):
    name = CharField(required=True, min_length=2, max_length=512)
    start = TimeField(required=True)
    end = TimeField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.shift = None

    def save(self, user):
        shift = Shifts()
        shift.name = self.cleaned_data['name']
        shift.start = self.cleaned_data['start']
        shift.end = self.cleaned_data['end']
        shift.creation_user = user
        shift.update_user = user
        shift.creation_timestamp = timezone.now()
        shift.update_timestamp = timezone.now()
        shift.save()
        self.shift = shift


class EditShiftForm(Form):
    shift = forms.IntegerField(required=True)
    name = CharField(required=True, min_length=2, max_length=512)
    start = TimeField(required=True)
    end = TimeField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.shift = None

    def clean(self):
        super(Form, self).clean()

        shift = Shifts.objects.filter(id=self.cleaned_data['shift'])
        if shift.count() == 0:
            raise ValidationError("")
        self.shift = shift.first()

    def save(self, user):
        shift = self.shift
        shift.name = self.cleaned_data['name']
        shift.start = self.cleaned_data['start']
        shift.end = self.cleaned_data['end']
        shift.update_user = user
        shift.update_timestamp = timezone.now()
        shift.save()


class DeleteShiftForm(Form):
    shift = forms.IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.shift = None

    def clean(self):
        super(Form, self).clean()

        shift = Shifts.objects.filter(id=self.cleaned_data['shift'])
        if shift.count() == 0:
            raise ValidationError("")
        self.shift = shift.first()

    def save(self, user):
        for us in Users.objects.filter(shift=self.shift):
            us.shift = None
            us.update_user = user
            us.update_timestamp = timezone.now()
            us.save()
        self.shift.delete()


class AddBadgeForm(Form):
    name = CharField(required=True, min_length=2, max_length=512)
    rfid = CharField(required=True, min_length=2, max_length=512)

    def __init__(self, data):
        super().__init__(data)
        self.badge = None

    def save(self, user):
        badge = Badges()
        badge.user = None
        badge.name = self.cleaned_data['name']
        badge.rfid = self.cleaned_data['rfid']
        badge.creation_user = user
        badge.update_user = user
        badge.creation_timestamp = timezone.now()
        badge.update_timestamp = timezone.now()
        badge.save()
        self.badge = badge


class EditBadgeAttribForm(Form):
    badge = IntegerField(required=True)
    user = IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.badge = None
        self.user = None

    def clean(self):
        super(Form, self).clean()

        badge = Badges.objects.filter(id=self.cleaned_data['badge'])
        if badge.count() == 0:
            raise ValidationError("")
        self.badge = badge.first()

        if self.cleaned_data['user'] != -1:
            user = Users.objects.filter(id=self.cleaned_data['user'])
            if user.count() == 0:
                raise ValidationError("")
            self.user = user.first()

    def save(self, user):
        aes = AesEncryption()
        badge = self.badge
        badge.user = aes.encrypt(str(self.user.id), "rfid") if self.user is not None else None
        badge.update_user = user
        badge.update_timestamp = timezone.now()
        badge.save()


class AddMessageForm(Form):
    users = JSONField(required=False)
    user = IntegerField(required=False)
    message = CharField(required=True, min_length=2, max_length=1024)
    recurring = BooleanField(required=False)
    start = DateField(required=True)
    end = DateField(required=False)

    def __init__(self, data):
        super().__init__(data)
        self.message = None
        self.users = []

    def clean(self):
        super(Form, self).clean()

        if "users" in self.cleaned_data:
            users = self.cleaned_data["users"]
            for user in users:
                user = Users.objects.filter(id=user, ihm_password=None)
                if user.count() == 0:
                    raise ValidationError("")
                self.users.append(user.first())
        elif "user" in self.cleaned_data:
            user = Users.objects.filter(id=self.cleaned_data["user"], ihm_password=None)
            if user.count() == 0:
                raise ValidationError("")
            self.users = [user.first()]
        else:
            raise ValidationError("")

        if "recurring" in self.cleaned_data and "end" not in self.cleaned_data:
            raise ValidationError("")

    def save(self, admin):
        messages = []
        for user in self.users:
            message = UserMessages()
            message.user = user
            message.message = self.cleaned_data["message"]
            message.start = self.cleaned_data["start"]
            message.end = self.cleaned_data["end"] if args_in_array(["end", "recurring"], self.cleaned_data) else None
            message.read = None
            message.creation_timestamp = timezone.now()
            message.creation_user = admin
            message.save()
            messages.append(message)
        if len(messages) == 1:
            self.message = messages[0]
        else:
            self.message = messages


class EditMessageForm(Form):
    message = CharField(required=True, min_length=2, max_length=1024)
    recurring = BooleanField(required=False)
    start = DateField(required=True)
    end = DateField(required=False)

    def __init__(self, data, message):
        super().__init__(data)
        self.message = message

    def clean(self):
        super(Form, self).clean()

        if "recurring" in self.cleaned_data and "end" not in self.cleaned_data:
            raise ValidationError("")

    def save(self):
        message = self.message
        message.message = self.cleaned_data["message"]
        message.start = self.cleaned_data["start"]
        message.end = self.cleaned_data["end"] if args_in_array(["end", "recurring"], self.cleaned_data) else None
        message.read = None
        message.save()


class DeleteMessageForm(Form):
    message = forms.IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.message = None

    def clean(self):
        super(Form, self).clean()

        message = UserMessages.objects.filter(id=self.cleaned_data['message'])
        if message.count() == 0:
            raise ValidationError("")
        self.message = message.first()

    def save(self):
        self.message.delete()


class RemoveKitAssignationForm(Form):
    kit = IntegerField(required=True)
    user = IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.kit = None
        self.user = None

    def clean(self):
        super(Form, self).clean()

        kit = Kits.objects.filter(id=self.cleaned_data['kit'])
        if kit.count() == 0:
            raise ValidationError("")
        self.kit = kit.first()

        user = Users.objects.filter(id=self.cleaned_data['user'])
        if user.count() == 0:
            raise ValidationError("")
        self.user = user.first()

    def save(self):
        kitAssign = KitAssignations.objects.filter(kit=self.kit, user=self.user)
        if kitAssign.count() > 0:
            kitAssign.first().delete()


class EditBadgeNameForm(Form):
    badge = IntegerField(required=True)
    name = CharField(required=True, min_length=2, max_length=512)

    def __init__(self, data):
        super().__init__(data)
        self.badge = None

    def clean(self):
        super(Form, self).clean()

        badge = Badges.objects.filter(id=self.cleaned_data['badge'])
        if badge.count() == 0:
            raise ValidationError("")
        self.badge = badge.first()

    def save(self, user):
        badge = self.badge
        badge.name = self.cleaned_data['name']
        badge.update_user = user
        badge.update_timestamp = timezone.now()
        badge.save()


class EditBadgeDeleteForm(Form):
    badge = IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.badge = None

    def clean(self):
        super(Form, self).clean()

        badge = Badges.objects.filter(id=self.cleaned_data['badge'])
        if badge.count() == 0:
            raise ValidationError("")
        self.badge = badge.first()

    def save(self):
        badge = self.badge
        badge.delete()


class EditUserJobsForm(Form):
    user = forms.IntegerField(required=True)
    jobs = forms.JSONField(required=False)

    def __init__(self, data):
        super().__init__(data)
        self.user = None
        self.jobList = None

    def clean(self):
        super(Form, self).clean()

        user = Users.objects.filter(id=self.cleaned_data['user'])
        if user.count() == 0:
            raise ValidationError("")
        self.user = user.first()

        jobList = []
        if "jobs" in self.cleaned_data:
            for jobId in self.cleaned_data['jobs']:
                job = Jobs.objects.filter(id=jobId)
                if job.count() == 0:
                    raise ValidationError("")
                jobList.append(job.first())
        self.jobList = jobList

    def save(self, user):
        noadd = []
        for ujob in UserJobs.objects.filter(user=self.user):
            present = False
            for job in self.jobList:
                if ujob.job.id == job.id:
                    present = True
                    noadd.append(job)
                    break
            if not present:
                ujob.delete()

        toadd = []
        for job in self.jobList:
            present = False
            for njob in noadd:
                present = present or njob.id == job.id
            if not present:
                toadd.append(job)

        for job in toadd:
            ujob = UserJobs()
            ujob.user = self.user
            ujob.job = job
            ujob.creation_user = user
            ujob.update_user = user
            ujob.creation_timestamp = timezone.now()
            ujob.update_timestamp = timezone.now()
            ujob.save()


class EditUserShiftForm(Form):
    user = forms.IntegerField(required=True)
    shift = forms.IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.user = None
        self.shift = None

    def clean(self):
        super(Form, self).clean()

        user = Users.objects.filter(id=self.cleaned_data['user'])
        if user.count() == 0:
            raise ValidationError("")
        self.user = user.first()

        if self.cleaned_data['shift'] != 0:
            shift = Shifts.objects.filter(id=self.cleaned_data['shift'])
            if shift.count() == 0:
                raise ValidationError("")
            self.shift = shift.first()

    def save(self, user):
        euser = self.user
        euser.shift = self.shift
        euser.update_user = user
        euser.update_timestamp = timezone.now()
        euser.save()


class EditJobKitForm(Form):
    job = forms.IntegerField(required=True)
    families = forms.JSONField(required=False)

    def __init__(self, data):
        super().__init__(data)
        self.job = None
        self.families = None

    def clean(self):
        super(Form, self).clean()

        job = Jobs.objects.filter(id=self.cleaned_data['job'])
        if job.count() == 0:
            raise ValidationError("")
        self.job = job.first()

        familyList = []
        if "families" in self.cleaned_data:
            for familyId in self.cleaned_data['families']:
                family = ProductFamily.objects.filter(id=familyId)
                if family.count() == 0:
                    raise ValidationError("")
                familyList.append(family.first())
        self.families = familyList

    def save(self, user):
        job = self.job
        job.update_user = user
        job.update_timestamp = timezone.now()

        if not self.families:
            job.kit_type = None
            job.save()
            return

        familiesLen = len(self.families)
        familiesIds = []
        for family in self.families:
            familiesIds.append(family.id)

        final_kit_type = None
        for kit_type in KitTypes.objects.all():
            ktpfList = KitTypeProductFamily.objects.filter(kit_type=kit_type)
            if len(ktpfList) == len(self.families):
                valid = []
                for ktpf in ktpfList:
                    if ktpf.product_family.id in familiesIds:
                        valid.append(ktpf.product_family.id)
                if len(valid) == familiesLen:
                    final_kit_type = kit_type
                    break

        if final_kit_type is None:
            final_kit_type = KitTypes()
            final_kit_type.creation_user = user
            final_kit_type.update_user = user
            final_kit_type.creation_timestamp = timezone.now()
            final_kit_type.update_timestamp = timezone.now()
            final_kit_type.save()

            for family in self.families:
                ktpf = KitTypeProductFamily()
                ktpf.product_family = family
                ktpf.kit_type = final_kit_type
                ktpf.creation_user = user
                ktpf.creation_timestamp = timezone.now()
                ktpf.save()

        job.kit_type = final_kit_type
        job.save()


class DeleteJobForm(Form):
    job = forms.IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.job = None

    def clean(self):
        super(Form, self).clean()

        job = Jobs.objects.filter(id=self.cleaned_data['job'])
        if job.count() == 0:
            raise ValidationError("")
        self.job = job.first()

    def save(self):
        for userjob in UserJobs.objects.filter(job=self.job):
            userjob.delete()
        for pjobout in ProductJobOut.objects.filter(job=self.job):
            pjobout.delete()
        self.job.delete()


class EditUserDisabledForm(Form):
    user = forms.IntegerField(required=True)
    disabled = forms.BooleanField(required=False)

    def __init__(self, data):
        super().__init__(data)
        self.user = None
        self.disabled = None

    def clean(self):
        super(Form, self).clean()

        user = Users.objects.filter(id=self.cleaned_data['user'])
        if user.count() == 0:
            raise ValidationError("")
        self.user = user.first()

        self.disabled = "disabled" in self.cleaned_data and self.cleaned_data["disabled"]

    def save(self, user):
        euser = self.user

        if euser.id == user.id:
            return
        euser.disabled = self.disabled
        euser.update_user = user
        euser.update_timestamp = timezone.now()
        euser.save()


class ResetPasswordForm(Form):
    user = forms.IntegerField(required=True)
    email = forms.EmailField(required=False)

    def __init__(self, data):
        super().__init__(data)
        self.user = None

    def clean(self):
        super(Form, self).clean()

        user = Users.objects.filter(id=self.cleaned_data['user'])
        if user.count() == 0:
            raise ValidationError("")
        self.user = user.first()

        if self.user.email is None:
            if "email" not in self.cleaned_data:
                raise ValidationError("")


class EditUserForm(Form):
    user = forms.IntegerField(required=True)
    email = EmailField(required=False)
    manager = IntegerField(required=True, min_value=0)

    def __init__(self, data):
        super().__init__(data)
        self.user = None
        self.manager = None

    def clean(self):
        super(Form, self).clean()

        user = Users.objects.filter(id=self.cleaned_data['user'])
        if user.count() == 0:
            raise ValidationError("")
        self.user = user.first()

        if self.cleaned_data['manager'] > 0:
            manager = Users.objects.filter(id=self.cleaned_data['manager'], email__isnull=False,
                                           ihm_password__isnull=True)

            if manager.count() == 0:
                raise ValidationError("")
            self.manager = manager.first()

    def save(self, user):
        aes = AesEncryption()
        email_edit = "email" in self.cleaned_data and len(self.cleaned_data['email']) > 0
        self.user.email = aes.encrypt(self.cleaned_data['email'], "email") if email_edit else None
        self.user.manager = self.manager
        self.user.update_user = user
        self.user.update_timestamp = timezone.now()
        self.user.save()
