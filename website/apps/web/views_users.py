import os
import uuid

from django.shortcuts import redirect, render
from django.utils import timezone
from django.views import View

from extranet import settings
from website.apps.api.models import UserPasswordUpdate
from website.apps.web import models
from website.apps.web.encryption import AesEncryption
from website.apps.web.forms_users import AddJobForm, AddShiftForm, AddUserForm, EditUserJobsForm, EditUserShiftForm, \
    EditUserDisabledForm, ResetPasswordForm, EditUserForm, EditJobKitForm, EditBadgeAttribForm, EditBadgeNameForm, \
    EditBadgeDeleteForm, RemoveUserForm, RemoveKitAssignationForm, DeleteJobForm, DeleteShiftForm, EditShiftForm, \
    EditJobForm, AddBadgeForm, ImportUsersForm, AddMessageForm, DeleteMessageForm, EditMessageForm
from website.apps.web.mail_util import send_reset_password
from website.apps.web.models import UserJobs, KitAssignations
from website.apps.web.util import get_pagination, log_web
from website.apps.web.views import is_logged, REDIRECT_IF_NOT_LOGGED


class Users(View):
    def return_values(self, request, import_errors=None):
        if import_errors is None:
            import_errors = []
        active_only = "active_only" in request.GET
        show_delete = "allow_delete" in request.GET

        shift = None
        if "shift" in request.GET:
            try:
                shift = models.Shifts.objects.filter(id=int(request.GET['shift']))
                if shift.count() == 0:
                    shift = None
                else:
                    shift = shift.first()
            except Exception:
                pass

        users = models.Users.objects.filter(ihm_password=None).order_by("id_internal_company")
        if active_only:
            users = users.filter(disabled=False)
        if shift:
            users = users.filter(shift=shift)
        managers = models.Users.objects.filter(ihm_password__isnull=True, email__isnull=False)
        shifts = models.Shifts.objects.all()

        search = ""
        if "search" in request.GET:
            search = request.GET["search"].lower()
            aes = AesEncryption()
            tusers = []
            for user in users:
                for elem in search.split(" "):
                    if elem in aes.decrypt(user.surname).lower() or elem in aes.decrypt(
                            user.name).lower() or elem in user.id_internal_company.lower() or user.get_badges().filter(
                        rfid__icontains=elem):
                        tusers.append(user)
                        break
            users = tusers

        if "order" in request.GET:
            ordering = request.GET["order"]
            if ordering in models.Users.allowed_order():
                if ordering in ["name", "surname", "email"]:
                    users = list(users)
                    aes = AesEncryption()
                    users = sorted(users, key=lambda t: (aes.decrypt(t.name) if ordering == "name" else (
                        aes.decrypt(t.surname) if ordering == "surname" else aes.decrypt(t.email))))
                    if "orderdesc" in request.GET:
                        users = list(reversed(users))
                else:
                    if "orderdesc" in request.GET:
                        ordering = "-" + ordering
                    users = users.order_by(ordering)

        (users, page, max_page, pages) = get_pagination(request, users)

        all_users = list(models.Users.objects.filter(ihm_password=None))

        return render(request, 'user/users.html', {"users": users, "managers": managers, "shifts": shifts,
                                                   "active_only": active_only, "show_delete": show_delete,
                                                   "page": page, "max_page": max_page, "pages": pages,
                                                   "search": search, "all_users": all_users,
                                                   "import_errors": import_errors})

    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        return self.return_values(request)

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        import_errors = []

        if "delete_user" in request.POST:
            form = RemoveUserForm(request.POST)
        elif "import_users" in request.POST:
            form = ImportUsersForm(request.POST, request.FILES)
        elif "export_users" in request.POST:
            form = None
        else:
            form = AddUserForm(request.POST)

        if form is not None and form.is_valid():
            if "delete_user" in request.POST:
                log_web(2, request,
                        "{} has deleted the user {}".format(request.user.get_full_name(),
                                                            form.user.get_full_name()))
                form.save(request.user)
            elif "import_users" in request.POST:
                added = form.save(request.user)
                ufullname = request.user.get_full_name()
                for user in added:
                    log_web(1, request,
                            "{} has added the user {}".format(ufullname, user.get_full_name()))
                import_errors = form.import_errors
            else:
                form.save(request.user)
                log_web(1, request,
                        "{} has added the user {}".format(request.user.get_full_name(),
                                                          form.user.get_full_name()))
                return redirect("/users/{}/".format(form.user.id))
        elif form is None:
            users_json = []
            aes = AesEncryption()
            for user in models.Users.objects.filter(ihm_password=None):
                if "user_{}".format(user.id) in request.POST:
                    manager = None
                    if user.manager and user.manager.id != user.id:
                        aes.decrypt(user.manager.email)
                    data = {"id_internal_company": user.id_internal_company,
                            "email": aes.decrypt(user.email) if user.email is not None else "",
                            "surname": aes.decrypt(user.surname),
                            "name": aes.decrypt(user.name)
                            }
                    if manager:
                        data["manager"] = manager if manager else ""
                    users_json.append(data)
            if len(users_json) > 0:
                path = os.path.join(settings.BASE_DIR, "static/log")
                if not os.path.lexists(path):
                    os.makedirs(path)
                logfile = os.path.join(path, "exported_users.csv")

                lines = []
                for user in users_json:
                    line = ""
                    for key, value in user.items():
                        line += value + ";"
                    lines.append(line[:-1] + "\n")

                with open(logfile, "w") as file:
                    file.writelines(lines)

                return redirect("/static/log/exported_users.csv")

        return self.return_values(request, import_errors=import_errors)


class Jobs(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        (jobs, page, max_page, pages) = get_pagination(request, models.Jobs.objects.all())

        return render(request, 'user/users_jobs.html',
                      {"jobs": jobs, "families": models.ProductFamily.objects.all(), "page": page, "max_page": max_page,
                       "pages": pages})

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        if "edit_job" in request.POST:
            form = EditJobForm(request.POST)
            if form.is_valid():
                form.save(request.user)
                log_web(1, request,
                        "{} has edited the job {}".format(request.user.get_full_name(),
                                                          form.job.get_printed()))
        elif "edit_families" in request.POST:
            form = EditJobKitForm(request.POST)
            if form.is_valid():
                form.save(request.user)
                log_web(1, request,
                        "{} has edited the job families of {}, families: [{}]".format(request.user.get_full_name(),
                                                                                      form.job.get_printed(),
                                                                                      form.job.get_families_printed()))
        elif "delete_job" in request.POST:
            form = DeleteJobForm(request.POST)
            if form.is_valid():
                log_web(2, request,
                        "{} has deleted the job {}".format(request.user.get_full_name(),
                                                           form.job.get_printed()))
                form.save()
        else:
            form = AddJobForm(request.POST)
            if form.is_valid():
                form.save(request.user)
                log_web(1, request,
                        "{} has added the job {}".format(request.user.get_full_name(),
                                                         form.job.get_printed()))

        (jobs, page, max_page, pages) = get_pagination(request, models.Jobs.objects.all())

        return render(request, 'user/users_jobs.html',
                      {"jobs": jobs, "families": models.ProductFamily.objects.all(), "page": page, "max_page": max_page,
                       "pages": pages})


class Shifts(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)
        return render(request, 'user/users_shifts.html', {"shifts": models.Shifts.objects.all()})

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        if "edit_shift" in request.POST:
            form = EditShiftForm(request.POST)
        elif "delete_shift" in request.POST:
            form = DeleteShiftForm(request.POST)
        else:
            form = AddShiftForm(request.POST)

        if form.is_valid():
            if "edit_shift" in request.POST:
                form.save(request.user)
                log_web(2, request,
                        "{} has edited the shift {}".format(request.user.get_full_name(),
                                                            form.shift.get_printed()))
            elif "delete_shift" in request.POST:
                log_web(2, request,
                        "{} has deleted the shift {}".format(request.user.get_full_name(),
                                                             form.shift.get_printed()))
                form.save(request.user)
            else:
                form.save(request.user)
                log_web(1, request,
                        "{} has added the shift {}".format(request.user.get_full_name(),
                                                           form.shift.get_printed()))

        return render(request, 'user/users_shifts.html', {"shifts": models.Shifts.objects.all()})


class ShiftUserList(View):
    def return_values(self, request):
        shift = None
        if "shift" in request.GET:
            try:
                shift = models.Shifts.objects.filter(id=int(request.GET['shift']))
                if shift.count() > 0:
                    shift = shift.first()
            except Exception:
                pass

        if not shift:
            return redirect("/shifts/")

        users = models.Users.objects.filter(ihm_password=None, shift=shift).order_by("id_internal_company")

        if "order" in request.GET:
            ordering = request.GET["order"]
            if ordering in models.Users.allowed_order():
                if ordering in ["name", "surname", "email"]:
                    users = list(users)
                    aes = AesEncryption()
                    users = sorted(users, key=lambda t: (aes.decrypt(t.name) if ordering == "name" else (
                        aes.decrypt(t.surname) if ordering == "surname" else aes.decrypt(t.email))))
                    if "orderdesc" in request.GET:
                        users = list(reversed(users))
                else:
                    if "orderdesc" in request.GET:
                        ordering = "-" + ordering
                    users = users.order_by(ordering)

        (users, page, max_page, pages) = get_pagination(request, users)

        all_users = list(models.Users.objects.filter(ihm_password=None).exclude(shift=shift))

        return render(request, 'user/users_shifts_list.html',
                      {"users": users, "page": page, "max_page": max_page, "pages": pages,
                       "all_users": all_users, "shift": shift})

    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        return self.return_values(request)

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        if "add_user" in request.POST and "shift" in request.GET:
            try:
                shift = models.Shifts.objects.filter(id=request.GET['shift'])
                user = models.Users.objects.filter(id=request.POST["add_user"], ihm_password__isnull=True)
                if shift.count() > 0 and user.count() > 0:
                    shift = shift.first()
                    user = user.first()
                    user.shift = shift
                    user.update_user = request.user
                    user.update_timestamp = timezone.now()
                    user.save()

                    log_web(1, request,
                            "{} has edited the user shift for {}, shift: {}".format(request.user.get_full_name(),
                                                                                    user.get_full_name(),
                                                                                    user.shift.get_printed() if user.shift else "None"))
            except Exception:
                pass

        return self.return_values(request)


class UserData(View):
    def return_values(self, request, kwargs, mail_pass=None):
        if "user" not in kwargs:
            return redirect("/users/")

        user = models.Users.objects.filter(id=kwargs['user'], ihm_password__isnull=True)
        if user.count() == 0:
            return redirect("/users/")
        user = user.first()

        kits = user.get_kits_out()

        encid = AesEncryption().encrypt(str(user.id), "rfid")
        badges = models.Badges.objects.filter(user=encid).all()
        if len(badges) == 0:
            badges = None

        all_badges = list(models.Badges.objects.filter(user__isnull=True)) + list(
            models.Badges.objects.exclude(user=encid))

        user_jobs = []
        user_jobs_ls = []
        for job in UserJobs.objects.filter(user=user):
            user_jobs.append(job.job)
            user_jobs_ls.append(job.job.id)
        if len(user_jobs) == 0:
            user_jobs = None

        all_jobs = models.Jobs.objects.all()
        shifts = models.Shifts.objects.all()
        managers = models.Users.objects.filter(ihm_password=None, email__isnull=False)

        assigned = []
        for assign in KitAssignations.objects.filter(user=user):
            assigned.append(assign.kit)
        if len(assigned) == 0:
            assigned = None

        messages = models.UserMessages.objects.filter(user=user)

        logs = models.KitLogs.objects.filter(user=user,
                                             creation_timestamp__gte=timezone.now() - timezone.timedelta(
                                                 days=20)).order_by("-creation_timestamp").all()[:100]

        return render(request, 'user/user_data.html',
                      {'user': user, "kits": kits, "badges": badges, "ujobs": user_jobs, "jobs": all_jobs,
                       "ujobs_ls": user_jobs_ls, "shifts": shifts, "managers": managers, "all_badges": all_badges,
                       "assigned": assigned, "mail_pass": mail_pass, "messages": messages, "back": "/users/",
                       "logs": logs})

    def get(self, request, *args, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        return self.return_values(request, kwargs)

    def post(self, request, *args, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        mail_pass = None

        if "edit_jobs" in request.POST:
            form = EditUserJobsForm(request.POST)
            if form.is_valid():
                form.save(request.user)
                log_web(1, request,
                        "{} has edited the user job for {}, job: {}".format(request.user.get_full_name(),
                                                                            form.user.get_full_name(),
                                                                            form.user.get_printed_jobs()))
        elif "edit_shift" in request.POST:
            form = EditUserShiftForm(request.POST)
            if form.is_valid():
                form.save(request.user)
                log_web(1, request,
                        "{} has edited the user shift for {}, shift: {}".format(request.user.get_full_name(),
                                                                                form.user.get_full_name(),
                                                                                form.user.shift.get_printed() if form.user.shift else "None"))
        elif "change_activation" in request.POST:
            form = EditUserDisabledForm(request.POST)
            if form.is_valid():
                form.save(request.user)
                log_web(2, request,
                        "{} has edited the user status for {}, disabled: {}".format(request.user.get_full_name(),
                                                                                    form.user.get_full_name(),
                                                                                    str(form.user.disabled)))
        elif "edit_user" in request.POST:
            if "send_email" in request.POST:
                form = ResetPasswordForm(request.POST)
                if form.is_valid():
                    if form.user.email is None:
                        email = form.cleaned_data["email"]
                        form.user.email = AesEncryption().encrypt(email, "email")

                    temp_pass = uuid.uuid4().hex[0:8]
                    form.user.set_password(temp_pass)
                    form.user.save()
                    passlog = UserPasswordUpdate()
                    passlog.user = form.user
                    passlog.temp = True
                    passlog.save()

                    if not settings.EMAIL_ACTIVATED or not send_reset_password(request, form.user, temp_pass):
                        mail_pass = temp_pass

                    log_web(2, request,
                            "{} has edited the user password for {}".format(
                                request.user.get_full_name(),
                                form.user.get_full_name()))
            else:
                form = EditUserForm(request.POST)
                if form.is_valid():
                    form.save(request.user)
                    log_web(2, request,
                            "{} has edited the user data for {} (EMAIL: {}, MANAGER: {})".format(
                                request.user.get_full_name(),
                                form.user.get_full_name(),
                                "email" in form.cleaned_data and len(form.cleaned_data['email']) > 0,
                                form.user.manager.get_full_name() if form.user.manager else "None"))
        elif "delete_badge" in request.POST or "add_badge" in request.POST:
            form = EditBadgeAttribForm(request.POST)
            if form.is_valid():
                log_web(1, request,
                        "{} has {} a badge {} the user {}".format(
                            request.user.get_full_name(),
                            "added" if "add_badge" in request.POST else "removed",
                            "to" if "add_badge" in request.POST else "from",
                            form.user.get_full_name() if form.user is not None else form.badge.get_user().get_full_name()))
                form.save(request.user)
        elif "delete_assignation" in request.POST:
            form = RemoveKitAssignationForm(request.POST)
            if form.is_valid():
                form.save()
                log_web(1, request,
                        "{} has unassigned {} to the kit {}".format(
                            request.user.get_full_name(),
                            form.user.get_full_name(),
                            form.kit.get_printed()))

        return self.return_values(request, kwargs, mail_pass=mail_pass)


class Badges(View):
    def get_return_values(self, request):
        badges = models.Badges.objects.all()

        search = ""
        if "search" in request.GET:
            search = request.GET["search"].lower()
            aes = AesEncryption()
            tbadges = []
            for badge in badges:
                for elem in search.split(" "):
                    if elem in badge.rfid.lower() or elem in badge.name.lower():
                        tbadges.append(badge)
                        break
                    elif badge.user is not None and (
                            elem in aes.decrypt(badge.get_user().name).lower() or elem in aes.decrypt(
                        badge.get_user().surname).lower()):
                        tbadges.append(badge)
                        break
            badges = tbadges

        (badges, page, max_page, pages) = get_pagination(request, badges)

        return render(request, 'user/badges.html',
                      {"badges": badges, "page": page, "max_page": max_page, "pages": pages, "search": search})

    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        return self.get_return_values(request)

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        if "operation" in request.POST:
            operation = request.POST['operation']

            if operation == "rename":
                form = EditBadgeNameForm(request.POST)
                if form.is_valid():
                    form.save(request.user)
                    log_web(1, request,
                            "{} has renamed a badge {}".format(
                                request.user.get_full_name(),
                                form.badge.get_printed()))
            elif operation == "delete":
                form = EditBadgeDeleteForm(request.POST)
                if form.is_valid():
                    log_web(1, request,
                            "{} has deleted a badge {}".format(
                                request.user.get_full_name(),
                                form.badge.get_printed()))
                    form.save()
            elif operation == "add_badge":
                form = AddBadgeForm(request.POST)
                if form.is_valid():
                    form.save(request.user)
                    log_web(1, request,
                            "{} has added a badge {}".format(
                                request.user.get_full_name(),
                                form.badge.get_printed()))
            # elif operation == "attribution":
            #     form = EditBadgeAttribForm(request.POST)
            #     if form.is_valid():
            #         form.save(request.user)

        return self.get_return_values(request)


class Messages(View):
    def return_data(self, request, form=None):
        messages = models.UserMessages.objects.order_by("-start")

        unread_only = "unread_only" in request.GET
        if unread_only:
            messages = messages.filter(read__isnull=True)

        (messages, page, max_page, pages) = get_pagination(request, messages.all())

        return render(request, 'user/users_messages.html',
                      {"users": models.Users.objects.filter(ihm_password=None).all(), "messages": messages,
                       "form": form, "page": page, "max_page": max_page, "pages": pages, "unread_only": unread_only})

    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)
        return self.return_data(request)

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        if "delete_message" in request.POST:
            form = DeleteMessageForm(request.POST)
        else:
            form = AddMessageForm(request.POST)

        if form.is_valid():
            if "delete_message" in request.POST:
                form.save()
            else:
                form.save(request.user)

        if "redirect_user" in request.GET:
            user = models.Users.objects.filter(id=request.GET["redirect_user"])
            if user.count() > 0:
                return redirect("/users/{}/".format(user.first().id))

        return self.return_data(request, form=form)


class MessagesEdit(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        message = None
        if "message" in kwargs:
            message = models.UserMessages.objects.filter(id=kwargs["message"])
            if message.count() > 0:
                message = message.first()
            else:
                message = None

        if message is None or (message.read and message.end is None):
            return redirect("/messages/")

        return render(request, 'user/users_edit_messages.html',
                      {"users": models.Users.objects.filter(ihm_password=None).all(), "message": message,
                       "redirect": "redirect_user" in request.GET, "back": "/messages/"})

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        message = None
        if "message" in kwargs:
            message = models.UserMessages.objects.filter(id=kwargs["message"])
            if message.count() > 0:
                message = message.first()
            else:
                message = None

        if message is None or (message.read and message.end is None):
            return redirect("/messages/")

        form = EditMessageForm(request.POST, message)
        if form.is_valid():
            form.save()

            if "redirect_user" in request.GET:
                return redirect("/users/{}/".format(message.user.id))

        return render(request, 'user/users_edit_messages.html',
                      {"users": models.Users.objects.filter(ihm_password=None).all(), "message": message, "form": form,
                       "redirect": "redirect_user" in request.GET, "back": "/messages/"})
