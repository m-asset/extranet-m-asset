import datetime
import os

from PIL import Image
from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from django.forms import EmailField, forms, CharField, IntegerField, DateField, BooleanField, Form, ImageField, \
    FileField
from django.utils import timezone
from django.utils.translation import ugettext, gettext

from extranet import settings
from website.apps.web.encryption import AesEncryption
from website.apps.web.models import Users, Shifts
from website.apps.web.stats import get_logs
from website.apps.web.stats_page import get_stats


class LoginForm(forms.Form):
    email = EmailField(required=True)
    password = CharField(strip=False, required=True)

    error_messages = {
        'invalid_login': ugettext("Invalid credentials !"),
        'inactive': ugettext("This account is disabled !")
    }

    def __init__(self, request=None):
        super().__init__(request)
        self.request = request

    def clean(self):
        username = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        if username is not None and password and self.request is not None:
            enc_username = AesEncryption().encrypt(username, "email")
            user = Users.objects.filter(email=enc_username)
            if not user.exists():
                raise self.get_invalid_login_error()
            user = user.first()

            self.user_cache = authenticate(self.request, email=enc_username, password=password)

            if self.user_cache is None:
                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user(self):
        return self.user_cache

    def get_invalid_login_error(self):
        return ValidationError(
            self.error_messages['invalid_login'],
            code='invalid_login'
        )


class ChangePasswordForm(forms.Form):
    password = CharField(strip=False, required=True)
    password2 = CharField(strip=False, required=True)

    def clean(self):
        super(ChangePasswordForm, self).clean()

        pass1 = self.cleaned_data.get('password')
        pass2 = self.cleaned_data.get('password2')

        if pass1 != pass2:
            raise ValidationError(gettext("Passwords do not match !"))


class StatsForm(Form):
    start = DateField(required=False, input_formats=["%Y-%m-%d"])
    end = DateField(required=False, input_formats=["%Y-%m-%d"])

    def __init__(self, data=None):
        super().__init__(data)
        self.start = None
        self.end = None

    def clean(self):
        super(Form, self).clean()

        start = self.cleaned_data['start'] if self.cleaned_data['start'] else timezone.now()
        self.start = timezone.now().combine(date=start, time=datetime.time.min)
        end = self.cleaned_data['end'] if self.cleaned_data['end'] else timezone.now()
        self.end = timezone.now().combine(date=end, time=datetime.time.max)

        if self.end < self.start:
            self.end = timezone.now().combine(date=start, time=datetime.time.max)

        if (self.end - self.start).days > 90:
            self.start = timezone.now().combine(date=end - timezone.timedelta(days=90), time=datetime.time.min)

    def save(self):
        return get_stats(self.start, self.end)


class LogsForm(Form):
    page = IntegerField(required=False, min_value=0)
    start = DateField(required=False, input_formats=["%Y-%m-%d"])
    end = DateField(required=False, input_formats=["%Y-%m-%d"])
    unfinished = BooleanField(required=False)
    shift = IntegerField(required=False)

    def __init__(self, data=None, android=False):
        super().__init__(data)
        self.start = None
        self.end = None
        self.unfinished = False
        self.shift = None
        self.page = 1
        self.android = android

    def clean(self):
        super(Form, self).clean()

        if "shift" in self.cleaned_data:
            shift = Shifts.objects.filter(id=self.cleaned_data['shift'])
            if shift.count() > 0:
                self.shift = shift.first()

        start = self.cleaned_data['start'] if self.cleaned_data['start'] else timezone.now()
        self.start = timezone.now().combine(date=start, time=datetime.time.min)
        end = self.cleaned_data['end'] if self.cleaned_data['end'] else timezone.now()
        self.end = timezone.now().combine(date=end, time=datetime.time.max)
        self.unfinished = self.cleaned_data['unfinished']
        self.page = self.cleaned_data['page'] if "page" in self.cleaned_data and self.cleaned_data["page"] else 1

    def save(self):
        return get_logs(self.start, self.end, self.unfinished, self.shift, self.page, self.android)


class EditCompanyLogoForm(Form):
    logo = ImageField(required=False)
    name = CharField(required=False, max_length=40)

    def __init__(self, data=None, files=None):
        super().__init__(data, files)

    def clean(self):
        super(Form, self).clean()

    def save(self):
        if self.cleaned_data['logo'] is not None:
            img = Image.open(self.cleaned_data['logo'])
            img.save(os.path.join(settings.BASE_DIR, "static/img/company.png"), format='PNG', quality=90)

        name = ""
        if self.cleaned_data['name'] and self.cleaned_data['name'] != "" and len(self.cleaned_data['name']) <= 40:
            name = self.cleaned_data['name']

        with open(os.path.join(settings.BASE_DIR, "company.txt"), "w") as f:
            f.write(name)


class AddDocumentForm(Form):
    document = FileField(required=True)

    def __init__(self, data=None, files=None):
        super().__init__(data, files)
        self.path = None

    def clean(self):
        super(Form, self).clean()

        if self.cleaned_data['document'] is None:
            raise ValidationError("")
        destination = os.path.join(settings.BASE_DIR, "static/docs")
        destination = os.path.join(destination, self.cleaned_data["document"].name)

        if os.path.lexists(destination):
            raise ValidationError("")
        self.path = destination

    def save(self):
        f = self.cleaned_data["document"]
        with open(self.path, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
