from django.apps import AppConfig


class WebApps(AppConfig):
    name = 'website.apps.web'
