from django.core.exceptions import ValidationError
from django.forms import forms, CharField, IntegerField, Form, BooleanField, JSONField
from django.utils import timezone

from website.apps.web.models import Users, Racks, Jobs, UserJobs, Cases, KitLogs, LogsCasesOpen, LogsKitTransaction, \
    KitAssignations, LogsProductTransaction, Products, KitTypeProductFamily, ProductJobOut, ProductIncidents, KitTypes, \
    GeneralConfig


class RackCreationForm(forms.Form):
    admin_pass = CharField(required=True, min_length=2, max_length=512)
    number = IntegerField(required=True, min_value=1)
    doors_count = IntegerField(required=True, min_value=1)
    serial = CharField(required=True, min_length=2, max_length=512)
    part_number = CharField(required=True, min_length=2, max_length=512)
    master_ip = CharField(required=True, min_length=2, max_length=512)
    master = BooleanField(required=False)
    uvc_activate = BooleanField(required=False)

    def __init__(self, data):
        super().__init__(data)
        self.rack = None
        self.admin = None

    def clean(self):
        super(Form, self).clean()

        if "admin_pass" in self.cleaned_data:
            admin = Users.objects.filter(ihm_password=self.cleaned_data['admin_pass'])
            if admin.count() == 0:
                raise ValidationError("")
            self.admin = admin.first()
        else:
            raise ValidationError("")

        if Racks.objects.filter(number=self.cleaned_data['number']).count() > 0:
            raise ValidationError("")
        if Racks.objects.filter(serial=self.cleaned_data['serial']).count() > 0:
            raise ValidationError("")

        if "doors_count" in self.cleaned_data:
            valid = [1, 2, 3, 4, 8, 16, 32, 64]
            if self.cleaned_data["doors_count"] not in valid:
                raise ValidationError("")

    def save(self, ip):
        rack = Racks()
        rack.number = str(self.cleaned_data['number'])
        rack.name = str(chr(self.cleaned_data['number'] + 64))
        rack.state = True
        rack.doors_count = self.cleaned_data['doors_count']
        rack.master = "master" in self.cleaned_data and self.cleaned_data["master"]
        rack.serial = self.cleaned_data["serial"]
        rack.partnumber = self.cleaned_data["part_number"]
        rack.uvc_activate = "uvc_activate" in self.cleaned_data and self.cleaned_data["uvc_activate"]
        rack.ip_address = ip
        rack.subnet_address = ""  # TODO
        rack.gtw_address = ""  # TODO
        rack.ip_address_master = self.cleaned_data["master_ip"]
        rack.creation_timestamp = timezone.now()
        rack.update_timestamp = timezone.now()
        rack.creation_user = self.admin
        rack.update_user = self.admin
        rack.save()
        self.rack = rack

        for i in range(1, rack.doors_count + 1):
            case = Cases()
            case.kit = None
            case.rack = rack
            case.active = True
            case.position = i
            case.creation_timestamp = timezone.now()
            case.update_timestamp = timezone.now()
            case.creation_user = self.admin
            case.update_user = self.admin
            case.save()


class RackEditionForm(forms.Form):
    user = IntegerField(required=True, min_value=1)
    number = IntegerField(required=True, min_value=1)
    ip = CharField(required=True, min_length=7, max_length=15)
    state = BooleanField(required=False)
    uvc_activate = BooleanField(required=False)
    door_count = IntegerField(required=True, min_value=8, max_value=16)
    part_number = CharField(required=True)
    serial_number = CharField(required=True)

    def __init__(self, data, rack):
        super().__init__(data)
        self.rack = rack
        self.admin = None

    def clean(self):
        super(Form, self).clean()

        if "user" in self.cleaned_data:
            user = Users.objects.filter(id=self.cleaned_data['user'], ihm_password__isnull=False)
            if user.count() == 0:
                raise ValidationError("1")
            self.admin = user.first()
        else:
            raise ValidationError("2")

        if self.rack.number != str(self.cleaned_data['number']) and Racks.objects.filter(
                number=self.cleaned_data['number']).count() > 0:
            raise ValidationError("3")

        if Racks.objects.filter(serial=self.cleaned_data['serial']).count() > 0:
            raise ValidationError("4")

        if "doors_count" in self.cleaned_data:
            valid = [8, 16]
            if self.cleaned_data["doors_count"] not in valid:
                raise ValidationError("5")

    def save(self):
        if self.rack.doors_count != self.cleaned_data["door_count"]:
            for i in range(self.rack.doors_count + 1, self.cleaned_data["door_count"] + 1):
                case = Cases()
                case.kit = None
                case.rack = self.rack
                case.active = True
                case.position = i
                case.creation_timestamp = timezone.now()
                case.update_timestamp = timezone.now()
                case.creation_user = self.admin
                case.update_user = self.admin
                case.save()

        self.rack.number = str(self.cleaned_data['number'])
        self.rack.name = str(chr(self.cleaned_data['number'] + 64))
        self.rack.state = "state" in self.cleaned_data and self.cleaned_data["state"]
        self.rack.uvc_activate = "uvc_activate" in self.cleaned_data and self.cleaned_data["uvc_activate"]
        self.rack.ip_address = self.cleaned_data["ip"]
        self.rack.doors_count = self.cleaned_data['door_count']
        self.rack.serial = self.cleaned_data["serial_number"]
        self.rack.partnumber = self.cleaned_data["part_number"]
        self.rack.update_timestamp = timezone.now()
        self.rack.update_user = self.admin
        self.rack.save()


class ConfigEditionForm(forms.Form):
    user = IntegerField(required=True, min_value=1)
    data = JSONField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.admin = None
        self.valid_data = {}

    def clean(self):
        super(Form, self).clean()

        if "user" in self.cleaned_data:
            user = Users.objects.filter(id=self.cleaned_data['user'], ihm_password__isnull=False)
            if user.count() == 0:
                raise ValidationError("1")
            self.admin = user.first()
        else:
            raise ValidationError("2")

        if "data" not in self.cleaned_data or type(self.cleaned_data["data"]) != dict:
            raise ValidationError("3")

        for key in self.cleaned_data["data"].keys():
            if GeneralConfig.objects.filter(key=key).count() > 0:
                self.valid_data[key] = self.cleaned_data["data"][key]

    def save(self):
        for key in self.valid_data.keys():
            conf = GeneralConfig.objects.filter(key=key).first()
            conf.value = self.valid_data[key]
            conf.update_timestamp = timezone.now()
            conf.update_user = self.admin
            conf.save()


class UserTakeProductsForm(forms.Form):
    user = IntegerField(required=True, min_value=1)
    job = IntegerField(required=False)
    type = IntegerField(required=False)

    def __init__(self, data):
        super().__init__(data)
        self.user = None
        self.job = None
        self.kitType = None

    def clean(self):
        super(Form, self).clean()

        if "user" in self.cleaned_data:
            user = Users.objects.filter(id=self.cleaned_data['user'], ihm_password=None, disabled=False)
            if user.count() == 0:
                raise ValidationError("1")
            self.user = user.first()
        else:
            raise ValidationError("2")

        if "job" in self.cleaned_data and self.cleaned_data["job"] is not None:
            job = Jobs.objects.filter(id=self.cleaned_data['job'])
            if job.count() == 0:
                raise ValidationError("3")
            job = job.first()
            if UserJobs.objects.filter(job=job, user=self.user).count() == 0:
                raise ValidationError("4")
            self.job = job
        elif "type" in self.cleaned_data and self.cleaned_data["type"] is not None:
            type = KitTypes.objects.filter(id=self.cleaned_data['type'])
            if type.count() == 0:
                raise ValidationError("5")
            type = type.first()
            self.kitType = type
        else:
            raise ValidationError("6")


class ConfirmProductGivenForm(forms.Form):
    user = IntegerField(required=True, min_value=1)
    job = IntegerField(required=False, min_value=1)
    case = IntegerField(required=True, min_value=1)

    def __init__(self, data):
        super().__init__(data)
        self.user = None
        self.job = None
        self.case = None

    def clean(self):
        super(Form, self).clean()

        if "user" in self.cleaned_data:
            user = Users.objects.filter(id=self.cleaned_data['user'], ihm_password=None, disabled=False)
            if user.count() == 0:
                raise ValidationError("")
            self.user = user.first()
        else:
            raise ValidationError("")

        if "job" in self.cleaned_data and self.cleaned_data["job"]:
            job = Jobs.objects.filter(id=self.cleaned_data['job'])
            if job.count() == 0:
                raise ValidationError("")
            job = job.first()
            if UserJobs.objects.filter(job=job, user=self.user).count() == 0:
                raise ValidationError("")
            self.job = job

        if "case" in self.cleaned_data:
            case = Cases.objects.filter(id=self.cleaned_data['case'])
            if case.count() == 0:
                raise ValidationError("")
            self.case = case.first()
        else:
            raise ValidationError("")

    def save(self):
        if self.case.kit is None:
            return
        now = timezone.now()

        kitlog = KitLogs()
        kitlog.kit = self.case.kit
        kitlog.user = self.user
        kitlog.out = True
        kitlog.grade = 0
        kitlog.creation_timestamp = now
        kitlog.save()

        if self.job:
            if self.case.kit.assignable and self.job.assign and KitAssignations.objects.filter(kit=self.case.kit,
                                                                                               user=self.user).count() == 0:
                kitAssign = KitAssignations()
                kitAssign.kit = self.case.kit
                kitAssign.user = self.user
                kitAssign.creation_timestamp = now
                kitAssign.update_timestamp = now
                kitAssign.creation_user = self.user
                kitAssign.update_user = self.user
                kitAssign.save()

            kitCount = KitTypeProductFamily.objects.filter(kit_type=self.case.kit.type).count()
            if kitCount == 1:
                jobCount = KitTypeProductFamily.objects.filter(kit_type=self.job.kit_type).count()

                pjo = ProductJobOut.objects.filter(user=self.user, job=self.job, product_out__lt=jobCount)
                if pjo.count() == 0:
                    pjo = ProductJobOut()
                    pjo.job = self.job
                    pjo.user = self.user
                    pjo.creation_timestamp = now
                    pjo.update_timestamp = now
                    pjo.product_out = 1
                else:
                    pjo = pjo.first()
                    pjo.product_out += 1
                pjo.save()

        log_case = LogsCasesOpen()
        log_case.rack = self.case.rack.name
        log_case.case = self.case.position
        log_case.creation_timestamp = now
        log_case.save()

        log_kit = LogsKitTransaction()
        log_kit.log_case = log_case
        log_kit.assigned = KitAssignations.objects.filter(kit=self.case.kit, user=self.user).count() > 0
        log_kit.out = kitlog.out
        log_kit.creation_timestamp = now
        log_kit.save()

        for product in Products.objects.filter(kit=self.case.kit):
            log_prod = LogsProductTransaction()
            log_prod.log_kit = log_kit
            log_prod.family = product.product_type.family.name
            log_prod.brand = product.product_type.brand
            log_prod.model = product.product_type.model
            log_prod.serial = product.serial_number
            log_prod.creation_timestamp = now
            log_prod.save()


class ConfirmProductReturnedForm(forms.Form):
    user = IntegerField(required=True, min_value=1)
    case = IntegerField(required=True, min_value=1)
    grade = IntegerField(required=True, min_value=0)

    def __init__(self, data):
        super().__init__(data)
        self.user = None
        self.case = None

    def clean(self):
        super(Form, self).clean()

        if "user" in self.cleaned_data:
            user = Users.objects.filter(id=self.cleaned_data['user'], ihm_password=None, disabled=False)
            if user.count() == 0:
                raise ValidationError("")
            self.user = user.first()
        else:
            raise ValidationError("")

        if "case" in self.cleaned_data:
            case = Cases.objects.filter(id=self.cleaned_data['case'])
            if case.count() == 0:
                raise ValidationError("")
            self.case = case.first()
        else:
            raise ValidationError("")

    def save(self):
        if self.case.kit is None:
            return
        now = timezone.now()

        kitlog = KitLogs()
        kitlog.kit = self.case.kit
        kitlog.user = self.user
        kitlog.out = False
        kitlog.grade = self.cleaned_data["grade"] if "grade" in self.cleaned_data and self.cleaned_data["grade"] else 0
        kitlog.creation_timestamp = now
        kitlog.save()

        log_case = LogsCasesOpen()
        log_case.rack = self.case.rack.name
        log_case.case = self.case.position
        log_case.creation_timestamp = now
        log_case.save()

        log_kit = LogsKitTransaction()
        log_kit.log_case = log_case
        log_kit.assigned = KitAssignations.objects.filter(kit=self.case.kit, user=self.user).count() > 0
        log_kit.out = kitlog.out
        log_kit.creation_timestamp = now
        log_kit.save()

        for product in Products.objects.filter(kit=self.case.kit):
            log_prod = LogsProductTransaction()
            log_prod.log_kit = log_kit
            log_prod.family = product.product_type.family.name
            log_prod.brand = product.product_type.brand
            log_prod.model = product.product_type.model
            log_prod.serial = product.serial_number
            log_prod.creation_timestamp = now
            log_prod.save()


class DeclareIncidentForm(forms.Form):
    user = IntegerField(required=True, min_value=1)
    case = IntegerField(required=True, min_value=1)
    product = IntegerField(required=True, min_value=0)
    incident = IntegerField(required=True, min_value=0, max_value=5)

    def __init__(self, data):
        super().__init__(data)
        self.user = None
        self.case = None
        self.product = None
        self.incident = None

    def clean(self):
        super(Form, self).clean()

        if "user" in self.cleaned_data:
            user = Users.objects.filter(id=self.cleaned_data['user'], ihm_password=None, disabled=False)
            if user.count() == 0:
                raise ValidationError("")
            self.user = user.first()
        else:
            raise ValidationError("")

        if "case" in self.cleaned_data:
            case = Cases.objects.filter(id=self.cleaned_data['case'])
            if case.count() == 0:
                raise ValidationError("")
            self.case = case.first()
        else:
            raise ValidationError("")

        if "product" in self.cleaned_data:
            product = Products.objects.filter(id=self.cleaned_data['product'])
            if product.count() == 0:
                raise ValidationError("")
            self.product = product.first()
        else:
            raise ValidationError("")

    def save(self):
        if self.case.kit is None:
            return
        now = timezone.now()

        kitlog = KitLogs()
        kitlog.kit = self.case.kit
        kitlog.user = self.user
        kitlog.out = False
        kitlog.grade = 0
        kitlog.creation_timestamp = now
        kitlog.save()

        incident = ProductIncidents()
        incident.product = self.product
        incident.type = self.cleaned_data["incident"]
        incident.solved = 0
        incident.creation_timestamp = now
        incident.update_timestamp = now
        incident.creation_user = self.user
        incident.update_user = self.user
        incident.save()
        self.incident = incident

        self.case.active = False
        self.case.save()

        log_case = LogsCasesOpen()
        log_case.rack = self.case.rack.name
        log_case.case = self.case.position
        log_case.creation_timestamp = now
        log_case.save()

        log_kit = LogsKitTransaction()
        log_kit.log_case = log_case
        log_kit.assigned = KitAssignations.objects.filter(kit=self.case.kit, user=self.user).count() > 0
        log_kit.out = kitlog.out
        log_kit.creation_timestamp = now
        log_kit.save()

        for product in Products.objects.filter(kit=self.case.kit):
            log_prod = LogsProductTransaction()
            log_prod.log_kit = log_kit
            log_prod.family = product.product_type.family.name
            log_prod.brand = product.product_type.brand
            log_prod.model = product.product_type.model
            log_prod.serial = product.serial_number
            log_prod.creation_timestamp = now
            log_prod.save()
