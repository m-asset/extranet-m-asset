import os
import uuid

from PIL import Image
from django import forms
from django.core.exceptions import ValidationError
from django.forms import Form, CharField, ImageField, IntegerField, FileField
from django.utils import timezone

from extranet import settings
from website.apps.api.views.api import args_in_array
from website.apps.web.models import ProductFamily, ProductModel, BatteryType, Battery, Racks, Products, Cases, Users, \
    Kits, KitAssignations, KitTypes, KitTypeProductFamily, KitLogs, ProductIncidents, LogsCasesOpen, LogsKitTransaction, \
    LogsProductTransaction, ProductIncidentsComment


class AddProductForm(Form):
    type = IntegerField(required=True)
    serial_number = CharField(required=True, min_length=2, max_length=512)
    id_tag = CharField(required=False, min_length=2, max_length=512)
    local_id = CharField(required=False, min_length=2, max_length=512)
    battery_type = IntegerField(required=False)

    def __init__(self, data=None, files=None):
        super().__init__(data, files)
        self.type = None
        self.battery = None
        self.product = None

    def clean(self):
        super(Form, self).clean()

        type = ProductModel.objects.filter(id=self.cleaned_data['type'])

        if type.count() == 0:
            raise ValidationError("")
        self.type = type.first()

        battery_type = self.cleaned_data['battery_type']

        if battery_type is not None and battery_type > 0:
            battery_type = BatteryType.objects.filter(id=battery_type)

            if battery_type.count() == 0:
                raise ValidationError("")

            battery_type = battery_type.first()
        else:
            battery_type = None

        self.battery = battery_type

    def save(self, user):
        product = Products()
        product.product_type = self.type
        product.serial_number = self.cleaned_data['serial_number']
        product.id_tag = self.cleaned_data['id_tag'] if "id_tag" in self.cleaned_data and len(
            self.cleaned_data['id_tag']) > 0 else None
        product.local_id = self.cleaned_data['local_id'] if "local_id" in self.cleaned_data else None
        product.battery_type = self.battery
        product.used = 0
        product.creation_user = user
        product.update_user = user
        product.creation_timestamp = timezone.now()
        product.update_timestamp = timezone.now()
        product.save()
        self.product = product


class ProductDeleteForm(Form):
    product = forms.IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.product = None

    def clean(self):
        super(Form, self).clean()

        product = Products.objects.filter(id=self.cleaned_data['product'])
        if product.count() == 0:
            raise ValidationError("")
        self.product = product.first()

        if not self.product.can_be_deleted():
            raise ValidationError("")

    def save(self):
        for incident in ProductIncidents.objects.filter(product=self.product):
            for comment in incident.get_comment_history():
                comment.delete()
            incident.delete()

        self.product.delete()

        return True


class EditProductForm(Form):
    product = IntegerField(required=True)
    type = IntegerField(required=True)
    serial_number = CharField(required=True, min_length=2, max_length=512)
    id_tag = CharField(required=False, min_length=2, max_length=512)
    local_id = CharField(required=False, min_length=2, max_length=512)
    battery_type = IntegerField(required=False)

    def __init__(self, data=None, files=None):
        super().__init__(data, files)
        self.product = None
        self.type = None
        self.battery = None

    def clean(self):
        super(Form, self).clean()

        if not args_in_array(["product", "type"], self.cleaned_data):
            raise ValidationError("")

        product = Products.objects.filter(id=self.cleaned_data['product'])
        if product.count() == 0:
            raise ValidationError("")
        self.product = product.first()

        type = ProductModel.objects.filter(id=self.cleaned_data['type'])

        if type.count() == 0:
            raise ValidationError("")
        self.type = type.first()

        battery_type = self.cleaned_data['battery_type']

        if battery_type is not None and battery_type > 0:
            battery_type = BatteryType.objects.filter(id=battery_type)

            if battery_type.count() == 0:
                raise ValidationError("")

            battery_type = battery_type.first()
        else:
            battery_type = None

        self.battery = battery_type

    def save(self, user):
        product = self.product
        product.product_type = self.type
        product.serial_number = self.cleaned_data['serial_number']
        if "id_tag" in self.data:
            product.id_tag = self.cleaned_data['id_tag'] if len(self.cleaned_data["id_tag"]) > 0 else None
        product.local_id = self.cleaned_data['local_id'] if "local_id" in self.cleaned_data else None
        product.battery_type = self.battery
        product.update_user = user
        product.update_timestamp = timezone.now()
        product.save()


class ImportProductsForm(Form):
    config = FileField(required=True)

    def __init__(self, data=None, files=None):
        super().__init__(data, files)
        self.products = []
        self.import_errors = []

    def clean(self):
        super(Form, self).clean()

        if self.cleaned_data['config'] is None:
            raise ValidationError("")

    def save(self, admin):
        if self.cleaned_data['config'].multiple_chunks(chunk_size=10000000):
            return
        data = self.cleaned_data['config'].readlines()

        added = []
        for index in range(len(data)):
            line = data[index]
            lineIndex = index + 1
            user_data = line.decode().rstrip().lstrip().split(";")
            if len(user_data) != 4:
                self.import_errors.append("Line {}, data is invalid !".format(lineIndex))
                continue

            type = user_data[0]
            serial_number = user_data[1]
            id_tag = user_data[2]
            local_id = user_data[3]

            if ProductModel.objects.filter(reference=type).count() == 0:
                self.import_errors.append("Line {}, cannot find the model with this reference !".format(lineIndex))
                continue
            else:
                type = ProductModel.objects.filter(reference=type).first()

            if Products.objects.filter(serial_number=serial_number).count() > 0:
                self.import_errors.append("Line {}, serial number already exists !".format(lineIndex))
                continue

            if Products.objects.filter(serial_number=local_id).count() > 0:
                self.import_errors.append("Line {}, local id already exists !".format(lineIndex))
                continue

            if len(id_tag) > 0 and Products.objects.filter(serial_number=id_tag).count() > 0:
                self.import_errors.append("Line {}, rfid tag already exists !".format(lineIndex))
                continue
            else:
                id_tag = None

            product_data = {
                "type": type,
                "serial_number": serial_number,
                "id_tag": id_tag,
                "local_id": local_id
            }

            added.append(self.save_product(admin, product_data))

        return added

    def save_product(self, admin, product_data):
        product = Products()
        product.product_type = product_data['type']
        product.serial_number = product_data['serial_number']
        product.id_tag = product_data['id_tag']
        product.local_id = product_data['local_id']
        product.battery_type = None
        product.used = 0
        product.creation_user = admin
        product.update_user = admin
        product.creation_timestamp = timezone.now()
        product.update_timestamp = timezone.now()
        product.save()

        return product


class AddProductFamilyForm(Form):
    name = CharField(required=True, min_length=2, max_length=512)

    def __init__(self, data=None):
        super().__init__(data, )
        self.family = None

    def save(self, user):
        family = ProductFamily()
        family.name = self.cleaned_data['name']
        family.creation_user = user
        family.update_user = user
        family.creation_timestamp = timezone.now()
        family.update_timestamp = timezone.now()
        family.save()
        self.family = family


class EditProductFamilyForm(Form):
    family = IntegerField(required=True)
    name = CharField(required=True, min_length=2, max_length=512)

    def __init__(self, data=None):
        super().__init__(data, )
        self.family = None

    def clean(self):
        super(Form, self).clean()

        if not args_in_array(["family", "name"], self.cleaned_data):
            raise ValidationError("")

        family = ProductFamily.objects.filter(id=self.cleaned_data["family"])
        if family.count == 0:
            raise ValidationError("")
        self.family = family.first()

    def save(self, user):
        family = self.family
        family.name = self.cleaned_data['name']
        family.update_user = user
        family.update_timestamp = timezone.now()
        family.save()


class AddProductTypeForm(Form):
    family = IntegerField(required=True)
    name = CharField(required=True, min_length=2, max_length=512)
    brand = CharField(required=True, min_length=2, max_length=512)
    model = CharField(required=True, min_length=2, max_length=512)
    reference = CharField(required=True, min_length=2, max_length=512)
    image = ImageField(required=False)

    def __init__(self, data=None, files=None):
        super().__init__(data, files)
        self.family = None
        self.type = None

    def clean(self):
        super(Form, self).clean()

        if "family" not in self.cleaned_data:
            raise ValidationError("")

        family = ProductFamily.objects.filter(id=self.cleaned_data['family'])

        if family.count() == 0:
            raise ValidationError("")
        self.family = family.first()

    def save(self, user):
        if self.cleaned_data['image'] is None:
            filename = "default.png"
        else:
            img = Image.open(self.cleaned_data['image'])
            width, height = img.size
            nwidth, nheight = width, height
            if width > height:
                nheight = width
            else:
                nwidth = height
            img.resize((nwidth, nheight))
            filename = str(uuid.uuid4()) + ".png"
            img.save(os.path.join(settings.BASE_DIR, "static/img/products/" + filename), format='PNG', quality=90)

        pmodel = ProductModel()
        pmodel.family = self.family
        pmodel.name = self.cleaned_data['name']
        pmodel.brand = self.cleaned_data['brand']
        pmodel.model = self.cleaned_data['model']
        pmodel.reference = self.cleaned_data['reference']
        pmodel.picture = filename
        pmodel.creation_user = user
        pmodel.update_user = user
        pmodel.creation_timestamp = timezone.now()
        pmodel.update_timestamp = timezone.now()
        pmodel.save()
        self.type = pmodel


class EditProductTypeForm(Form):
    model = IntegerField(required=True)
    image = ImageField(required=False)

    def __init__(self, data=None, files=None):
        super().__init__(data, files)
        self.model = None

    def clean(self):
        super(Form, self).clean()

        if "model" not in self.cleaned_data:
            raise ValidationError("")

        model = ProductModel.objects.filter(id=self.cleaned_data['model'])
        if model.count() == 0:
            raise ValidationError("")
        self.model = model.first()

    def save(self, user):
        if self.cleaned_data['image'] is None:
            filename = "default.png"
        else:
            img = Image.open(self.cleaned_data['image'])
            width, height = img.size
            nwidth, nheight = width, height
            if width > height:
                nheight = width
            else:
                nwidth = height
            img.resize((nwidth, nheight))
            filename = str(uuid.uuid4()) + ".png"
            img.save(os.path.join(settings.BASE_DIR, "static/img/products/" + filename), format='PNG', quality=90)

        model = self.model

        if model.picture != "default.png":
            path = os.path.join(settings.BASE_DIR, "static/img/products/" + model.picture)
            if os.path.lexists(path):
                os.remove(path)

        model.picture = filename
        model.update_user = user
        model.update_timestamp = timezone.now()
        model.save()


class AddBatteryForm(Form):
    type = IntegerField(required=True)
    serial = CharField(required=True, min_length=2, max_length=512)
    local_id = CharField(required=True, min_length=2, max_length=512)
    rack = IntegerField(required=False)

    def __init__(self, data=None, files=None):
        super().__init__(data, files)
        self.type = None
        self.rack = None

    def clean(self):
        super(Form, self).clean()

        type = BatteryType.objects.filter(id=self.cleaned_data['type'])

        if type.count() == 0:
            raise ValidationError("")
        self.type = type.first()

        rack = self.cleaned_data['rack']

        if rack is not None:
            rack = Racks.objects.filter(id=rack)

            if rack.count() == 0:
                raise ValidationError("")

            rack = rack.first()

        self.rack = rack

    def save(self, user):
        battery = Battery()
        battery.type = self.type
        battery.serial_number = self.cleaned_data['serial']
        battery.local_id = self.cleaned_data['local_id']
        battery.used = 0
        battery.rack = self.rack
        battery.creation_user = user
        battery.update_user = user
        battery.creation_timestamp = timezone.now()
        battery.update_timestamp = timezone.now()
        battery.save()


class AddBatteryTypeForm(Form):
    name = CharField(required=True, min_length=2, max_length=512)

    def save(self, user):
        batttype = BatteryType()
        batttype.name = self.cleaned_data['name']
        batttype.creation_user = user
        batttype.update_user = user
        batttype.creation_timestamp = timezone.now()
        batttype.update_timestamp = timezone.now()
        batttype.save()


class KitAddForm(Form):
    case = forms.IntegerField(required=True)
    priority = forms.IntegerField(required=True, min_value=0, max_value=100)
    assignable = forms.BooleanField(required=False)
    products = forms.JSONField(required=False)
    assigned = forms.JSONField(required=False)

    def __init__(self, data):
        super().__init__(data)
        self.case = None
        self.assignable = False
        self.products = None
        self.assigned = None
        self.kit = None

    def clean(self):
        super(Form, self).clean()

        case = Cases.objects.filter(id=self.cleaned_data['case'])
        if case.count() == 0:
            raise ValidationError("")
        self.case = case.first()

        if self.case.kit is not None:
            raise ValidationError("")

        productList = []
        if "products" in self.cleaned_data:
            for productId in self.cleaned_data['products']:
                product = Products.objects.filter(id=productId)
                if product.count() == 0:
                    raise ValidationError("")
                product = product.first()

                if product.kit is not None:
                    raise ValidationError("")

                valid = True
                for productL in productList:
                    valid = valid and productL.id != product.id and productL.product_type.family.id != product.product_type.family.id
                if valid:
                    productList.append(product)
        self.products = productList

        userList = []
        if "assigned" in self.cleaned_data:
            for userId in self.cleaned_data['assigned']:
                user = Users.objects.filter(id=userId)
                if user.count() == 0:
                    raise ValidationError("")
                user = user.first()
                userList.append(user)
        self.assigned = userList

        self.assignable = "assignable" in self.cleaned_data and self.cleaned_data["assignable"]

    def save(self, user):
        pCount = len(self.products)
        pFamilies = []
        for product in self.products:
            pFamilies.append(product.product_type.family.id)

        finalKitType = None
        for kitType in KitTypes.objects.all():
            ktpfList = KitTypeProductFamily.objects.filter(kit_type=kitType)
            if ktpfList.count() != pCount:
                continue
            families = []
            for ktpf in ktpfList:
                if ktpf.product_family.id in pFamilies:
                    families.append(ktpf.product_family.id)
            if pCount != len(families):
                continue
            finalKitType = kitType
            break

        if finalKitType is None:
            finalKitType = KitTypes()
            finalKitType.creation_user = user
            finalKitType.update_user = user
            finalKitType.creation_timestamp = timezone.now()
            finalKitType.update_timestamp = timezone.now()
            finalKitType.save()

            for product in self.products:
                ktpf = KitTypeProductFamily()
                ktpf.kit_type = finalKitType
                ktpf.product_family = product.product_type.family
                ktpf.creation_user = user
                ktpf.creation_timestamp = timezone.now()
                ktpf.save()

        kit = Kits()
        kit.type = finalKitType
        kit.assignable = self.assignable
        kit.priority = self.cleaned_data['priority']
        kit.creation_user = user
        kit.update_user = user
        kit.creation_timestamp = timezone.now()
        kit.update_timestamp = timezone.now()
        kit.save()
        self.kit = kit

        for product in self.products:
            product.kit = kit
            product.update_user = user
            product.update_timestamp = timezone.now()
            product.save()

        if self.assignable and len(self.assigned) > 0:
            for assigned in self.assigned:
                assignation = KitAssignations()
                assignation.kit = kit
                assignation.user = assigned
                assignation.creation_user = user
                assignation.update_user = user
                assignation.creation_timestamp = timezone.now()
                assignation.update_timestamp = timezone.now()
                assignation.save()

        self.case.kit = kit
        self.case.save()


class CaseOpenForm(Form):
    case = forms.IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.case = None

    def clean(self):
        super(Form, self).clean()

        case = Cases.objects.filter(id=self.cleaned_data['case'])
        if case.count() == 0:
            raise ValidationError("")
        self.case = case.first()


class KitLogForm(Form):
    kit = forms.IntegerField(required=True)
    out = forms.BooleanField(required=False)
    grade = forms.IntegerField(required=False)

    def __init__(self, data):
        super().__init__(data)
        self.kit = None
        self.out = False
        self.last_kit_log = None
        self.grade = 0

    def clean(self):
        super(Form, self).clean()

        kit = Kits.objects.filter(id=self.cleaned_data['kit'])
        if kit.count() == 0:
            raise ValidationError("")
        self.kit = kit.first()

        kit_log = KitLogs.objects.filter(kit=self.kit).order_by("-id")
        if kit_log.count() > 0:
            kit_log = kit_log.first()
            if not kit_log.out:
                kit_log = None
        else:
            kit_log = None
        self.last_kit_log = kit_log

        self.out = "out" in self.cleaned_data and self.cleaned_data["out"]
        self.grade = min(max(self.cleaned_data["grade"], 1), 5) if "grade" in self.cleaned_data else 0

    def save(self, user):
        now = timezone.now()

        kitlog = KitLogs()
        kitlog.kit = self.kit
        kitlog.user = user
        kitlog.out = self.out
        kitlog.grade = self.grade
        kitlog.creation_timestamp = now
        kitlog.save()

        case = Cases.objects.filter(kit=self.kit)
        if case.count() > 0:
            case = case.first()
            log_case = LogsCasesOpen()
            log_case.rack = case.rack.name
            log_case.case = case.position
            log_case.creation_timestamp = now
            log_case.save()

            log_kit = LogsKitTransaction()
            log_kit.log_case = log_case
            log_kit.assigned = KitAssignations.objects.filter(kit=self.kit, user=user).count() > 0
            log_kit.out = kitlog.out
            log_kit.creation_timestamp = now
            log_kit.save()

            for product in Products.objects.filter(kit=self.kit):
                log_prod = LogsProductTransaction()
                log_prod.log_kit = log_kit
                log_prod.family = product.product_type.family.name
                log_prod.brand = product.product_type.brand
                log_prod.model = product.product_type.model
                log_prod.serial = product.serial_number
                log_prod.creation_timestamp = now
                log_prod.save()

        if not self.out and self.last_kit_log is not None:
            starttime = self.last_kit_log.creation_timestamp
            timespan = now - starttime
            usage = timespan.total_seconds()
            for product in Products.objects.filter(kit=self.kit):
                product.used = product.used + usage
                product.save()


class KitRemoveForm(Form):
    kit = forms.IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.kit = None

    def clean(self):
        super(Form, self).clean()

        kit = Kits.objects.filter(id=self.cleaned_data['kit'])
        if kit.count() == 0:
            raise ValidationError("")
        self.kit = kit.first()

    def save(self, user):
        for kitlog in KitLogs.objects.filter(kit=self.kit):
            kitlog.delete()

        for product in Products.objects.filter(kit=self.kit):
            product.kit = None
            product.update_timestamp = timezone.now()
            product.update_user = user
            product.save()

        for case in Cases.objects.filter(kit=self.kit):
            case.kit = None
            case.update_timestamp = timezone.now()
            case.update_user = user
            case.save()

        for kitassign in KitAssignations.objects.filter(kit=self.kit):
            kitassign.delete()

        self.kit.delete()

        return True


class CaseSwitchForm(Form):
    case = forms.IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.case = None

    def clean(self):
        super(Form, self).clean()

        case = Cases.objects.filter(id=self.cleaned_data['case'])
        if case.count() == 0:
            raise ValidationError("")
        self.case = case.first()

    def save(self, user):
        self.case.active = not self.case.active
        self.case.update_timestamp = timezone.now()
        self.case.update_user = user
        self.case.save()


class KitAssignForm(Form):
    kit = forms.IntegerField(required=True)
    assignable = forms.BooleanField(required=False)
    assigned = forms.JSONField(required=False)

    def __init__(self, data):
        super().__init__(data)
        self.kit = None
        self.assignable = False
        self.assigned = None

    def clean(self):
        super(Form, self).clean()

        kit = Kits.objects.filter(id=self.cleaned_data['kit'])
        if kit.count() == 0:
            raise ValidationError("")
        self.kit = kit.first()

        self.assignable = "assignable" in self.cleaned_data and self.cleaned_data["assignable"]

        userList = []
        if "assigned" in self.cleaned_data and self.cleaned_data["assigned"] is not None and self.assignable:
            for userId in self.cleaned_data['assigned']:
                user = Users.objects.filter(id=userId)
                if user.count() == 0:
                    raise ValidationError("")
                user = user.first()
                userList.append(user)
        self.assigned = userList

    def save(self, user):
        self.kit.assignable = self.assignable
        self.kit.update_user = user
        self.kit.update_timestamp = timezone.now()
        self.kit.save()

        assignedList = KitAssignations.objects.filter(kit=self.kit)
        assignedMap = {}
        for assigned in assignedList:
            assignedMap[assigned.user.id] = assigned

        if not self.assignable and len(assignedMap) > 0:
            for assigned in assignedMap.values():
                assigned.delete()
        elif self.assignable:
            assignedVal = []
            for assigned in self.assigned:
                if assigned.id in assignedMap:
                    assignedVal.append(assigned.id)
                else:
                    assignation = KitAssignations()
                    assignation.kit = self.kit
                    assignation.user = assigned
                    assignation.creation_user = user
                    assignation.update_user = user
                    assignation.creation_timestamp = timezone.now()
                    assignation.update_timestamp = timezone.now()
                    assignation.save()
            for assigned in assignedMap.values():
                if assigned.user.id not in assignedVal:
                    assigned.delete()


class IncidentSolvedForm(Form):
    incident = forms.IntegerField(required=True)

    def __init__(self, data):
        super().__init__(data)
        self.incident = None

    def clean(self):
        super(Form, self).clean()

        incident = ProductIncidents.objects.filter(id=self.cleaned_data['incident'])
        if incident.count() == 0:
            raise ValidationError("")
        self.incident = incident.first()

    def save(self, user):
        self.incident.solved = 2
        self.incident.update_timestamp = timezone.now()
        self.incident.update_user = user
        self.incident.save()


class IncidentCommentForm(Form):
    incident = forms.IntegerField(required=True)
    comment = forms.CharField(required=True, min_length=2)

    def __init__(self, data):
        super().__init__(data)
        self.incident = None
        self.comment = None

    def clean(self):
        super(Form, self).clean()

        incident = ProductIncidents.objects.filter(id=self.cleaned_data['incident'])
        if incident.count() == 0:
            raise ValidationError("")
        self.incident = incident.first()

        if "comment" not in self.cleaned_data:
            raise ValidationError("")

    def save(self, user):
        self.incident.solved = 1
        self.incident.update_timestamp = timezone.now()
        self.incident.update_user = user
        self.incident.save()

        self.comment = ProductIncidentsComment()
        self.comment.incident = self.incident
        self.comment.comment = self.cleaned_data["comment"]
        self.comment.creation_user = user
        self.comment.creation_timestamp = timezone.now()
        self.comment.save()


class ProductSetRfidForm(Form):
    product = forms.IntegerField(required=True)
    id_tag = forms.CharField(required=True, min_length=2, max_length=512)

    def __init__(self, data):
        super().__init__(data)
        self.product = None

    def clean(self):
        super(Form, self).clean()

        product = Products.objects.filter(id=self.cleaned_data['product'])
        if product.count() == 0:
            raise ValidationError("")
        self.product = product.first()

    def save(self, user):
        self.product.id_tag = self.cleaned_data['id_tag']
        self.product.update_user = user
        self.product.update_timestamp = timezone.now()
        self.product.save()
