from website.apps.web.models import GeneralConfig


def GetValue(key, default):
    conf = GeneralConfig.objects.filter(key=key)
    if conf.count() == 0:
        return default
    return conf.first().value


def BatteriesActivated():
    return GetValue("BATTERY_SYSTEM_ACTIVATED", "0") == "0"


def IdentificationActivated():
    return GetValue("IDENTIFICATION_ACTIVATED", "0") == "1"


def MaxPasswordDays():
    return int(GetValue("MAX_PASSWORD_DAYS", 45))
