from django.contrib.auth.backends import ModelBackend

from website.apps.web.models import Users


class AuthUsersBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        if username is None:
            if "email" in kwargs:
                username = kwargs["email"]
            else:
                return None

        user = Users.objects.get(email=username)

        if user is None:
            return None

        if user.disabled:
            return None

        if user.email is None or user.password is None:
            return None

        if not user.check_password(password):
            return None

        return user

    def get_user(self, user_id):
        try:
            return Users.objects.get(id=user_id)
        except Users.DoesNotExist:
            return None
