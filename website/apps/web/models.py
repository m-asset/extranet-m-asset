# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.templatetags.static import static
from django.utils import timezone
from django.utils.translation import gettext

from website.apps.web.encryption import AesEncryption


class AlertProductIn(models.Model):
    id = models.BigAutoField(primary_key=True)
    alert = models.ForeignKey('Alerts', models.DO_NOTHING, db_column='alert', related_name='+')
    user = models.ForeignKey('Users', models.DO_NOTHING, db_column='user', related_name='+')
    kit_log_out = models.ForeignKey('KitLogs', models.DO_NOTHING, db_column='kit_log_out', related_name='+')
    kit_log_in = models.ForeignKey('KitLogs', models.DO_NOTHING, db_column='kit_log_in', related_name='+')

    class Meta:
        managed = False
        db_table = 'alert_product_in'


class Alerts(models.Model):
    id = models.BigAutoField(primary_key=True)
    creation_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'alerts'


class Battery(models.Model):
    id = models.BigAutoField(primary_key=True)
    serial_number = models.CharField(unique=True, max_length=255)
    local_id = models.CharField(unique=True, max_length=255)
    type = models.ForeignKey('BatteryType', models.DO_NOTHING, db_column='type', related_name='+')
    used = models.BigIntegerField()
    rack = models.ForeignKey('Racks', models.DO_NOTHING, db_column='rack', related_name='+')
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField(blank=True, null=True)
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'battery'


class BatteryLogs(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey('Users', models.DO_NOTHING, db_column='user', related_name='+')
    battery = models.ForeignKey(Battery, models.DO_NOTHING, db_column='battery', related_name='+')
    out = models.IntegerField()
    grade = models.IntegerField()
    creation_timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'battery_logs'


class BatteryType(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=255)
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'battery_type'


class Cases(models.Model):
    id = models.BigAutoField(primary_key=True)
    kit = models.ForeignKey('Kits', models.DO_NOTHING, db_column='kit', related_name='+', blank=True, null=True)
    rack = models.ForeignKey('Racks', models.DO_NOTHING, db_column='rack', related_name='+')
    active = models.BooleanField()
    position = models.IntegerField()
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'cases'

    def is_empty(self):
        empty = True

        if self.kit is not None:
            last_kit = KitLogs.objects.filter(kit=self.kit).order_by("-id")
            if last_kit.count() > 0:
                empty = last_kit.first().out == 1
        return empty

    def is_in_hand(self):
        inhand = False

        if self.kit is not None:
            last_kit = KitLogs.objects.filter(kit=self.kit).order_by("-id")
            if last_kit.count() > 0:
                inhand = last_kit.first().out == 1
        return inhand

    def get_in_hand(self):
        if self.kit is not None:
            last_kit = KitLogs.objects.filter(kit=self.kit).order_by("-id")
            if last_kit.count() > 0:
                last_kit = last_kit.first()
                if last_kit.out == 1:
                    return {"user": last_kit.user, "out": last_kit.creation_timestamp}
        return {"user": None, "out": None}


class GeneralConfig(models.Model):
    id = models.BigAutoField(primary_key=True)
    key = models.CharField(max_length=255)
    value = models.TextField()
    update_timestamp = models.DateTimeField()
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'general_config'


class Jobs(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.TextField()
    kit_type = models.ForeignKey('KitTypes', models.DO_NOTHING, db_column='kit_type', related_name='+', blank=True,
                                 null=True)
    assign = models.IntegerField()
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+', null=True,
                                    blank=True)

    class Meta:
        managed = False
        db_table = 'jobs'

    @staticmethod
    def allowed_order():
        return ["id", "name"]

    def get_printed(self):
        return self.name

    def get_families_printed(self):
        families = ""
        for family in self.selected_families():
            families += "{}, ".format(family.name)
        return families[:-2] if len(families) > 2 else ""

    def selected_families(self):
        families = []
        for ktpf in KitTypeProductFamily.objects.filter(kit_type=self.kit_type):
            families.append(ktpf.product_family)
        return families


class KitAssignations(models.Model):
    id = models.BigAutoField(primary_key=True)
    kit = models.ForeignKey('Kits', models.DO_NOTHING, db_column='kit', related_name='+')
    user = models.ForeignKey('Users', models.DO_NOTHING, db_column='user', related_name='+')
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'kit_assignations'


class KitLogs(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey('Users', models.DO_NOTHING, db_column='user', related_name='+')
    kit = models.ForeignKey('Kits', models.DO_NOTHING, db_column='kit', related_name='+')
    out = models.IntegerField()
    grade = models.IntegerField()
    creation_timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'kit_logs'


class KitTypeProductFamily(models.Model):
    id = models.BigAutoField(primary_key=True)
    kit_type = models.ForeignKey('KitTypes', models.DO_NOTHING, db_column='kit_type', related_name='+')
    product_family = models.ForeignKey('ProductFamily', models.DO_NOTHING, db_column='product_family', related_name='+')
    creation_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'kit_type_product_family'


class KitTypes(models.Model):
    id = models.BigAutoField(primary_key=True)
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'kit_types'


class Kits(models.Model):
    id = models.BigAutoField(primary_key=True)
    type = models.ForeignKey(KitTypes, models.DO_NOTHING, db_column='type', related_name='+')
    assignable = models.IntegerField()
    priority = models.IntegerField()
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'kits'

    def get_printed(self):
        prod = ""
        for produ in self.get_products():
            prod += "{}, ".format(produ.get_printed())
        return "{} [{}]".format(self.id, prod[:-2] if len(prod) > 0 else "")

    def get_printed_assigned(self):
        users = ""
        for user in self.get_assigned_users():
            users += "{}, ".format(user.user.get_full_name())
        return users[:-2] if len(users) > 2 else ""

    def get_products(self):
        return Products.objects.filter(kit=self)

    def get_assigned_users(self):
        return KitAssignations.objects.filter(kit=self)

    def get_assigned_users_list(self):
        out = []
        for kitassign in self.get_assigned_users():
            out.append(kitassign.user.id)
        return out

    def user_currently_assigned(self):
        assigns = self.get_assigned_users()
        for assign in assigns:
            if assign.user.shift is None:
                return assign.user
            else:
                time = timezone.now().time()
                if assign.user.shift.start <= time <= assign.user.shift.end:
                    return assign.user
        return None

    def get_location(self):
        case = Cases.objects.filter(kit=self)
        if case.count() > 0:
            return case.first()
        return None

    def is_available_for_user(self, user):
        case = self.get_location()
        if case is None or not case.active:
            return False
        kitLogs = KitLogs.objects.filter(kit=self).order_by("-creation_timestamp")
        if kitLogs.count() > 0:
            if kitLogs.first().out:
                return False
        elif kitLogs.count() == 0:
            return False
        if not self.assignable:
            return True
        if KitAssignations.objects.filter(kit=self, user=user).count() > 0:
            return True
        if user.shift is None:
            return False
        shift = user.shift
        for ka in KitAssignations.objects.filter(kit=self):
            ushift = ka.user.shift
            if ushift is None or ushift == shift:
                return False
        return True


class ProductFamily(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.TextField()
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'product_family'

    @staticmethod
    def allowed_order():
        return ["id", "name"]


class ProductModel(models.Model):
    id = models.BigAutoField(primary_key=True)
    family = models.ForeignKey(ProductFamily, models.DO_NOTHING, db_column='family', related_name='+')
    name = models.TextField()
    brand = models.TextField()
    model = models.TextField()
    reference = models.TextField()
    picture = models.TextField(blank=True, null=True)
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'product_model'

    @staticmethod
    def allowed_order():
        return ["id", "name", "brand", "model", "family", "family__name", "reference"]

    def get_printed(self):
        return "{} (BRAND: {}, MODEL: {}, FAM: {})".format(self.name, self.brand, self.model, self.family.name)

    def get_picture(self):
        if not self.picture:
            return static("img/products/default.png")
        else:
            return static("img/products/" + self.picture)


class Products(models.Model):
    id = models.BigAutoField(primary_key=True)
    serial_number = models.CharField(unique=True, max_length=255)
    id_tag = models.CharField(max_length=255, blank=True, null=True)
    local_id = models.CharField(unique=True, max_length=255, blank=True, null=True)
    product_type = models.ForeignKey(ProductModel, models.DO_NOTHING, db_column='product_type', related_name='+')
    kit = models.ForeignKey(Kits, models.DO_NOTHING, db_column='kit', related_name='+', blank=True, null=True)
    used = models.BigIntegerField()
    battery_type = models.ForeignKey(BatteryType, models.DO_NOTHING, db_column='battery_type', related_name='+',
                                     blank=True, null=True)
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'products'

    @staticmethod
    def allowed_order():
        return ["id", "serial_number", "id_tag", "local_id", "product_type", "product_type__name", "used",
                "kit__assignable"]

    def get_printed(self):
        return "{} (SN: {}, ID: {}, FAM: {})".format(self.product_type.name, self.serial_number, self.local_id,
                                                     self.product_type.family.name)

    def is_in_hand(self):
        inhand = False

        if self.kit is not None:
            last_kit = KitLogs.objects.filter(kit=self.kit).order_by("-id")
            if last_kit.count() > 0:
                last_kit = last_kit.first()
                inhand = last_kit.out == 1

        return inhand

    def is_available(self):
        if self.kit is None:
            return False
        case = Cases.objects.filter(kit=self.kit)
        if case.count() == 0:
            return False
        return case.first().active

    def locate(self):
        if self.kit is None:
            return ""
        case = Cases.objects.filter(kit=self.kit)
        if case.count() == 0:
            return ""
        case = case.first()
        return case.rack.name + str(case.position)

    def get_assigned(self):
        if self.kit is None or not self.kit.assignable:
            return []
        ls = []
        for ka in KitAssignations.objects.filter(kit=self.kit):
            ls.append(ka.user)
        return ls

    def can_be_deleted(self):
        return self.kit is None


class Racks(models.Model):
    id = models.BigAutoField(primary_key=True)
    number = models.CharField(unique=True, max_length=255)
    name = models.TextField()
    state = models.IntegerField()
    doors_count = models.IntegerField()
    master = models.IntegerField()
    serial = models.CharField(unique=True, max_length=255)
    partnumber = models.CharField(max_length=255)
    uvc_activate = models.IntegerField()
    ip_address = models.TextField()
    subnet_address = models.TextField()
    gtw_address = models.TextField()
    ip_address_master = models.TextField()
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'racks'

    def get_printed(self):
        return "Rack {} (NUM: {}, IP: {}, DOORS: {}, STATE: {}, MASTER: {})".format(self.name, self.number,
                                                                                    self.ip_address, self.doors_count,
                                                                                    self.state, self.master)

    def get_cases(self):
        cases = Cases.objects.filter(rack=self)
        outcases = []
        if len(cases) > 0:
            mid = len(cases) // 2
            for i in range(mid):
                outcases.append((cases[i], cases[i + mid]))
        return outcases


class Shifts(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.TextField()
    start = models.TimeField()
    end = models.TimeField()
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'shifts'

    @staticmethod
    def allowed_order():
        return ["id", "name", "start", "end"]

    def get_printed(self):
        return "{} (START: {}, END: {})".format(self.name, self.start, self.end)

    def currently_valid(self):
        now = timezone.now().time()

        if self.start > self.end:
            return self.start <= now or now <= self.end
        return self.start <= now <= self.end


class UserAdminLogs(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey('Users', models.DO_NOTHING, db_column='user', related_name='+')
    rack = models.ForeignKey(Racks, models.DO_NOTHING, db_column='rack', related_name='+', blank=True, null=True)
    web = models.IntegerField()
    phone = models.IntegerField()
    valid = models.IntegerField()
    creation_timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'user_admin_logs'


class UserJobs(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey('Users', models.DO_NOTHING, db_column='user', related_name='+')
    job = models.ForeignKey(Jobs, models.DO_NOTHING, db_column='job', related_name='+')
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'user_jobs'


class UserLogs(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey('Users', models.DO_NOTHING, db_column='user', related_name='+')
    rack = models.ForeignKey(Racks, models.DO_NOTHING, db_column='rack', related_name='+')
    valid = models.IntegerField()
    creation_timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'user_logs'


class UserPasswords(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey('Users', models.DO_NOTHING, db_column='user', related_name='+')
    password = models.TextField()
    creation_timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'user_passwords'


class UserMessages(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey('Users', models.DO_NOTHING, db_column='user', related_name='+')
    message = models.TextField()
    start = models.DateTimeField()
    end = models.DateTimeField(default=None, null=True)
    read = models.DateTimeField(default=None, null=True)
    creation_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'user_messages'


class Users(AbstractUser):
    username = None
    last_name = None
    first_name = None
    is_staff = None
    is_active = True
    is_superuser = None
    date_joined = None

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name', 'surname']

    id = models.BigAutoField(primary_key=True)
    id_internal_company = models.CharField(unique=True, max_length=255)
    email = models.CharField(unique=True, max_length=255, blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    ihm_password = models.TextField(blank=True, null=True)
    phone = models.TextField(blank=True, null=True)
    name = models.TextField()
    surname = models.TextField()
    disabled = models.BooleanField(db_column='disabled')
    manager = models.ForeignKey('self', models.DO_NOTHING, db_column='manager', related_name='+', blank=True, null=True)
    lang = models.CharField(max_length=255)
    shift = models.ForeignKey(Shifts, models.DO_NOTHING, db_column='shift', related_name='+', blank=True, null=True)
    first_login = models.DateTimeField(blank=True, null=True)
    last_login = models.DateTimeField(blank=True, null=True)
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField(blank=True, null=True)
    creation_user = models.ForeignKey('self', models.DO_NOTHING, db_column='creation_user', related_name='+',
                                      blank=True, null=True)
    update_user = models.ForeignKey('self', models.DO_NOTHING, db_column='update_user', related_name='+', blank=True,
                                    null=True)

    class Meta:
        managed = False
        db_table = 'users'

    @staticmethod
    def allowed_order():
        return ["id", "id_internal_company", "email", "name", "surname", "disabled", "last_login", "manager"]

    def is_deleted(self):
        return self.name == "REMOVED"

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        if self.ihm_password is not None:
            return 'ADMIN VENDOR'
        elif self.is_deleted():
            return 'DELETED USER'
        aes = AesEncryption()
        full_name = '%s %s' % (aes.decrypt(self.name), aes.decrypt(self.surname))
        return full_name.strip()

    def get_printed_jobs(self):
        jobs = ""
        for job in UserJobs.objects.filter(user=self):
            jobs += "{}, ".format(job.job.name)
        return jobs[:-2] if len(jobs) > 2 else ""

    def get_short_name(self):
        """Return the short name for the user."""
        if self.ihm_password is not None:
            return 'ADMIN VENDOR'
        elif self.is_deleted():
            return 'DELETED USER'
        aes = AesEncryption()
        name = aes.decrypt(self.name)
        surname = aes.decrypt(self.surname)
        short_name = '%s %s' % (name, surname[0:1] if len(surname) >= 1 else "")
        return short_name.strip()

    def get_last_login(self):
        compare = []
        if self.last_login is not None:
            compare.append(self.last_login)
        ual = UserAdminLogs.objects.filter(user=self).order_by("-creation_timestamp")
        if ual.count() > 0:
            compare.append(ual.first().creation_timestamp)
        ul = UserLogs.objects.filter(user=self, valid__gt=0).order_by("-creation_timestamp")
        if ul.count() > 0:
            compare.append(ul.first().creation_timestamp)

        last = None
        for val in compare:
            if last is None:
                last = val
            else:
                last = val if val > last else last

        return last

    def get_badges(self):
        if self.is_deleted():
            return []

        encid = AesEncryption().encrypt(str(self.id), "rfid")
        return Badges.objects.filter(user=encid).all()

    def get_kits_out(self):
        if self.is_deleted():
            return None

        kits = []
        for kit in reversed(KitLogs.objects.filter(user=self, out=1).order_by("-id")[:20]):
            if KitLogs.objects.filter(kit=kit.kit, out=0, creation_timestamp__gt=kit.creation_timestamp).count() == 0:
                kits.append(kit.kit)
        if len(kits) == 0:
            kits = None
        return kits


class Badges(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.TextField()
    rfid = models.TextField()
    user = models.TextField()
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey('Users', models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'badges'

    @staticmethod
    def allowed_order():
        return ["id", "name", "rfid"]

    def get_printed(self):
        user = self.get_user()
        return "{} (TAG: {}, USER: {}".format(self.name, self.rfid, user.get_full_name() if user else "None")

    def get_user(self):
        if self.user is None or len(self.user) == 0:
            return None
        userid = AesEncryption().decrypt(self.user)
        user = Users.objects.filter(id=userid)
        if user.count() > 0:
            return user.first()
        return None


class UvcLamp(models.Model):
    id = models.BigAutoField(primary_key=True)
    number = models.IntegerField()
    counter = models.BigIntegerField()
    lifetime = models.BigIntegerField()
    rack = models.ForeignKey(Racks, models.DO_NOTHING, db_column='rack', related_name='+')
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey(Users, models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey(Users, models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'uvc_lamp'


class ProductIncidents(models.Model):
    id = models.BigAutoField(primary_key=True)
    product = models.ForeignKey(Products, models.DO_NOTHING, db_column='product', related_name='+')
    type = models.IntegerField()
    solved = models.IntegerField(default=0)
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey(Users, models.DO_NOTHING, db_column='creation_user', related_name='+')
    update_user = models.ForeignKey(Users, models.DO_NOTHING, db_column='update_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'product_incidents'

    @staticmethod
    def allowed_order():
        return ["id", "solved", "type"]

    def get_type(self):
        if self.type == 0:
            return gettext("The equipment does not start")
        elif self.type == 1:
            return gettext("Cannot open the software of the equipment")
        elif self.type == 2:
            return gettext("The battery does not hold")
        elif self.type == 3:
            return gettext("Buttons are not working")
        elif self.type == 4:
            return gettext("The equipment is not correct")
        elif self.type == 5:
            return gettext("Equipment out for too long")
        return gettext("Unknown incident !")

    def get_last_comment(self):
        comment = ProductIncidentsComment.objects.filter(incident=self).order_by("-creation_timestamp")
        if comment.count() == 0:
            return None
        return comment.first()

    def get_comment_history(self):
        return ProductIncidentsComment.objects.filter(incident=self).order_by("-creation_timestamp").all()


class ProductIncidentsComment(models.Model):
    id = models.BigAutoField(primary_key=True)
    incident = models.ForeignKey(ProductIncidents, models.DO_NOTHING, db_column='incident', related_name='+')
    comment = models.TextField(max_length=512)
    creation_timestamp = models.DateTimeField()
    creation_user = models.ForeignKey(Users, models.DO_NOTHING, db_column='creation_user', related_name='+')

    class Meta:
        managed = False
        db_table = 'product_incidents_comment'


class LogsCasesOpen(models.Model):
    id = models.BigAutoField(primary_key=True)
    rack = models.CharField(max_length=256)
    case = models.IntegerField()
    creation_timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'logs_cases_open'


class LogsKitTransaction(models.Model):
    id = models.BigAutoField(primary_key=True)
    log_case = models.ForeignKey(LogsCasesOpen, models.DO_NOTHING, db_column='log_case')
    assigned = models.IntegerField()
    out = models.IntegerField()
    creation_timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'logs_kit_transaction'


class LogsProductTransaction(models.Model):
    id = models.BigAutoField(primary_key=True)
    log_kit = models.ForeignKey(LogsKitTransaction, models.DO_NOTHING, db_column='log_kit')
    family = models.TextField()
    model = models.TextField()
    brand = models.TextField()
    serial = models.TextField()
    creation_timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'logs_product_transaction'


class ProductJobOut(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey('Users', models.DO_NOTHING, db_column='user')
    job = models.ForeignKey(Jobs, models.DO_NOTHING, db_column='job')
    product_out = models.IntegerField()
    creation_timestamp = models.DateTimeField()
    update_timestamp = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'product_job_out'


class LogsForensic(models.Model):
    id = models.BigAutoField(primary_key=True)
    level = models.IntegerField()
    source = models.CharField(max_length=128)
    log = models.TextField()
    creation_timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'logs_forensic'
