import json

from django import template
from django.template import TemplateSyntaxError, Node
from django.utils.translation import gettext, get_language

from website.apps.web.config import BatteriesActivated
from website.apps.web.encryption import AesEncryption
from website.apps.web.models import ProductIncidents

register = template.Library()


@register.simple_tag()
def calc_price(qty, *args, **kwargs):
    return qty / 100


@register.simple_tag()
def decrypt(inp, *args, **kwargs):
    return AesEncryption().decrypt(inp)


@register.simple_tag()
def formatused(inp, *args, **kwargs):
    if inp < 14400:
        return str(inp // 60) + " " + gettext("minute(s)")
    return str(inp // 3600) + " " + gettext("hour(s)")


@register.tag("company_name")
def company_name(parser, token):
    args = token.contents.split()
    if len(args) != 3 or args[1] != 'as':
        raise TemplateSyntaxError("'company_name' requires 'as variable' (got %r)" % args)
    return GetCurrentCompanyName(args[2])


class GetCurrentCompanyName(Node):
    def __init__(self, variable):
        self.variable = variable

    def render(self, context):
        try:
            with open("company.txt", "r") as f:
                name = f.read()[0:40]
        except Exception:
            name = ""
        context[self.variable] = name
        return ''


@register.tag("battery_system")
def battery_system(parser, token):
    args = token.contents.split()
    if len(args) != 3 or args[1] != 'as':
        raise TemplateSyntaxError("'battery_system' requires 'as variable' (got %r)" % args)
    return GetCurrentBatteryMode(args[2])


class GetCurrentBatteryMode(Node):
    def __init__(self, variable):
        self.variable = variable

    def render(self, context):
        context[self.variable] = not BatteriesActivated()
        return ''


@register.tag("incidents_remaining")
def incidents_remaining(parser, token):
    args = token.contents.split()
    if len(args) != 3 or args[1] != 'as':
        raise TemplateSyntaxError("'incidents_remaining' requires 'as variable' (got %r)" % args)
    return GetCurrentIncidentsRemaining(args[2])


class GetCurrentIncidentsRemaining(Node):
    def __init__(self, variable):
        self.variable = variable

    def render(self, context):
        context[self.variable] = min(ProductIncidents.objects.filter(solved=0).count(), 99)
        return ''


@register.simple_tag()
def picture_width(products):
    plen = len(products)
    return (92 - (plen - 1)) // plen


@register.simple_tag()
def jobs_to_list(inp, *args, **kwargs):
    ls = []
    if inp is not None:
        for job in inp:
            ls.append(job.id)
    return json.dumps(ls)


@register.simple_tag()
def cdate(date, *args, **kwargs):
    lang = get_language()
    if lang == "fr":
        return date.strftime("%d-%m-%Y")
    return date.strftime("%m-%d-%Y")


@register.simple_tag()
def cdateslash(date, *args, **kwargs):
    return cdate(date).replace("-", "/")
