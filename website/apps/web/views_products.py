# Create your views here.
import ipaddress
import os

from django.shortcuts import redirect, render
from django.utils import timezone
from django.views import View

from extranet import settings
from website.apps.web import models
from website.apps.web.config import IdentificationActivated
from website.apps.web.encryption import AesEncryption
from website.apps.web.forms_products import AddProductFamilyForm, AddProductTypeForm, AddBatteryTypeForm, \
    AddBatteryForm, AddProductForm, CaseSwitchForm, KitAssignForm, IncidentSolvedForm, ProductDeleteForm, \
    EditProductForm, EditProductFamilyForm, IncidentCommentForm, EditProductTypeForm, ImportProductsForm
from website.apps.web.mail_util import send_incident_update
from website.apps.web.models import ProductFamily, ProductModel, BatteryType, Battery, Racks, Kits, Users, KitLogs, \
    Cases, UserAdminLogs, UserLogs, UvcLamp
from website.apps.web.util import get_pagination, log_web
from website.apps.web.views import REDIRECT_IF_NOT_LOGGED, is_logged


class Products(View):
    def return_values(self, request, import_errors=None):
        req = None
        if "fam" in request.GET:
            try:
                val = int(request.GET['fam'])

                if val > 0:
                    fam = ProductFamily.objects.filter(id=val)
                    if fam.count() > 0:
                        req = models.Products.objects.filter(product_type__family=fam.first())
            except Exception:
                pass
        if req is None:
            req = models.Products.objects.all()

        search = ""
        if "search" in request.GET:
            search = request.GET["search"].lower()
            aes = AesEncryption()
            tproduct = []
            for product in req:
                id_tag = product.id_tag if product.id_tag else ""
                for elem in search.split(" "):
                    if elem in id_tag.lower() or elem in product.local_id.lower() \
                            or elem in product.serial_number.lower() or elem in product.product_type.name.lower() \
                            or elem in product.product_type.model.lower() \
                            or elem in product.product_type.brand.lower() \
                            or elem in product.product_type.family.name.lower():
                        tproduct.append(product)
                        break
                    else:
                        valid = False
                        for assigned in product.get_assigned():
                            user = assigned.user
                            if elem in aes.decrypt(user.name).lower() or elem in aes.decrypt(user.surname).lower():
                                valid = True
                                break
                        if valid:
                            tproduct.append(product)
                            break
            req = tproduct

        if "order" in request.GET:
            ordering = request.GET["order"]
            if ordering in models.Products.allowed_order():
                if "orderdesc" in request.GET:
                    ordering = "-" + ordering
                req = req.order_by(ordering)
            elif ordering == "location":
                req = sorted(list(req), key=lambda prod: prod.locate(), reverse="orderdesc" in request.GET)

        (products, page, max_page, pages) = get_pagination(request, req)

        all_products = list(models.Products.objects.all())

        return render(request, 'products/products.html',
                      {"products": products, "families": ProductFamily.objects.all(), "all_products": all_products,
                       "battery_types": BatteryType.objects.all(), "prod_ident": IdentificationActivated(),
                       "page": page, "max_page": max_page, "pages": pages, "search": search,
                       "import_errors": import_errors})

    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        return self.return_values(request)

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        import_errors = []

        if "import_users" in request.POST:
            form = ImportProductsForm(request.POST, request.FILES)
        elif "export_users" in request.POST:
            form = None
        else:
            form = AddProductForm(request.POST)

        if form is not None and form.is_valid():
            if "import_users" in request.POST:
                added = form.save(request.user)
                ufullname = request.user.get_full_name()
                for product in added:
                    log_web(1, request,
                            "{} has added the product {}".format(ufullname, product.get_printed()))
                import_errors = form.import_errors
            else:
                form.save(request.user)
                log_web(1, request,
                        request.user.get_full_name() + " has added a product : " + form.product.get_printed())
        elif form is None:
            products_json = []
            for product in models.Products.objects.all():
                if "product_{}".format(product.id) in request.POST:
                    data = {
                        "type": product.product_type.reference,
                        "serial_number": product.serial_number,
                        "id_tag": product.id_tag if product.id_tag else "",
                        "local_id": product.local_id
                    }
                    products_json.append(data)
            if len(products_json) > 0:
                path = os.path.join(settings.BASE_DIR, "static/log")
                if not os.path.lexists(path):
                    os.makedirs(path)
                logfile = os.path.join(path, "exported_products.csv")

                lines = []
                for product in products_json:
                    line = ""
                    for key, value in product.items():
                        line += value + ";"
                    lines.append(line[:-1] + "\n")

                with open(logfile, "w") as file:
                    file.writelines(lines)

                return redirect("/static/log/exported_products.csv")

        return self.return_values(request, import_errors=import_errors)


class ProductFamilies(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)
        return render(request, 'products/products_families.html', {"families": ProductFamily.objects.all()})

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        if "edit_family" in request.POST:
            form = EditProductFamilyForm(request.POST)
            if form.is_valid():
                form.save(request.user)
                log_web(2, request,
                        request.user.get_full_name() + " has edited a product family : " + form.family.name)
        else:
            form = AddProductFamilyForm(request.POST)
            if form.is_valid():
                form.save(request.user)
                log_web(1, request,
                        request.user.get_full_name() + " has added a product family : " + form.family.name)

        return render(request, 'products/products_families.html', {"families": ProductFamily.objects.all()})


class ProductModels(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        if "js" in request.GET:
            js = None
            try:
                js = ProductFamily.objects.filter(id=int(request.GET['js']))
            except Exception:
                pass
            if js is not None and js.count() > 0:
                js = js.first()
            modelList = ProductModel.objects
            if js:
                modelList = modelList.filter(family=js)
            return render(request, 'products/js_products_types.html', {"models": modelList.all()})

        (modelsL, page, max_page, pages) = get_pagination(request, ProductModel.objects.all())

        return render(request, 'products/products_types.html',
                      {"models": modelsL, "families": ProductFamily.objects.all(), "page": page, "max_page": max_page,
                       "pages": pages})

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        if "edit_picture" in request.POST or "remove_picture" in request.POST:
            form = EditProductTypeForm(request.POST, request.FILES)
        else:
            form = AddProductTypeForm(request.POST, request.FILES)

        if "edit_picture" in request.POST or "remove_picture" in request.POST:
            if form.is_valid():
                form.save(request.user)
                log_web(1, request,
                        request.user.get_full_name() + " has edited a product model picture")
        else:
            if form.is_valid():
                form.save(request.user)
                log_web(1, request,
                        request.user.get_full_name() + " has added a product model : " + form.type.get_printed())

        (modelsL, page, max_page, pages) = get_pagination(request, ProductModel.objects.all())

        return render(request, 'products/products_types.html',
                      {"models": modelsL, "families": ProductFamily.objects.all(), "page": page, "max_page": max_page,
                       "pages": pages})


class ProductBatteries(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        return render(request, 'products/batteries.html',
                      {"batteries": Battery.objects.all(), "battery_types": BatteryType.objects.all(),
                       "racks": Racks.objects.all(), "prod_ident": IdentificationActivated()})

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        form = AddBatteryForm(request.POST)

        if form.is_valid():
            form.save(request.user)

        return render(request, 'products/batteries.html',
                      {"batteries": Battery.objects.all(), "battery_types": BatteryType.objects.all(),
                       "racks": Racks.objects.all(), "prod_ident": IdentificationActivated()})


class ProductBatteryTypes(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)
        return render(request, 'products/products_battery_types.html',
                      {"battery_types": BatteryType.objects.all(), "prod_ident": IdentificationActivated()})

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        form = AddBatteryTypeForm(request.POST)

        if form.is_valid():
            form.save(request.user)

        return render(request, 'products/products_battery_types.html',
                      {"battery_types": BatteryType.objects.all(), "prod_ident": IdentificationActivated()})


class ProductIncidents(View):
    def get(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        incidents = models.ProductIncidents.objects.order_by("solved").order_by("-creation_timestamp")
        (incidents, page, max_page, pages) = get_pagination(request, incidents)

        return render(request, 'products/incidents.html',
                      {"incidents": incidents, "page": page, "max_page": max_page, "pages": pages})


class RacksView(View):
    def get(self, request):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        return render(request, 'racks/racks.html', {'racks': Racks.objects.order_by("number").all(), 'goto': None})

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        rackEdit = None
        if "rack" in request.POST and (
                "activation" in request.POST or "edit_ip" in request.POST or "edit_model" in request.POST
                or "delete_rack" in request.POST or "reset_rack" in request.POST):
            rack = Racks.objects.filter(id=int(request.POST['rack']))
            if rack.count() > 0:
                rack = rack.first()
                edit = False
                if "activation" in request.POST:
                    rack.state = 0 if rack.state else 1
                    log_web(2, request, "{} has edited the rack {} set state to {}".format(request.user.get_full_name(),
                                                                                           rack.get_printed(),
                                                                                           str(rack.state)))
                    edit = True
                elif "ip" in request.POST:
                    try:
                        ipaddress.ip_address(request.POST["ip"])
                        rack.ip_address = request.POST["ip"]
                        log_web(2, request,
                                "{} has edited the rack {} set ip to {}".format(request.user.get_full_name(),
                                                                                rack.get_printed(),
                                                                                rack.ip_address))
                        edit = True
                    except Exception:
                        pass
                elif "door_count" in request.POST:
                    try:
                        door_count = int(request.POST["door_count"])
                        old_count = rack.doors_count
                        if door_count != old_count:
                            if door_count == 8:
                                for case in rack.get_cases():
                                    if case.position > 8:
                                        if case.kit is not None:
                                            for prod in case.kit.get_products():
                                                prod.kit = None
                                                prod.save()
                                            for kitlog in KitLogs.objects.filter(kit=case.kit):
                                                kitlog.delete()
                                            case.kit.delete()
                                        case.delete()
                                edit = True
                            elif door_count == 16:
                                for i in range(9, 17):
                                    case = Cases()
                                    case.kit = None
                                    case.rack = rack
                                    case.active = True
                                    case.position = i
                                    case.creation_timestamp = timezone.now()
                                    case.update_timestamp = timezone.now()
                                    case.creation_user = request.user
                                    case.update_user = request.user
                                    case.save()
                                edit = True
                        if edit:
                            rack.doors_count = door_count
                            rack.partnumber = "MAM{}UV".format("08" if door_count == 8 else "16")
                            log_web(2, request,
                                    "{} has edited the rack {} set door count from {} to {}".format(
                                        request.user.get_full_name(),
                                        rack.get_printed(),
                                        old_count, door_count))
                    except Exception:
                        pass
                elif "delete_rack" in request.POST:
                    password = request.POST["password"]
                    if request.user.check_password(password):
                        for case in rack.get_cases():
                            if case.kit is not None:
                                for prod in case.kit.get_products():
                                    prod.kit = None
                                    prod.save()
                                for kitlog in KitLogs.objects.filter(kit=case.kit):
                                    kitlog.delete()
                                case.kit.delete()
                            case.delete()
                        for useradminlog in UserAdminLogs.objects.filter(rack=rack):
                            useradminlog.rack = None
                            useradminlog.save()
                        for userlog in UserLogs.objects.filter(rack=rack):
                            userlog.delete()
                        for uvclamp in UvcLamp.objects.filter(rack=rack):
                            uvclamp.delete()
                        for battery in Battery.objects.filter(rack=rack):
                            battery.delete()
                        log_web(2, request,
                                "{} has delete the rack {}".format(
                                    request.user.get_full_name(),
                                    rack.get_printed()))
                        rack.delete()
                elif "reset_rack" in request.POST:
                    password = request.POST["password"]
                    if request.user.check_password(password):
                        for case in rack.get_cases():
                            if case.kit is not None:
                                for prod in case.kit.get_products():
                                    prod.kit = None
                                    prod.save()
                                for kitlog in KitLogs.objects.filter(kit=case.kit):
                                    kitlog.delete()
                                case.kit.delete()
                            case.kit = None
                            case.save()
                        log_web(2, request,
                                "{} has reseted the rack {}".format(
                                    request.user.get_full_name(),
                                    rack.get_printed()))
                        edit = True

                if edit:
                    rack.update_user = request.user
                    rack.update_timestamp = timezone.now()
                    rack.save()
                rackEdit = rack.id
        else:
            form = CaseSwitchForm(request.POST)
            if form.is_valid():
                form.save(request.user)
                log_web(1, request,
                        "{} has edited the rack {} set case status to {} on {}".format(request.user.get_full_name(),
                                                                                       form.case.rack.get_printed(),
                                                                                       str(form.case.active),
                                                                                       form.case.position))
                rackEdit = form.case.rack.id

        return render(request, 'racks/racks.html', {'racks': Racks.objects.order_by("number").all(), "goto": rackEdit})


class KitData(View):
    def get(self, request, *args, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        if "kit" not in kwargs:
            return redirect("/racks/")
        kit = Kits.objects.filter(id=kwargs['kit'])
        if kit.count() == 0:
            return redirect("/racks/")
        kit = kit.first()

        return render(request, 'racks/kit_data.html',
                      {'kit': kit, "users": Users.objects.filter(ihm_password__isnull=True),
                       "prod_ident": IdentificationActivated()})

    def post(self, request, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        form = KitAssignForm(request.POST)

        if form.is_valid():
            form.save(request.user)
            log_web(1, request,
                    "{} has edited the kit {} set assignation to {}".format(request.user.get_full_name(),
                                                                            form.kit.get_printed(),
                                                                            "False" if not form.assignable else "True [{}]".format(
                                                                                form.kit.get_printed_assigned())))

        return render(request, 'racks/kit_data.html',
                      {'kit': form.kit, "users": Users.objects.filter(ihm_password__isnull=True),
                       "prod_ident": IdentificationActivated()})


class ProductData(View):
    def filter_and_load_data(self, request, kwargs):
        if "product" not in kwargs:
            return redirect("/products/")
        product = models.Products.objects.filter(id=kwargs['product'])
        if product.count() == 0:
            return redirect("/products/")
        product = product.first()

        case = None
        userOut = None
        if product.kit:
            case = models.Cases.objects.filter(kit=product.kit)
            if case.count() > 0:
                case = case.first()
            else:
                case = None

            last_kit = KitLogs.objects.filter(kit=product.kit).order_by("-id")
            if last_kit.count() > 0:
                last_kit = last_kit.first()
                if last_kit.out == 1:
                    userOut = last_kit.user

        incidents = models.ProductIncidents.objects.filter(product=product).order_by("solved")

        logs = models.KitLogs.objects.filter(kit=product.kit,
                                             creation_timestamp__gte=timezone.now() - timezone.timedelta(
                                                 days=30)).order_by(
            "-creation_timestamp").all()[:100] if product.kit else None

        return render(request, 'products/product_data.html',
                      {'product': product, "case": case, "userOut": userOut, "prod_ident": IdentificationActivated(),
                       "incidents": incidents, "families": ProductFamily.objects.all(), "back": "/products/",
                       "models": ProductModel.objects.filter(family=product.product_type.family).all(), "logs": logs})

    def get(self, request, *args, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        return self.filter_and_load_data(request, kwargs)

    def post(self, request, *args, **kwargs):
        if not is_logged(request):
            return redirect(REDIRECT_IF_NOT_LOGGED)

        if "edit_product" in request.POST:
            form = EditProductForm(request.POST)
            if form.is_valid():
                form.save(request.user)
                log_web(2, request,
                        "{} has edited the product {}".format(request.user.get_full_name(),
                                                              form.product.get_printed()))
        if "delete_product" in request.POST:
            form = ProductDeleteForm(request.POST)
            if form.is_valid():
                log_web(2, request,
                        "{} has deleted the product {}".format(request.user.get_full_name(),
                                                               form.product.get_printed()))
                if form.save():
                    return redirect("/products/")
        if "solve_incident" in request.POST:
            form = IncidentSolvedForm(request.POST)
            if form.is_valid():
                form.save(request.user)
        elif "comment_incident" in request.POST:
            form = IncidentCommentForm(request.POST)
            if form.is_valid():
                form.save(request.user)

                if settings.EMAIL_ACTIVATED:
                    send_incident_update(request, form.incident.product, form.incident, form.comment)

        return self.filter_and_load_data(request, kwargs)
