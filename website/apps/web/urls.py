from django.urls import path

from website.apps.web import views, views_products, views_users

app_name = 'web'

urlpatterns = [
    path('', views.Index.as_view(), name='index'),
    path('login/', views.Login.as_view(), name='login'),
    path('users/', views_users.Users.as_view(), name='users'),
    path('users/<int:user>/', views_users.UserData.as_view(), name='user_data'),
    path('jobs/', views_users.Jobs.as_view(), name='jobs'),
    path('shifts/', views_users.Shifts.as_view(), name='shifts'),
    path('shifts/users/', views_users.ShiftUserList.as_view(), name='shift_user_list'),
    path('badges/', views_users.Badges.as_view(), name='badges'),
    path('messages/', views_users.Messages.as_view(), name='messages'),
    path('messages/<int:message>/', views_users.MessagesEdit.as_view(), name='messages_edit'),
    path('products/', views_products.Products.as_view(), name='products'),
    path('products/<int:product>/', views_products.ProductData.as_view(), name='product_data'),
    path('products/families/', views_products.ProductFamilies.as_view(), name='product_families'),
    path('products/models/', views_products.ProductModels.as_view(), name='product_models'),
    path('products/batteries/', views_products.ProductBatteries.as_view(), name='product_batteries'),
    path('products/batteries/models/', views_products.ProductBatteryTypes.as_view(), name='product_battery_types'),
    path('products/incidents/', views_products.ProductIncidents.as_view(), name='product_incidents'),
    path('racks/', views_products.RacksView.as_view(), name='racks'),
    path('racks/kit/<int:kit>/', views_products.KitData.as_view(), name='rack_kit_data'),
    path('stats/', views.Stats.as_view(), name='stats'),
    path('logs/', views.Logs.as_view(), name='logs'),
    path('logs/request/forensic/', views.RequestLogForensic.as_view(), name='logs_forensic'),
    path('logs/clean/', views.LogsClean.as_view(), name='logs_clean'),
    path('edit_company_logo/', views.EditCompanyLogo.as_view(), name='edit_company_logo'),
    path('docs/', views.Docs.as_view(), name='docs'),
]
