#!/bin/sh

until echo '\q' | mysql -u "$DB_USER" -p"$DB_PASSWORD" -P "$DB_PORT" -h "$DB_HOST" "$DB_NAME"; do
    >&2 echo "MySQL is unavailable - sleeping"
    sleep 1
done

RM=false
FILE_FIRST="persistent/first_runned"
if [ ! -f $FILE_FIRST ]; then
    touch $FILE_FIRST
    echo "-- First container startup --"

    echo "-- Setting up cron --"
    # write out current crontab
    crontab -l > mycron
    # echo new cron into cron file
    echo "00 */6 * * * sh $CWD/cron.sh" >> mycron
    # install new cron file
    crontab mycron
    rm mycron

    echo "-- Building up the database --"
    RM=true
else
    if [ "$1" = "gunicorn" ]; then
        while ! mysqladmin ping -h"$DB_HOST" --silent; do
            sleep 1
        done

        echo "-- Starting gunicorn --"
        exec $@
        exit 0
    fi
fi

if [ "$#" -ge 1 ] && [ $1 = "true" ]; then
    RM=true
fi

if [ "$RM" = "true" ]; then
    echo "-- Removing database $DB_NAME --"
    mysql -h $DB_HOST -P $DB_PORT -u $DB_USER -p$DB_PASSWORD -e "DROP DATABASE IF EXISTS $DB_NAME;"
    mysql -h $DB_HOST -P $DB_PORT -u $DB_USER -p$DB_PASSWORD -e "CREATE DATABASE $DB_NAME;"
    echo "-- End of removal"
fi

echo "-- Starting database creation --"
mysql -u "$DB_USER" -p"$DB_PASSWORD" -P "$DB_PORT" -h "$DB_HOST" "$DB_NAME" < default.sql
echo "-- End of database creation --"

if [ "$1" = "gunicorn" ]; then
    echo "-- Starting gunicorn --"
    exec $@
fi
