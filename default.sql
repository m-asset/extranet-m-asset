SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `myasset`
--

-- --------------------------------------------------------

--
-- Structure de la table `alerts`
--

CREATE TABLE `alerts` (
  `id` bigint(20) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `creation_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `alert_product_in`
--

CREATE TABLE `alert_product_in` (
  `id` bigint(20) NOT NULL,
  `alert` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  `kit_log_out` bigint(20) NOT NULL,
  `kit_log_in` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `api_apitry`
--

CREATE TABLE `api_apitry` (
  `id` bigint(20) NOT NULL,
  `ip` varchar(128) NOT NULL,
  `time` datetime(6) NOT NULL,
  `valid` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `api_authtoken`
--

CREATE TABLE `api_authtoken` (
  `id` bigint(20) NOT NULL,
  `token` varchar(256) NOT NULL,
  `valid` tinyint(1) NOT NULL,
  `time` datetime(6) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `ip` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add permission', 1, 'add_permission'),
(2, 'Can change permission', 1, 'change_permission'),
(3, 'Can delete permission', 1, 'delete_permission'),
(4, 'Can view permission', 1, 'view_permission'),
(5, 'Can add group', 2, 'add_group'),
(6, 'Can change group', 2, 'change_group'),
(7, 'Can delete group', 2, 'delete_group'),
(8, 'Can view group', 2, 'view_group'),
(9, 'Can add content type', 3, 'add_contenttype'),
(10, 'Can change content type', 3, 'change_contenttype'),
(11, 'Can delete content type', 3, 'delete_contenttype'),
(12, 'Can view content type', 3, 'view_contenttype'),
(13, 'Can add session', 4, 'add_session'),
(14, 'Can change session', 4, 'change_session'),
(15, 'Can delete session', 4, 'delete_session'),
(16, 'Can view session', 4, 'view_session'),
(17, 'Can add users', 5, 'add_users'),
(18, 'Can change users', 5, 'change_users'),
(19, 'Can delete users', 5, 'delete_users'),
(20, 'Can view users', 5, 'view_users'),
(21, 'Can add alert product in', 6, 'add_alertproductin'),
(22, 'Can change alert product in', 6, 'change_alertproductin'),
(23, 'Can delete alert product in', 6, 'delete_alertproductin'),
(24, 'Can view alert product in', 6, 'view_alertproductin'),
(25, 'Can add alerts', 7, 'add_alerts'),
(26, 'Can change alerts', 7, 'change_alerts'),
(27, 'Can delete alerts', 7, 'delete_alerts'),
(28, 'Can view alerts', 7, 'view_alerts'),
(29, 'Can add battery', 8, 'add_battery'),
(30, 'Can change battery', 8, 'change_battery'),
(31, 'Can delete battery', 8, 'delete_battery'),
(32, 'Can view battery', 8, 'view_battery'),
(33, 'Can add battery logs', 9, 'add_batterylogs'),
(34, 'Can change battery logs', 9, 'change_batterylogs'),
(35, 'Can delete battery logs', 9, 'delete_batterylogs'),
(36, 'Can view battery logs', 9, 'view_batterylogs'),
(37, 'Can add battery type', 10, 'add_batterytype'),
(38, 'Can change battery type', 10, 'change_batterytype'),
(39, 'Can delete battery type', 10, 'delete_batterytype'),
(40, 'Can view battery type', 10, 'view_batterytype'),
(41, 'Can add cases', 11, 'add_cases'),
(42, 'Can change cases', 11, 'change_cases'),
(43, 'Can delete cases', 11, 'delete_cases'),
(44, 'Can view cases', 11, 'view_cases'),
(45, 'Can add general config', 12, 'add_generalconfig'),
(46, 'Can change general config', 12, 'change_generalconfig'),
(47, 'Can delete general config', 12, 'delete_generalconfig'),
(48, 'Can view general config', 12, 'view_generalconfig'),
(49, 'Can add jobs', 13, 'add_jobs'),
(50, 'Can change jobs', 13, 'change_jobs'),
(51, 'Can delete jobs', 13, 'delete_jobs'),
(52, 'Can view jobs', 13, 'view_jobs'),
(53, 'Can add kit assignations', 14, 'add_kitassignations'),
(54, 'Can change kit assignations', 14, 'change_kitassignations'),
(55, 'Can delete kit assignations', 14, 'delete_kitassignations'),
(56, 'Can view kit assignations', 14, 'view_kitassignations'),
(57, 'Can add kit logs', 15, 'add_kitlogs'),
(58, 'Can change kit logs', 15, 'change_kitlogs'),
(59, 'Can delete kit logs', 15, 'delete_kitlogs'),
(60, 'Can view kit logs', 15, 'view_kitlogs'),
(61, 'Can add kits', 16, 'add_kits'),
(62, 'Can change kits', 16, 'change_kits'),
(63, 'Can delete kits', 16, 'delete_kits'),
(64, 'Can view kits', 16, 'view_kits'),
(65, 'Can add kit type product family', 17, 'add_kittypeproductfamily'),
(66, 'Can change kit type product family', 17, 'change_kittypeproductfamily'),
(67, 'Can delete kit type product family', 17, 'delete_kittypeproductfamily'),
(68, 'Can view kit type product family', 17, 'view_kittypeproductfamily'),
(69, 'Can add kit types', 18, 'add_kittypes'),
(70, 'Can change kit types', 18, 'change_kittypes'),
(71, 'Can delete kit types', 18, 'delete_kittypes'),
(72, 'Can view kit types', 18, 'view_kittypes'),
(73, 'Can add product family', 19, 'add_productfamily'),
(74, 'Can change product family', 19, 'change_productfamily'),
(75, 'Can delete product family', 19, 'delete_productfamily'),
(76, 'Can view product family', 19, 'view_productfamily'),
(77, 'Can add product model', 20, 'add_productmodel'),
(78, 'Can change product model', 20, 'change_productmodel'),
(79, 'Can delete product model', 20, 'delete_productmodel'),
(80, 'Can view product model', 20, 'view_productmodel'),
(81, 'Can add products', 21, 'add_products'),
(82, 'Can change products', 21, 'change_products'),
(83, 'Can delete products', 21, 'delete_products'),
(84, 'Can view products', 21, 'view_products'),
(85, 'Can add racks', 22, 'add_racks'),
(86, 'Can change racks', 22, 'change_racks'),
(87, 'Can delete racks', 22, 'delete_racks'),
(88, 'Can view racks', 22, 'view_racks'),
(89, 'Can add shifts', 23, 'add_shifts'),
(90, 'Can change shifts', 23, 'change_shifts'),
(91, 'Can delete shifts', 23, 'delete_shifts'),
(92, 'Can view shifts', 23, 'view_shifts'),
(93, 'Can add user admin logs', 24, 'add_useradminlogs'),
(94, 'Can change user admin logs', 24, 'change_useradminlogs'),
(95, 'Can delete user admin logs', 24, 'delete_useradminlogs'),
(96, 'Can view user admin logs', 24, 'view_useradminlogs'),
(97, 'Can add user jobs', 25, 'add_userjobs'),
(98, 'Can change user jobs', 25, 'change_userjobs'),
(99, 'Can delete user jobs', 25, 'delete_userjobs'),
(100, 'Can view user jobs', 25, 'view_userjobs'),
(101, 'Can add user logs', 26, 'add_userlogs'),
(102, 'Can change user logs', 26, 'change_userlogs'),
(103, 'Can delete user logs', 26, 'delete_userlogs'),
(104, 'Can view user logs', 26, 'view_userlogs'),
(105, 'Can add user passwords', 27, 'add_userpasswords'),
(106, 'Can change user passwords', 27, 'change_userpasswords'),
(107, 'Can delete user passwords', 27, 'delete_userpasswords'),
(108, 'Can view user passwords', 27, 'view_userpasswords'),
(109, 'Can add uvc lamp', 28, 'add_uvclamp'),
(110, 'Can change uvc lamp', 28, 'change_uvclamp'),
(111, 'Can delete uvc lamp', 28, 'delete_uvclamp'),
(112, 'Can view uvc lamp', 28, 'view_uvclamp'),
(113, 'Can add Token', 29, 'add_token'),
(114, 'Can change Token', 29, 'change_token'),
(115, 'Can delete Token', 29, 'delete_token'),
(116, 'Can view Token', 29, 'view_token'),
(117, 'Can add token', 30, 'add_tokenproxy'),
(118, 'Can change token', 30, 'change_tokenproxy'),
(119, 'Can delete token', 30, 'delete_tokenproxy'),
(120, 'Can view token', 30, 'view_tokenproxy'),
(121, 'Can add auth token', 31, 'add_authtoken'),
(122, 'Can change auth token', 31, 'change_authtoken'),
(123, 'Can delete auth token', 31, 'delete_authtoken'),
(124, 'Can view auth token', 31, 'view_authtoken'),
(125, 'Can add api try', 32, 'add_apitry'),
(126, 'Can change api try', 32, 'change_apitry'),
(127, 'Can delete api try', 32, 'delete_apitry'),
(128, 'Can view api try', 32, 'view_apitry'),
(129, 'Can add badges', 33, 'add_badges'),
(130, 'Can change badges', 33, 'change_badges'),
(131, 'Can delete badges', 33, 'delete_badges'),
(132, 'Can view badges', 33, 'view_badges');

-- --------------------------------------------------------

--
-- Structure de la table `badges`
--

CREATE TABLE `badges` (
  `id` bigint(20) NOT NULL,
  `name` text NOT NULL,
  `rfid` text NOT NULL,
  `user` text DEFAULT NULL,
  `creation_timestamp` datetime NOT NULL,
  `update_timestamp` datetime NOT NULL,
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `battery`
--

CREATE TABLE `battery` (
  `id` bigint(20) NOT NULL,
  `serial_number` varchar(255) NOT NULL,
  `local_id` varchar(255) NOT NULL,
  `type` bigint(20) NOT NULL,
  `used` bigint(20) NOT NULL,
  `rack` bigint(20) DEFAULT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `battery_logs`
--

CREATE TABLE `battery_logs` (
  `id` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  `battery` bigint(20) NOT NULL,
  `out` tinyint(1) NOT NULL,
  `grade` int(11) NOT NULL DEFAULT 0,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `battery_type`
--

CREATE TABLE `battery_type` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `cases`
--

CREATE TABLE `cases` (
  `id` bigint(20) NOT NULL,
  `kit` bigint(20) DEFAULT NULL,
  `rack` bigint(20) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT TRUE,
  `position` int(11) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(32, 'api', 'apitry'),
(31, 'api', 'authtoken'),
(2, 'auth', 'group'),
(1, 'auth', 'permission'),
(29, 'authtoken', 'token'),
(30, 'authtoken', 'tokenproxy'),
(3, 'contenttypes', 'contenttype'),
(4, 'sessions', 'session'),
(6, 'web', 'alertproductin'),
(7, 'web', 'alerts'),
(33, 'web', 'badges'),
(8, 'web', 'battery'),
(9, 'web', 'batterylogs'),
(10, 'web', 'batterytype'),
(11, 'web', 'cases'),
(12, 'web', 'generalconfig'),
(13, 'web', 'jobs'),
(14, 'web', 'kitassignations'),
(15, 'web', 'kitlogs'),
(16, 'web', 'kits'),
(17, 'web', 'kittypeproductfamily'),
(18, 'web', 'kittypes'),
(19, 'web', 'productfamily'),
(20, 'web', 'productmodel'),
(21, 'web', 'products'),
(22, 'web', 'racks'),
(23, 'web', 'shifts'),
(24, 'web', 'useradminlogs'),
(25, 'web', 'userjobs'),
(26, 'web', 'userlogs'),
(27, 'web', 'userpasswords'),
(5, 'web', 'users'),
(28, 'web', 'uvclamp');

-- --------------------------------------------------------

--
-- Structure de la table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2021-04-13 17:21:52.147298'),
(2, 'contenttypes', '0002_remove_content_type_name', '2021-04-13 17:21:52.300158'),
(3, 'auth', '0001_initial', '2021-04-13 17:21:52.799607'),
(4, 'auth', '0002_alter_permission_name_max_length', '2021-04-13 17:21:52.843339'),
(5, 'auth', '0003_alter_user_email_max_length', '2021-04-13 17:21:52.878333'),
(6, 'auth', '0004_alter_user_username_opts', '2021-04-13 17:21:52.905480'),
(7, 'auth', '0005_alter_user_last_login_null', '2021-04-13 17:21:52.930572'),
(8, 'auth', '0006_require_contenttypes_0002', '2021-04-13 17:21:52.953621'),
(9, 'auth', '0007_alter_validators_add_error_messages', '2021-04-13 17:21:52.980673'),
(10, 'auth', '0008_alter_user_username_max_length', '2021-04-13 17:21:53.016107'),
(11, 'auth', '0009_alter_user_last_name_max_length', '2021-04-13 17:21:53.043080'),
(12, 'auth', '0010_alter_group_name_max_length', '2021-04-13 17:21:53.090170'),
(13, 'auth', '0011_update_proxy_permissions', '2021-04-13 17:21:53.144947'),
(14, 'auth', '0012_alter_user_first_name_max_length', '2021-04-13 17:21:53.171858'),
(15, 'sessions', '0001_initial', '2021-04-13 17:21:53.327202'),
(16, 'web', '0001_initial', '2021-04-13 17:21:53.390411'),
(17, 'authtoken', '0001_initial', '2021-04-15 15:32:03.553193'),
(18, 'authtoken', '0002_auto_20160226_1747', '2021-04-15 15:32:03.581111'),
(19, 'authtoken', '0003_tokenproxy', '2021-04-15 15:32:03.601593'),
(20, 'web', '0002_badges', '2021-04-15 16:35:24.903930'),
(21, 'api', '0001_initial', '2021-04-15 16:53:02.787166'),
(22, 'api', '0002_auto_20210415_1853', '2021-04-15 16:53:43.032672'),
(23, 'api', '0003_authtoken_ip', '2021-04-15 17:06:08.683456'),
(24, 'api', '0004_apitry_valid', '2021-04-15 17:07:01.800852');

-- --------------------------------------------------------

--
-- Structure de la table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `general_config`
--

CREATE TABLE `general_config` (
  `id` bigint(20) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `update_timestamp` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `general_config`
--

INSERT INTO `general_config` (`id`, `key`, `value`, `update_timestamp`, `update_user`) VALUES
(1, 'MIN_HARDWARE_OUT_FOR_ALERT', '86400', '2021-04-13 11:06:04', 1),
(2, 'GRADES_ACTIVATED', '1', '2021-04-13 11:06:04', 1),
(3, 'MAX_PASSWORD_DAYS', '45', '2021-04-13 11:06:04', 1),
(4, 'GLOBAL_UVC_ACTIVE', '1', '2021-04-13 11:06:04', 1),
(5, 'TEMPO_START_UVC_UP', '30', '2021-04-13 11:06:04', 1),
(6, 'TEMPO_START_UVC_DOWN', '30', '2021-04-13 11:06:04', 1),
(7, 'TIME_UVC_UP', '15', '2021-04-13 11:06:04', 1),
(8, 'TIME_UVC_DOWN', '15', '2021-04-13 11:06:04', 1),
(9, 'RESTART_AFTER_INTERRUPT_DELAY', '300', '2021-04-13 11:06:04', 1),
(10, 'DOOR_OPEN_ALARM', '1', '2021-04-13 11:10:59', 1),
(11, 'BATTERY_SYSTEM_ACTIVATED', '0', '2021-04-13 11:10:59', 1),
(12, 'IDENTIFICATION_ACTIVATED', '0', '2021-05-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) NOT NULL,
  `name` text NOT NULL,
  `kit_type` bigint(20) DEFAULT NULL,
  `assign` tinyint(1) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `kits`
--

CREATE TABLE `kits` (
  `id` bigint(20) NOT NULL,
  `type` bigint(20) NOT NULL,
  `assignable` tinyint(1) NOT NULL,
  `priority` int(11) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `kit_assignations`
--

CREATE TABLE `kit_assignations` (
  `id` bigint(20) NOT NULL,
  `kit` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `kit_logs`
--

CREATE TABLE `kit_logs` (
  `id` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  `kit` bigint(20) NOT NULL,
  `out` tinyint(1) NOT NULL,
  `grade` int(11) NOT NULL DEFAULT 0,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `kit_types`
--

CREATE TABLE `kit_types` (
  `id` bigint(20) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `kit_type_product_family`
--

CREATE TABLE `kit_type_product_family` (
  `id` bigint(20) NOT NULL,
  `kit_type` bigint(20) NOT NULL,
  `product_family` bigint(20) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `creation_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) NOT NULL,
  `serial_number` varchar(255) NOT NULL,
  `id_tag` varchar(255) DEFAULT NULL,
  `local_id` varchar(255) NOT NULL,
  `product_type` bigint(20) NOT NULL,
  `kit` bigint(20) DEFAULT NULL,
  `used` bigint(20) NOT NULL,
  `battery_type` bigint(20) DEFAULT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `product_family`
--

CREATE TABLE `product_family` (
  `id` bigint(20) NOT NULL,
  `name` text NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `product_model`
--

CREATE TABLE `product_model` (
  `id` bigint(20) NOT NULL,
  `family` bigint(20) NOT NULL,
  `name` text NOT NULL,
  `brand` text NOT NULL,
  `model` text NOT NULL,
  `reference` text NOT NULL,
  `picture` text DEFAULT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `racks`
--

CREATE TABLE `racks` (
  `id` bigint(20) NOT NULL,
  `number` varchar(255) NOT NULL,
  `name` text NOT NULL,
  `state` tinyint(1) NOT NULL,
  `doors_count` int(11) NOT NULL,
  `master` tinyint(1) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `partnumber` varchar(255) NOT NULL,
  `uvc_activate` tinyint(1) NOT NULL,
  `ip_address` text NOT NULL,
  `subnet_address` text NOT NULL,
  `gtw_address` text NOT NULL,
  `ip_address_master` text NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `shifts`
--

CREATE TABLE `shifts` (
  `id` bigint(20) NOT NULL,
  `name` text NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `id_internal_company` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `ihm_password` text DEFAULT NULL,
  `phone` text DEFAULT NULL,
  `name` text NOT NULL,
  `surname` text NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `manager` bigint(20) DEFAULT NULL,
  `lang` varchar(255) NOT NULL,
  `shift` bigint(20) DEFAULT NULL,
  `first_login` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `id_internal_company`, `email`, `password`, `ihm_password`, `phone`, `name`, `surname`, `disabled`, `manager`, `lang`, `shift`, `first_login`, `last_login`, `creation_timestamp`, `update_timestamp`, `creation_user`, `update_user`) VALUES
(1, '0', 'ZW1haWxlbWFpbGVtYWlsZTroiNP9gTL1fgUtVMminQg0UXVnmjKkx94CryJoJ5aUBhSVco7z/V2caObxX2Oh91tuNfwFAyiWxnDd9T7GjPI=', 'pbkdf2_sha256$260000$t1KocRrRYcebnoT5OTdi0M$Nl79Z5DifldSVPAK0ym9oKjQo2R6Z1UMaTbWIajUGr8=', '25695478', NULL, 'KyR2S7VMOjqp8Fw+sy8a/PzWgXrpoLpKYKg7GCLnVbjpMupTj1Udd1/TH7iG61xOWf/r3jorFAXGJa5fK9UoXA==', 'pUxINMPw4PDVvK1Iq6vUOuGCcJCFw19tvhQmvpGxJRellIk2FMJfxEe4sy68yswK0NzrX+mgqm+OXMhBo++ATg==', 0, NULL, 'fr_FR', NULL, '2021-01-01 00:00:00', '2021-01-01 00:00:00', '2021-01-01 00:00:00', '2021-01-01 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user_admin_logs`
--

CREATE TABLE `user_admin_logs` (
  `id` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  `rack` bigint(20) DEFAULT NULL,
  `web` tinyint(1) NOT NULL DEFAULT 0,
  `phone` tinyint(1) NOT NULL DEFAULT 0,
  `valid` tinyint(1) NOT NULL,
  `creation_timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_jobs`
--

CREATE TABLE `user_jobs` (
  `id` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  `job` bigint(20) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `user_logs`
--

CREATE TABLE `user_logs` (
  `id` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  `rack` bigint(20) NOT NULL,
  `valid` tinyint(1) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `user_passwords`
--

CREATE TABLE `user_passwords` (
  `id` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  `password` text NOT NULL,
  `creation_timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `uvc_lamp`
--

CREATE TABLE `uvc_lamp` (
  `id` bigint(20) NOT NULL,
  `number` int(11) NOT NULL,
  `counter` bigint(20) NOT NULL,
  `lifetime` bigint(20) NOT NULL,
  `rack` bigint(20) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `product_incidents`
--

CREATE TABLE `product_incidents` (
  `id` bigint(20) NOT NULL,
  `product` bigint(20) NOT NULL,
  `type` int(11) NOT NULL,
  `solved` int(11) NOT NULL DEFAULT '0',
  `comment` text NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_user` bigint(20) NOT NULL,
  `update_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `product_incidents_comment`
--

CREATE TABLE `product_incidents_comment` (
  `id` bigint(20) NOT NULL,
  `incident` bigint(20) NOT NULL,
  `comment` text NOT NULL,
  `creation_user` bigint(20) NOT NULL,
  `creation_timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `product_job_out`
--

CREATE TABLE `product_job_out` (
  `id` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  `job` bigint(20) NOT NULL,
  `product_out` int(11) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `update_timestamp` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `api_userpasswordupdate`
--

CREATE TABLE `api_userpasswordupdate` (
  `id` bigint(20) NOT NULL,
  `time` datetime(6) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `temp` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `logs_cases_open`
--

CREATE TABLE `logs_cases_open` (
  `id` bigint(20) NOT NULL,
  `rack` varchar(256) NOT NULL,
  `case` int(11) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `logs_kit_transaction`
--

CREATE TABLE `logs_kit_transaction` (
  `id` bigint(20) NOT NULL,
  `log_case` bigint(20) NOT NULL,
  `assigned` tinyint(1) NOT NULL,
  `out` tinyint(1) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `logs_product_transaction`
--

CREATE TABLE `logs_product_transaction` (
  `id` bigint(20) NOT NULL,
  `log_kit` bigint(20) NOT NULL,
  `family` text NOT NULL,
  `model` text NOT NULL,
  `brand` text NOT NULL,
  `serial` text NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `logs_forensic`
--

CREATE TABLE `logs_forensic` (
  `id` bigint(20) NOT NULL,
  `level` int(11) NOT NULL,
  `source` varchar(128) NOT NULL,
  `log` text NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `user_messages`
--

CREATE TABLE `user_messages` (
  `id` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NULL,
  `read` datetime NULL,
  `creation_user` bigint(20) NOT NULL,
  `creation_timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `alerts`
--
ALTER TABLE `alerts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `alerts_fk0` (`creation_user`);

--
-- Index pour la table `alert_product_in`
--
ALTER TABLE `alert_product_in`
  ADD PRIMARY KEY (`id`),
  ADD KEY `alert_product_in_fk0` (`alert`),
  ADD KEY `alert_product_in_fk1` (`user`),
  ADD KEY `alert_product_in_fk2` (`kit_log_out`),
  ADD KEY `alert_product_in_fk3` (`kit_log_in`);

--
-- Index pour la table `api_apitry`
--
ALTER TABLE `api_apitry`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `api_authtoken`
--
ALTER TABLE `api_authtoken`
  ADD PRIMARY KEY (`id`),
  ADD KEY `api_authtoken_user_id_53db953f_fk_users_id` (`user_id`);

--
-- Index pour la table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Index pour la table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Index pour la table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Index pour la table `badges`
--
ALTER TABLE `badges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `badges_fk0` (`creation_user`),
  ADD KEY `badges_fk1` (`update_user`);

--
-- Index pour la table `battery`
--
ALTER TABLE `battery`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `serial_number` (`serial_number`),
  ADD UNIQUE KEY `local_id` (`local_id`),
  ADD KEY `battery_fk0` (`type`),
  ADD KEY `battery_fk1` (`creation_user`),
  ADD KEY `battery_fk2` (`update_user`),
  ADD KEY `battery_fk3` (`rack`) USING BTREE;

--
-- Index pour la table `battery_logs`
--
ALTER TABLE `battery_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `battery_log_fk0` (`user`),
  ADD KEY `battery_log_fk1` (`battery`);

--
-- Index pour la table `battery_type`
--
ALTER TABLE `battery_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `battery_type_fk0` (`creation_user`),
  ADD KEY `battery_type_fk1` (`update_user`);

--
-- Index pour la table `cases`
--
ALTER TABLE `cases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cases_fk0` (`kit`),
  ADD KEY `cases_fk1` (`rack`),
  ADD KEY `cases_fk2` (`creation_user`),
  ADD KEY `cases_fk3` (`update_user`);

--
-- Index pour la table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Index pour la table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Index pour la table `general_config`
--
ALTER TABLE `general_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `general_config_fk0` (`update_user`) USING BTREE;

--
-- Index pour la table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_fk0` (`kit_type`),
  ADD KEY `jobs_fk1` (`creation_user`),
  ADD KEY `jobs_fk2` (`update_user`);

--
-- Index pour la table `kits`
--
ALTER TABLE `kits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kits_fk0` (`type`),
  ADD KEY `kits_fk1` (`creation_user`),
  ADD KEY `kits_fk2` (`update_user`);

--
-- Index pour la table `kit_assignations`
--
ALTER TABLE `kit_assignations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kit_assignations_fk0` (`kit`),
  ADD KEY `kit_assignations_fk1` (`user`),
  ADD KEY `kit_assignations_fk3` (`creation_user`),
  ADD KEY `kit_assignations_fk4` (`update_user`);

--
-- Index pour la table `kit_logs`
--
ALTER TABLE `kit_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kit_logs_fk0` (`user`),
  ADD KEY `kit_logs_fk1` (`kit`);

--
-- Index pour la table `kit_types`
--
ALTER TABLE `kit_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kit_types_fk0` (`creation_user`),
  ADD KEY `kit_types_fk1` (`update_user`);

--
-- Index pour la table `kit_type_product_family`
--
ALTER TABLE `kit_type_product_family`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kit_type_product_family_fk0` (`kit_type`),
  ADD KEY `kit_type_product_family_fk1` (`product_family`),
  ADD KEY `kit_type_product_family_fk2` (`creation_user`);

--
-- Index pour la table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `serial_number` (`serial_number`),
  ADD KEY `products_fk0` (`product_type`),
  ADD KEY `products_fk1` (`kit`),
  ADD KEY `products_fk2` (`battery_type`),
  ADD KEY `products_fk3` (`creation_user`),
  ADD KEY `products_fk4` (`update_user`);

--
-- Index pour la table `product_family`
--
ALTER TABLE `product_family`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_family_fk0` (`creation_user`),
  ADD KEY `product_family_fk1` (`update_user`);

--
-- Index pour la table `product_model`
--
ALTER TABLE `product_model`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_model_fk0` (`family`),
  ADD KEY `product_model_fk1` (`creation_user`),
  ADD KEY `product_model_fk2` (`update_user`);

--
-- Index pour la table `racks`
--
ALTER TABLE `racks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `number` (`number`),
  ADD UNIQUE KEY `serial` (`serial`),
  ADD KEY `racks_fk0` (`creation_user`),
  ADD KEY `racks_fk1` (`update_user`);

--
-- Index pour la table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shifts_fk0` (`creation_user`),
  ADD KEY `shifts_fk1` (`update_user`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_internal_company` (`id_internal_company`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `users_fk0` (`manager`),
  ADD KEY `users_fk1` (`shift`),
  ADD KEY `users_fk2` (`creation_user`),
  ADD KEY `users_fk3` (`update_user`);

--
-- Index pour la table `user_admin_logs`
--
ALTER TABLE `user_admin_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_admin_logs_fk0` (`user`),
  ADD KEY `user_admin_logs_fk1` (`rack`);

--
-- Index pour la table `user_jobs`
--
ALTER TABLE `user_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_jobs_fk0` (`user`),
  ADD KEY `user_jobs_fk1` (`job`),
  ADD KEY `user_jobs_fk2` (`creation_user`),
  ADD KEY `user_jobs_fk3` (`update_user`);

--
-- Index pour la table `user_logs`
--
ALTER TABLE `user_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_logs_fk0` (`rack`),
  ADD KEY `user_logs_fk1` (`user`) USING BTREE;

--
-- Index pour la table `user_passwords`
--
ALTER TABLE `user_passwords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_passwords_fk0` (`user`);

--
-- Index pour la table `uvc_lamp`
--
ALTER TABLE `uvc_lamp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uvc_lamp_fk0` (`rack`),
  ADD KEY `uvc_lamp_fk1` (`creation_user`),
  ADD KEY `uvc_lamp_fk2` (`update_user`);

--
-- Index pour la table `product_incidents`
--
ALTER TABLE `product_incidents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_incidents_fk0` (`product`),
  ADD KEY `product_incidents_fk1` (`creation_user`),
  ADD KEY `product_incidents_fk2` (`update_user`);

--
-- Index pour la table `product_incidents_comment`
--
ALTER TABLE `product_incidents_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_incidents_comment_fk0` (`incident`),
  ADD KEY `product_incidents_comment_fk1` (`creation_user`);

--
-- Index pour la table `product_job_out`
--
ALTER TABLE `product_job_out`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_job_out_fk0` (`user`),
  ADD KEY `product_job_out_fk1` (`job`);

--
-- Index pour la table `api_userpasswordupdate`
--
ALTER TABLE `api_userpasswordupdate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `api_userpasswordupdate_user_id_3ab20a80_fk_users_id` (`user_id`);

--
-- Index pour la table `logs_cases_open`
--
ALTER TABLE `logs_cases_open`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `logs_kit_transaction`
--
ALTER TABLE `logs_kit_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logs_kit_transaction_fk0` (`log_case`);

--
-- Index pour la table `logs_product_transaction`
--
ALTER TABLE `logs_product_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logs_product_transaction_fk0` (`log_kit`);

--
-- Index pour la table `logs_forensic`
--
ALTER TABLE `logs_forensic`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_messages`
--
ALTER TABLE `user_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_messages_fk0` (`user`),
  ADD KEY `user_messages_fk1` (`creation_user`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `alerts`
--
ALTER TABLE `alerts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `alert_product_in`
--
ALTER TABLE `alert_product_in`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `api_apitry`
--
ALTER TABLE `api_apitry`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `api_authtoken`
--
ALTER TABLE `api_authtoken`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
--
-- AUTO_INCREMENT pour la table `badges`
--
ALTER TABLE `badges`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `battery`
--
ALTER TABLE `battery`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `battery_logs`
--
ALTER TABLE `battery_logs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `battery_type`
--
ALTER TABLE `battery_type`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cases`
--
ALTER TABLE `cases`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT pour la table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `general_config`
--
ALTER TABLE `general_config`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `kits`
--
ALTER TABLE `kits`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `kit_assignations`
--
ALTER TABLE `kit_assignations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `kit_logs`
--
ALTER TABLE `kit_logs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `kit_types`
--
ALTER TABLE `kit_types`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `kit_type_product_family`
--
ALTER TABLE `kit_type_product_family`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `product_family`
--
ALTER TABLE `product_family`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `product_model`
--
ALTER TABLE `product_model`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `racks`
--
ALTER TABLE `racks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `user_admin_logs`
--
ALTER TABLE `user_admin_logs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user_jobs`
--
ALTER TABLE `user_jobs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user_logs`
--
ALTER TABLE `user_logs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user_passwords`
--
ALTER TABLE `user_passwords`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `uvc_lamp`
--
ALTER TABLE `uvc_lamp`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `product_incidents`
--
ALTER TABLE `product_incidents`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `product_incidents_comment`
--
ALTER TABLE `product_incidents_comment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `product_job_out`
--
ALTER TABLE `product_job_out`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `api_userpasswordupdate`
--
ALTER TABLE `api_userpasswordupdate`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `logs_cases_open`
--
ALTER TABLE `logs_cases_open`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `logs_kit_transaction`
--
ALTER TABLE `logs_kit_transaction`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `logs_product_transaction`
--
ALTER TABLE `logs_product_transaction`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `logs_forensic`
--
ALTER TABLE `logs_forensic`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user_messages`
--
ALTER TABLE `user_messages`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `alerts`
--
ALTER TABLE `alerts`
  ADD CONSTRAINT `alerts_fk0` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `alert_product_in`
--
ALTER TABLE `alert_product_in`
  ADD CONSTRAINT `alert_product_in_fk0` FOREIGN KEY (`alert`) REFERENCES `alerts` (`id`),
  ADD CONSTRAINT `alert_product_in_fk1` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `alert_product_in_fk2` FOREIGN KEY (`kit_log_out`) REFERENCES `kit_logs` (`id`),
  ADD CONSTRAINT `alert_product_in_fk3` FOREIGN KEY (`kit_log_in`) REFERENCES `kit_logs` (`id`);

--
-- Contraintes pour la table `api_authtoken`
--
ALTER TABLE `api_authtoken`
  ADD CONSTRAINT `api_authtoken_user_id_53db953f_fk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Contraintes pour la table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Contraintes pour la table `badges`
--
ALTER TABLE `badges`
  ADD CONSTRAINT `badges_fk0` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `badges_fk1` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `battery`
--
ALTER TABLE `battery`
  ADD CONSTRAINT `battery_fk0` FOREIGN KEY (`type`) REFERENCES `battery_type` (`id`),
  ADD CONSTRAINT `battery_fk1` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `battery_fk2` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `battery_fk3` FOREIGN KEY (`rack`) REFERENCES `racks` (`id`);

--
-- Contraintes pour la table `battery_logs`
--
ALTER TABLE `battery_logs`
  ADD CONSTRAINT `battery_log_fk0` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `battery_log_fk1` FOREIGN KEY (`battery`) REFERENCES `battery` (`id`);

--
-- Contraintes pour la table `battery_type`
--
ALTER TABLE `battery_type`
  ADD CONSTRAINT `battery_type_fk0` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `battery_type_fk1` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `cases`
--
ALTER TABLE `cases`
  ADD CONSTRAINT `cases_fk0` FOREIGN KEY (`kit`) REFERENCES `kits` (`id`),
  ADD CONSTRAINT `cases_fk1` FOREIGN KEY (`rack`) REFERENCES `racks` (`id`),
  ADD CONSTRAINT `cases_fk2` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `cases_fk3` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `general_config`
--
ALTER TABLE `general_config`
  ADD CONSTRAINT `general_config_fk0` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `jobs_fk0` FOREIGN KEY (`kit_type`) REFERENCES `kit_types` (`id`),
  ADD CONSTRAINT `jobs_fk1` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `jobs_fk2` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `kits`
--
ALTER TABLE `kits`
  ADD CONSTRAINT `kits_fk0` FOREIGN KEY (`type`) REFERENCES `kit_types` (`id`),
  ADD CONSTRAINT `kits_fk1` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `kits_fk2` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `kit_assignations`
--
ALTER TABLE `kit_assignations`
  ADD CONSTRAINT `kit_assignations_fk0` FOREIGN KEY (`kit`) REFERENCES `kits` (`id`),
  ADD CONSTRAINT `kit_assignations_fk1` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `kit_assignations_fk3` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `kit_assignations_fk4` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `kit_logs`
--
ALTER TABLE `kit_logs`
  ADD CONSTRAINT `kit_logs_fk0` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `kit_logs_fk1` FOREIGN KEY (`kit`) REFERENCES `kits` (`id`);

--
-- Contraintes pour la table `kit_types`
--
ALTER TABLE `kit_types`
  ADD CONSTRAINT `kit_types_fk0` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `kit_types_fk1` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `kit_type_product_family`
--
ALTER TABLE `kit_type_product_family`
  ADD CONSTRAINT `kit_type_product_family_fk0` FOREIGN KEY (`kit_type`) REFERENCES `kit_types` (`id`),
  ADD CONSTRAINT `kit_type_product_family_fk1` FOREIGN KEY (`product_family`) REFERENCES `product_family` (`id`),
  ADD CONSTRAINT `kit_type_product_family_fk2` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_fk0` FOREIGN KEY (`product_type`) REFERENCES `product_model` (`id`),
  ADD CONSTRAINT `products_fk1` FOREIGN KEY (`kit`) REFERENCES `kits` (`id`),
  ADD CONSTRAINT `products_fk2` FOREIGN KEY (`battery_type`) REFERENCES `battery_type` (`id`),
  ADD CONSTRAINT `products_fk3` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `products_fk4` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `product_family`
--
ALTER TABLE `product_family`
  ADD CONSTRAINT `product_family_fk0` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `product_family_fk1` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `product_model`
--
ALTER TABLE `product_model`
  ADD CONSTRAINT `product_model_fk0` FOREIGN KEY (`family`) REFERENCES `product_family` (`id`),
  ADD CONSTRAINT `product_model_fk1` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `product_model_fk2` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `racks`
--
ALTER TABLE `racks`
  ADD CONSTRAINT `racks_fk0` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `racks_fk1` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `shifts`
--
ALTER TABLE `shifts`
  ADD CONSTRAINT `shifts_fk0` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `shifts_fk1` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_fk0` FOREIGN KEY (`manager`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `users_fk1` FOREIGN KEY (`shift`) REFERENCES `shifts` (`id`),
  ADD CONSTRAINT `users_fk2` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `users_fk3` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `user_admin_logs`
--
ALTER TABLE `user_admin_logs`
  ADD CONSTRAINT `user_admin_logs_fk0` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_admin_logs_fk1` FOREIGN KEY (`rack`) REFERENCES `racks` (`id`);

--
-- Contraintes pour la table `user_jobs`
--
ALTER TABLE `user_jobs`
  ADD CONSTRAINT `user_jobs_fk0` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_jobs_fk1` FOREIGN KEY (`job`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `user_jobs_fk2` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_jobs_fk3` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `user_logs`
--
ALTER TABLE `user_logs`
  ADD CONSTRAINT `user_logs_fk0` FOREIGN KEY (`rack`) REFERENCES `racks` (`id`),
  ADD CONSTRAINT `user_logs_fk1` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `user_passwords`
--
ALTER TABLE `user_passwords`
  ADD CONSTRAINT `user_passwords_fk0` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `uvc_lamp`
--
ALTER TABLE `uvc_lamp`
  ADD CONSTRAINT `uvc_lamp_fk0` FOREIGN KEY (`rack`) REFERENCES `racks` (`id`),
  ADD CONSTRAINT `uvc_lamp_fk1` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `uvc_lamp_fk2` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `product_incidents`
--
ALTER TABLE `product_incidents`
  ADD CONSTRAINT `product_incidents_fk0` FOREIGN KEY (`product`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `product_incidents_fk1` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `product_incidents_fk2` FOREIGN KEY (`update_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `product_incidents_comment`
--
ALTER TABLE `product_incidents_comment`
  ADD CONSTRAINT `product_incidents_comment_fk0` FOREIGN KEY (`incident`) REFERENCES `product_incidents` (`id`),
  ADD CONSTRAINT `product_incidents_comment_fk1` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `product_job_out`
--
ALTER TABLE `product_job_out`
  ADD CONSTRAINT `product_job_out_fk0` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `product_job_out_fk1` FOREIGN KEY (`job`) REFERENCES `jobs` (`id`);

--
-- Contraintes pour la table `api_userpasswordupdate`
--
ALTER TABLE `api_userpasswordupdate`
  ADD CONSTRAINT `api_userpasswordupdate_user_id_3ab20a80_fk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `logs_kit_transaction`
--
ALTER TABLE `logs_kit_transaction`
  ADD CONSTRAINT `logs_kit_transaction_fk0` FOREIGN KEY (`log_case`) REFERENCES `logs_cases_open` (`id`);

--
-- Contraintes pour la table `logs_kit_transaction`
--
ALTER TABLE `logs_product_transaction`
  ADD CONSTRAINT `logs_product_transaction_fk0` FOREIGN KEY (`log_kit`) REFERENCES `logs_kit_transaction` (`id`);

--
-- Contraintes pour la table `user_passwords`
--
ALTER TABLE `user_passwords`
  ADD CONSTRAINT `user_passwords_fk0` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `user_messages`
--
ALTER TABLE `user_messages`
  ADD CONSTRAINT `user_messages_fk0` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_messages_fk1` FOREIGN KEY (`creation_user`) REFERENCES `users` (`id`);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;