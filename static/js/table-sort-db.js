/* 
table-sort-js
Author: Lee Wannacott
Licence: MIT License Copyright (c) 2021 Lee Wannacott 
    
GitHub Repository: https://github.com/LeeWannacott/table-sort-js
npm package: https://www.npmjs.com/package/table-sort-js
Demo: https://leewannacott.github.io/Portfolio/#/GitHub

Install:
Frontend: <script src="https://leewannacott.github.io/table-sort-js/table-sort.js"></script> or
Download this file and add <script src="table-sort.js"></script> to your HTML 

Backend: npm install table-sort-js and use require("../node_modules/table-sort-js/table-sort.js") 

Instructions:
  Add class="table-sort" to tables you'd like to make sortable
  Click on the table headers to sort them.
*/

function tableSortJs() {
    for (let table of document.getElementsByTagName("table")) {
        if (table.classList.contains("table-sort")) {
            makeTableSortable(table);
        }
    }

    function makeTableSortable(sortableTable) {
        if (sortableTable.getElementsByTagName("thead").length === 0) {
            const the = document.createElement("thead");
            the.appendChild(sortableTable.rows[0]);
            sortableTable.insertBefore(the, sortableTable.firstChild);
        }

        const tableHead = sortableTable.querySelector("thead");
        const tableHeadHeaders = tableHead.querySelectorAll("th.sorted");

        for (let [, th] of tableHeadHeaders.entries()) {
            let timesClickedColumn = 0;
            if (window.location.search.includes("order=" + $(th).data("name"))) {
                timesClickedColumn++;
                if (window.location.search.includes("orderdesc=1"))
                    timesClickedColumn++;
                updateTable(th);
            }
            th.style.cursor = "pointer";

            function updateTable(th) {
                function clearArrows(arrowUp = "<i class=\"fas fa-long-arrow-alt-up\"></i>", arrowDown = "<i class=\"fas fa-long-arrow-alt-down\"></i>") {
                    th.innerHTML = th.innerHTML.replace(arrowUp, "");
                    th.innerHTML = th.innerHTML.replace(arrowDown, "");
                }

                let arrowUp = " <i class=\"fas fa-long-arrow-alt-up\"></i>";
                let arrowDown = " <i class=\"fas fa-long-arrow-alt-down\"></i>";
                let tableArrows = sortableTable.classList.contains('table-arrows');

                if (timesClickedColumn === 1) {
                    if (tableArrows) {
                        clearArrows(arrowUp, arrowDown);
                        th.insertAdjacentHTML("beforeend", arrowUp);
                    }
                } else if (timesClickedColumn === 2) {
                    if (tableArrows) {
                        clearArrows(arrowUp, arrowDown);
                        th.insertAdjacentHTML("beforeend", arrowDown);
                    }
                } else if (timesClickedColumn === 3) {
                    timesClickedColumn = 0;
                    clearArrows(arrowUp, arrowDown);
                }
            }

            th.addEventListener("click", function () {
                timesClickedColumn += 1;

                let name = $(th).data("name");
                if (timesClickedColumn === 1) {
                    window.location.search = "?order=" + name;
                } else if (timesClickedColumn === 2) {
                    window.location.search = "?orderdesc=1&order=" + name;
                } else {
                    window.location.search = "";
                }
            });
        }
    }
}

if (
    document.readyState === "complete" ||
    document.readyState === "interactive"
) {
    tableSortJs();
} else if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", tableSortJs, false);
}