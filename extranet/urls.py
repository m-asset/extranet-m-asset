from django.contrib.auth import views as auth_views
from django.urls import path, include

urlpatterns = [
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('', include('website.apps.web.urls', namespace='web')),
    path('api/', include('website.apps.api.urls', namespace='api')),
    path('apikiosk/', include('website.apps.api.urls_kiosk', namespace='api_kiosk')),
]
