import os

from extranet.settings import BASE_DIR


def check():
    path = os.path.join(BASE_DIR, "extranet_errors.log")

    if os.path.lexists(path):
        size = os.path.getsize(path)
        if size > 2097152:
            open(path, 'w').close()
