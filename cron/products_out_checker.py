from django.utils import timezone

from extranet import settings
from website.apps.web.mail_util import send_incident
from website.apps.web.models import KitLogs, ProductIncidents, Users


def check():
    start = timezone.now() - timezone.timedelta(days=3)
    alert = timezone.now() - timezone.timedelta(hours=10)
    kitHandled = []
    kitsOut = []
    for kitlog in KitLogs.objects.filter(creation_timestamp__gte=start, creation_timestamp__lte=alert, out=True):
        kit = kitlog.kit
        if kit.id in kitHandled:
            continue
        kitHandled.append(kit.id)
        last_kitlog = KitLogs.objects.filter(kit=kit).order_by("-creation_timestamp")
        if last_kitlog.count() > 0:
            last_kitlog = last_kitlog.first()

            if last_kitlog.out and last_kitlog.creation_timestamp < alert:
                kitsOut.append({"kit": last_kitlog.kit, "log": last_kitlog})

    if len(kitsOut) == 0:
        return

    vendor = Users.objects.filter(ihm_password__isnull=False).first()

    for data in kitsOut:
        kit = data["kit"]
        log = data["log"]

        skip = False
        for product in kit.get_products():
            if skip:
                continue
            if ProductIncidents.objects.filter(product=product,
                                               creation_timestamp__gte=log.creation_timestamp).count() > 0:
                skip = True
                continue

        if skip:
            continue

        for product in kit.get_products():
            incident = ProductIncidents()
            incident.product = product
            incident.type = 5
            incident.solved = 0
            incident.creation_timestamp = timezone.now()
            incident.update_timestamp = timezone.now()
            incident.creation_user = vendor
            incident.update_user = vendor
            incident.save()

            if settings.EMAIL_ACTIVATED:
                send_incident(product, incident)
